-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.44-0ubuntu0.14.10.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table boozeMe.revisions
DROP TABLE IF EXISTS `revisions`;
CREATE TABLE IF NOT EXISTS `revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.revisions: ~115 rows (approximately)
DELETE FROM `revisions`;
/*!40000 ALTER TABLE `revisions` DISABLE KEYS */;
INSERT INTO `revisions` (`id`, `timestamp`, `username`) VALUES
	(1, '2015-09-07 06:32:47', 'a'),
	(2, '2015-09-07 06:48:22', 'a'),
	(3, '2015-09-07 09:19:12', 'a'),
	(4, '2015-09-07 09:21:12', 'a'),
	(5, '2015-09-07 09:27:06', 'a'),
	(6, '2015-09-07 10:03:48', 'a'),
	(7, '2015-09-07 12:07:18', 'a'),
	(8, '2015-09-08 04:07:08', 'a'),
	(9, '2015-09-08 04:08:27', 'a'),
	(10, '2015-09-08 04:10:41', 'a'),
	(11, '2015-09-08 04:33:59', 'a'),
	(12, '2015-09-08 04:35:21', 'a'),
	(13, '2015-09-08 04:38:59', 'a'),
	(14, '2015-09-08 04:39:19', 'a'),
	(15, '2015-09-08 04:52:48', 'a'),
	(16, '2015-09-08 05:07:40', 'a'),
	(17, '2015-09-08 05:07:51', 'a'),
	(18, '2015-09-08 05:08:04', 'a'),
	(19, '2015-09-08 05:15:41', 'a'),
	(20, '2015-09-08 05:21:52', 'a'),
	(21, '2015-09-08 05:29:35', 'a'),
	(22, '2015-09-08 05:37:53', 'a'),
	(23, '2015-09-08 05:39:39', 'a'),
	(24, '2015-09-08 05:41:28', 'a'),
	(25, '2015-09-08 05:41:45', 'a'),
	(26, '2015-09-08 05:45:53', 'a'),
	(27, '2015-09-08 08:26:15', 'a'),
	(28, '2015-09-08 08:56:37', 'a'),
	(29, '2015-09-08 08:57:02', 'a'),
	(30, '2015-09-08 08:58:08', 'a'),
	(31, '2015-09-08 08:59:49', 'a'),
	(32, '2015-09-08 09:01:55', 'a'),
	(33, '2015-09-08 09:09:06', 'a'),
	(34, '2015-09-08 09:09:32', 'a'),
	(35, '2015-09-08 09:10:00', 'a'),
	(36, '2015-09-08 09:12:56', 'a'),
	(37, '2015-09-08 09:15:36', 'a'),
	(38, '2015-09-08 09:32:04', 'a'),
	(39, '2015-09-08 09:43:57', 'a'),
	(40, '2015-09-08 09:47:23', 'a'),
	(41, '2015-09-08 09:47:36', 'a'),
	(42, '2015-09-08 09:48:05', 'a'),
	(43, '2015-09-08 09:49:43', 'a'),
	(44, '2015-09-08 09:53:02', 'a'),
	(45, '2015-09-08 09:53:42', 'a'),
	(46, '2015-09-08 09:54:37', 'a'),
	(47, '2015-09-08 10:16:06', 'a'),
	(48, '2015-09-08 10:16:48', 'a'),
	(49, '2015-09-08 10:17:05', 'a'),
	(50, '2015-09-08 10:18:41', 'a'),
	(51, '2015-09-08 10:30:44', 'a'),
	(52, '2015-09-08 10:32:38', 'a'),
	(53, '2015-09-08 10:32:50', 'a'),
	(54, '2015-09-08 10:33:01', 'a'),
	(55, '2015-09-08 10:42:05', 'a'),
	(56, '2015-09-08 10:42:52', 'a'),
	(57, '2015-09-08 11:20:22', 'a'),
	(58, '2015-09-08 11:20:48', 'a'),
	(59, '2015-09-08 11:33:30', 'a'),
	(60, '2015-09-08 11:44:32', 'a'),
	(61, '2015-09-11 04:18:40', 'a'),
	(62, '2015-09-11 04:19:14', 'a'),
	(63, '2015-09-11 04:19:19', 'a'),
	(64, '2015-09-11 04:21:05', 'a'),
	(65, '2015-09-11 04:21:12', 'a'),
	(66, '2015-09-11 08:58:18', 'd@gmail.com'),
	(67, '2015-09-15 05:30:40', 'a'),
	(68, '2015-09-15 05:30:48', 'a'),
	(69, '2015-09-18 09:07:28', 'a'),
	(70, '2015-09-18 10:00:47', 'a'),
	(71, '2015-09-20 04:29:24', 'a'),
	(72, '2015-09-20 04:37:15', 'a'),
	(73, '2015-09-20 04:52:33', 'a'),
	(74, '2015-09-20 04:52:56', 'a'),
	(75, '2015-09-20 04:53:06', 'a'),
	(76, '2015-09-20 05:09:39', 'a'),
	(77, '2015-09-20 05:09:45', 'a'),
	(78, '2015-09-20 05:16:05', 'a'),
	(79, '2015-09-20 05:17:28', 'a'),
	(80, '2015-09-20 05:32:58', 'a'),
	(81, '2015-09-20 05:42:35', 'a'),
	(82, '2015-09-20 05:44:55', 'a'),
	(83, '2015-09-20 06:01:39', 'a'),
	(84, '2015-09-20 06:06:16', 'a'),
	(85, '2015-09-20 06:08:05', 'a'),
	(86, '2015-09-20 06:08:26', 'a'),
	(87, '2015-09-20 06:11:18', 'a'),
	(88, '2015-09-20 06:11:26', 'a'),
	(89, '2015-09-20 06:11:42', 'a'),
	(90, '2015-09-20 06:15:54', 'a'),
	(91, '2015-09-20 06:16:14', 'a'),
	(92, '2015-09-20 06:19:50', 'a'),
	(93, '2015-09-20 06:34:28', 'a'),
	(94, '2015-09-20 06:36:21', 'a'),
	(95, '2015-09-20 06:43:56', 'a'),
	(96, '2015-09-20 06:47:02', 'a'),
	(97, '2015-09-20 06:47:24', 'a'),
	(98, '2015-09-20 06:47:34', 'a'),
	(99, '2015-09-20 06:47:43', 'a'),
	(100, '2015-09-20 06:49:47', 'a'),
	(101, '2015-09-20 06:49:54', 'a'),
	(102, '2015-09-20 06:50:16', 'a'),
	(103, '2015-09-20 06:50:21', 'a'),
	(104, '2015-09-20 06:52:12', 'a'),
	(105, '2015-09-20 06:52:18', 'a'),
	(106, '2015-09-20 06:55:03', 'a'),
	(107, '2015-09-20 06:56:11', 'a'),
	(108, '2015-09-20 06:56:17', 'a'),
	(109, '2015-09-20 06:56:23', 'a'),
	(110, '2015-09-20 07:05:00', 'a'),
	(111, '2015-09-20 07:05:37', 'a'),
	(112, '2015-09-20 07:07:26', 'a'),
	(113, '2015-09-20 07:10:30', 'a'),
	(114, '2015-09-20 07:11:01', 'a'),
	(115, '2015-09-20 08:32:08', 'a');
/*!40000 ALTER TABLE `revisions` ENABLE KEYS */;


-- Dumping structure for table boozeMe.schema_migration_versions
DROP TABLE IF EXISTS `schema_migration_versions`;
CREATE TABLE IF NOT EXISTS `schema_migration_versions` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.schema_migration_versions: ~50 rows (approximately)
DELETE FROM `schema_migration_versions`;
/*!40000 ALTER TABLE `schema_migration_versions` DISABLE KEYS */;
INSERT INTO `schema_migration_versions` (`version`) VALUES
	('20150826230017'),
	('20150828103030'),
	('20150828103116'),
	('20150828104111'),
	('20150830212421'),
	('20150830230113'),
	('20150830230211'),
	('20150904153320'),
	('20150904173427'),
	('20150905222413'),
	('20150906144917'),
	('20150906145832'),
	('20150907113614'),
	('20150907113803'),
	('20150907121733'),
	('20150908144426'),
	('20150908165136'),
	('20150909102453'),
	('20150909113215'),
	('20150909113346'),
	('20150909113659'),
	('20150909123457'),
	('20150909154916'),
	('20150910112008'),
	('20150910112217'),
	('20150910112616'),
	('20150911103139'),
	('20150911153704'),
	('20150911155312'),
	('20150911155400'),
	('20150915101043'),
	('20150915101252'),
	('20150915101503'),
	('20150915101601'),
	('20150915101725'),
	('20150915102059'),
	('20150915102236'),
	('20150915104019'),
	('20150915110937'),
	('20150915111353'),
	('20150915113656'),
	('20150915122603'),
	('20150915123009'),
	('20150915125634'),
	('20150915125717'),
	('20150917151357'),
	('20150917151539'),
	('20150918145423'),
	('20150918145813'),
	('20150918145932');
/*!40000 ALTER TABLE `schema_migration_versions` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_areas
DROP TABLE IF EXISTS `ys_areas`;
CREATE TABLE IF NOT EXISTS `ys_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68F76007B08FA272` (`district_id`),
  CONSTRAINT `FK_68F76007B08FA272` FOREIGN KEY (`district_id`) REFERENCES `ys_districts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_areas: ~0 rows (approximately)
DELETE FROM `ys_areas`;
/*!40000 ALTER TABLE `ys_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ys_areas` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_categories
DROP TABLE IF EXISTS `ys_categories`;
CREATE TABLE IF NOT EXISTS `ys_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_53FE6416727ACA70` (`parent_id`),
  CONSTRAINT `FK_53FE6416727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `ys_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_categories: ~12 rows (approximately)
DELETE FROM `ys_categories`;
/*!40000 ALTER TABLE `ys_categories` DISABLE KEYS */;
INSERT INTO `ys_categories` (`id`, `parent_id`, `created`, `name`, `description`, `status`, `image`, `deleted`) VALUES
	(1, NULL, '2015-09-06 09:20:34', 'AAA', 'dfdf', 0, 'avatar-300.png', 1),
	(2, NULL, '2015-09-06 09:21:11', 'BBB', 'sdds', 1, 'jd9.jpg', 0),
	(3, NULL, '2015-09-06 09:21:37', 'CCC', 'kdfjdf', 1, '55af672c6fa0b1.PNG', 0),
	(4, 3, '2015-09-06 09:21:50', 'DDD', 'dkfdjf', 1, '55af672c6fa0b2.PNG', 0),
	(5, NULL, '2015-09-10 05:31:30', 'EEE', 'kdfjd', 1, '04142011figure_a1.jpg', 0),
	(6, NULL, '2015-09-16 04:24:30', 'FFF', 'fkdjf', 1, '04142011figure_a2.jpg', 0),
	(8, 6, '2015-09-16 04:25:47', 'GGG', 'df', 1, '04142011figure_a4.jpg', 0),
	(9, NULL, '2015-09-18 09:17:17', 'naya', 'new', 1, '04142011figure_a5.jpg', 0),
	(10, NULL, '2015-09-20 09:22:13', 'Hello Category', 'its hello', 1, '04142011figure_a6.jpg', 0),
	(11, NULL, '2015-09-20 10:07:30', 'Blue Level', 'Nice and high cost', 1, '04142011figure_a7.jpg', 0),
	(12, NULL, '2015-09-21 04:04:22', 'dsd', 'sdsd', 1, 'jd14.jpg', 0),
	(13, NULL, '2015-09-21 04:06:44', 'MMM', 'dd', 1, '04142011figure_a8.jpg', 0);
/*!40000 ALTER TABLE `ys_categories` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_countries
DROP TABLE IF EXISTS `ys_countries`;
CREATE TABLE IF NOT EXISTS `ys_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `iso_2` varchar(10) NOT NULL,
  `iso_3` varchar(10) NOT NULL,
  `dialing_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_56866B3E5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_countries: ~250 rows (approximately)
DELETE FROM `ys_countries`;
/*!40000 ALTER TABLE `ys_countries` DISABLE KEYS */;
INSERT INTO `ys_countries` (`id`, `name`, `nationality`, `iso_2`, `iso_3`, `dialing_code`) VALUES
	(1, 'Afghanistan', 'Afghan', 'AF', 'AFG', ''),
	(2, 'Aland Islands', '', 'AX', 'ALA', NULL),
	(3, 'Albania', 'Albanian', 'AL', 'ALB', NULL),
	(4, 'Algeria', 'Algerian', 'DZ', 'DZA', NULL),
	(5, 'American Samoa', 'American Samoan', 'AS', 'ASM', NULL),
	(6, 'Andorra', 'Andorran', 'AD', 'AND', NULL),
	(7, 'Angola', 'Angolan', 'AO', 'AGO', NULL),
	(8, 'Anguilla', 'Anguillan', 'AI', 'AIA', NULL),
	(9, 'Antarctica', '', 'AQ', 'ATA', NULL),
	(10, 'Antigua and Barbuda', 'Antiguan/Barbudan', 'AG', 'ATG', NULL),
	(11, 'Argentina', 'Argentinean', 'AR', 'ARG', NULL),
	(12, 'Armenia', 'Armenian', 'AM', 'ARM', NULL),
	(13, 'Aruba', 'Aruban', 'AW', 'ABW', NULL),
	(14, 'Australia', 'Australian', 'AU', 'AUS', NULL),
	(15, 'Austria', 'Austrian', 'AT', 'AUT', NULL),
	(16, 'Azerbaijan', 'Azerbaijani', 'AZ', 'AZE', NULL),
	(17, 'Bahamas', 'Bahamian', 'BS', 'BHS', NULL),
	(18, 'Bahrain', 'Bahraini', 'BH', 'BHR', NULL),
	(19, 'Bangladesh', 'Bangladeshi', 'BD', 'BGD', NULL),
	(20, 'Barbados', 'Barbadian', 'BB', 'BRB', NULL),
	(21, 'Belarus', 'Belarusian', 'BY', 'BLR', NULL),
	(22, 'Belgium', 'Belgian', 'BE', 'BEL', NULL),
	(23, 'Belize', 'Belizean', 'BZ', 'BLZ', NULL),
	(24, 'Benin', 'Beninese', 'BJ', 'BEN', NULL),
	(25, 'Bermuda', 'Bermudian', 'BM', 'BMU', NULL),
	(26, 'Bhutan', 'Bhutanese', 'BT', 'BTN', NULL),
	(27, 'Bolivia, Plurinational State of', '', 'BO', 'BOL', NULL),
	(28, 'Bonaire, Sint Eustatius and Saba', '', 'BQ', 'BES', NULL),
	(29, 'Bosnia and Herzegovina', 'Bosnian/Herzegovinian', 'BA', 'BIH', NULL),
	(30, 'Botswana', 'Motswana', 'BW', 'BWA', NULL),
	(31, 'Bouvet Island', '', 'BV', 'BVT', NULL),
	(32, 'Brazil', 'Brazilian', 'BR', 'BRA', NULL),
	(33, 'British Indian Ocean Territory', '', 'IO', 'IOT', NULL),
	(34, 'Brunei Darussalam', '', 'BN', 'BRN', NULL),
	(35, 'Bulgaria', 'Bulgarian', 'BG', 'BGR', NULL),
	(36, 'Burkina Faso', '', 'BF', 'BFA', NULL),
	(37, 'Burundi', 'Burundian', 'BI', 'BDI', NULL),
	(38, 'Cambodia', 'Cambodian', 'KH', 'KHM', NULL),
	(39, 'Cameroon', 'Cameroonian', 'CM', 'CMR', NULL),
	(40, 'Canada', 'Canadian', 'CA', 'CAN', NULL),
	(41, 'Cape Verde', 'Cape Verdean', 'CV', 'CPV', NULL),
	(42, 'Cayman Islands', 'Caymanian', 'KY', 'CYM', NULL),
	(43, 'Central African Republic', 'Central African', 'CF', 'CAF', NULL),
	(44, 'Chad', 'Chadian', 'TD', 'TCD', NULL),
	(45, 'Chile', 'Chilean', 'CL', 'CHL', NULL),
	(46, 'China', '', 'CN', 'CHN', NULL),
	(47, 'Christmas Island', 'Christmas Island', 'CX', 'CXR', NULL),
	(48, 'Cocos (Keeling) Islands', 'Cocos Island', 'CC', 'CCK', NULL),
	(49, 'Colombia', 'Colombian', 'CO', 'COL', NULL),
	(50, 'Comoros', 'Comorian', 'KM', 'COM', NULL),
	(51, 'Congo', '', 'CG', 'COG', NULL),
	(52, 'Congo, The Democratic Republic of the', '', 'CD', 'COD', NULL),
	(53, 'Cook Islands', 'Cook Island', 'CK', 'COK', NULL),
	(54, 'Costa Rica', 'Costa Rican', 'CR', 'CRI', NULL),
	(55, 'Cote d\'Ivoire', '', 'CI', 'CIV', NULL),
	(56, 'Croatia', 'Croatian', 'HR', 'HRV', NULL),
	(57, 'Cuba', 'Cuban', 'CU', 'CUB', NULL),
	(58, 'Curacao', '', 'CW', 'CUW', NULL),
	(59, 'Cyprus', 'Cypriot', 'CY', 'CYP', NULL),
	(60, 'Czech Republic', 'Czech', 'CZ', 'CZE', NULL),
	(61, 'Denmark', 'Danish', 'DK', 'DNK', NULL),
	(62, 'Djibouti', 'Djiboutian', 'DJ', 'DJI', NULL),
	(63, 'Dominica', 'Dominicand', 'DM', 'DMA', NULL),
	(64, 'Dominican Republic', 'Dominicane', 'DO', 'DOM', NULL),
	(65, 'Ecuador', 'Ecuadorian', 'EC', 'ECU', NULL),
	(66, 'Egypt', 'Egyptian', 'EG', 'EGY', NULL),
	(67, 'El Salvador', 'Salvadoran', 'SV', 'SLV', NULL),
	(68, 'Equatorial Guinea', 'Equatorial Guinean', 'GQ', 'GNQ', NULL),
	(69, 'Eritrea', 'Eritrean', 'ER', 'ERI', NULL),
	(70, 'Estonia', 'Estonian', 'EE', 'EST', NULL),
	(71, 'Ethiopia', 'Ethiopian', 'ET', 'ETH', NULL),
	(72, 'Falkland Islands (Malvinas)', '', 'FK', 'FLK', NULL),
	(73, 'Faroe Islands', 'Faroese', 'FO', 'FRO', NULL),
	(74, 'Fiji', 'Fijian', 'FJ', 'FJI', NULL),
	(75, 'Finland', 'Finnish', 'FI', 'FIN', NULL),
	(76, 'France', 'French', 'FR', 'FRA', NULL),
	(77, 'French Guiana', 'French Guianese', 'GF', 'GUF', NULL),
	(78, 'French Polynesia', 'French Polynesian', 'PF', 'PYF', NULL),
	(79, 'French Southern Territories', '', 'TF', 'ATF', NULL),
	(80, 'Gabon', 'Gabonese', 'GA', 'GAB', NULL),
	(81, 'Gambia', 'Gambian', 'GM', 'GMB', NULL),
	(82, 'Georgia', 'Georgian', 'GE', 'GEO', NULL),
	(83, 'Germany', 'German', 'DE', 'DEU', NULL),
	(84, 'Ghana', 'Ghanaian', 'GH', 'GHA', NULL),
	(85, 'Gibraltar', 'Gibraltar', 'GI', 'GIB', NULL),
	(86, 'Greece', 'Greek', 'GR', 'GRC', NULL),
	(87, 'Greenland', 'Greenlandic', 'GL', 'GRL', NULL),
	(88, 'Grenada', 'Grenadian', 'GD', 'GRD', NULL),
	(89, 'Guadeloupe', 'Guadeloupe', 'GP', 'GLP', NULL),
	(90, 'Guam', 'Guamanian', 'GU', 'GUM', NULL),
	(91, 'Guatemala', 'Guatemalan', 'GT', 'GTM', NULL),
	(92, 'Guernsey', '', 'GG', 'GGY', NULL),
	(93, 'Guinea', 'Guinean', 'GN', 'GIN', NULL),
	(94, 'Guinea-Bissau', 'Guinean', 'GW', 'GNB', NULL),
	(95, 'Guyana', 'Guyanese', 'GY', 'GUY', NULL),
	(96, 'Haiti', 'Haitian', 'HT', 'HTI', NULL),
	(97, 'Heard Island and McDonald Islands', '', 'HM', 'HMD', NULL),
	(98, 'Holy See (Vatican City State)', '', 'VA', 'VAT', NULL),
	(99, 'Honduras', 'Honduran', 'HN', 'HND', NULL),
	(100, 'Hong Kong', 'Hongkongese', 'HK', 'HKG', NULL),
	(101, 'Hungary', 'Hungarian', 'HU', 'HUN', NULL),
	(102, 'Iceland', 'Icelandic', 'IS', 'ISL', NULL),
	(103, 'India', 'Indian', 'IN', 'IND', '91'),
	(104, 'Indonesia', 'Indonesian', 'ID', 'IDN', NULL),
	(105, 'Iran, Islamic Republic of', '', 'IR', 'IRN', NULL),
	(106, 'Iraq', 'Iraqi', 'IQ', 'IRQ', NULL),
	(107, 'Ireland', 'Irish', 'IE', 'IRL', NULL),
	(108, 'Isle of Man', 'Manx', 'IM', 'IMN', NULL),
	(109, 'Israel', 'Israeli', 'IL', 'ISR', NULL),
	(110, 'Italy', 'Italian', 'IT', 'ITA', NULL),
	(111, 'Jamaica', 'Jamaican', 'JM', 'JAM', NULL),
	(112, 'Japan', 'Japanese', 'JP', 'JPN', NULL),
	(113, 'Jersey', '', 'JE', 'JEY', NULL),
	(114, 'Jordan', 'Jordanian', 'JO', 'JOR', NULL),
	(115, 'Kazakhstan', 'Kazakh', 'KZ', 'KAZ', NULL),
	(116, 'Kenya', 'Kenyan', 'KE', 'KEN', NULL),
	(117, 'Kiribati', 'I-Kiribati', 'KI', 'KIR', NULL),
	(118, 'Korea, Democratic People\'s Republic of', '', 'KP', 'PRK', NULL),
	(119, 'Korea, Republic of', '', 'KR', 'KOR', NULL),
	(120, 'Kuwait', 'Kuwaiti', 'KW', 'KWT', NULL),
	(121, 'Kyrgyzstan', 'Kyrgyzstani', 'KG', 'KGZ', NULL),
	(122, 'Lao People\'s Democratic Republic', '', 'LA', 'LAO', NULL),
	(123, 'Latvia', 'Latvian', 'LV', 'LVA', NULL),
	(124, 'Lebanon', 'Lebanese', 'LB', 'LBN', NULL),
	(125, 'Lesotho', 'Basotho', 'LS', 'LSO', NULL),
	(126, 'Liberia', 'Liberian', 'LR', 'LBR', NULL),
	(127, 'Libyan Arab Jamahiriya', '', 'LY', 'LBY', NULL),
	(128, 'Liechtenstein', 'Liechtenstein', 'LI', 'LIE', NULL),
	(129, 'Lithuania', 'Lithuanian', 'LT', 'LTU', NULL),
	(130, 'Luxembourg', 'Luxembourgish', 'LU', 'LUX', NULL),
	(131, 'Macao', '', 'MO', 'MAC', NULL),
	(132, 'Macedonia, The former Yugoslav Republic of', '', 'MK', 'MKD', NULL),
	(133, 'Madagascar', 'Malagasy', 'MG', 'MDG', NULL),
	(134, 'Malawi', 'Malawian', 'MW', 'MWI', NULL),
	(135, 'Malaysia', 'Malaysian', 'MY', 'MYS', NULL),
	(136, 'Maldives', 'Maldivian', 'MV', 'MDV', NULL),
	(137, 'Mali', 'Malian', 'ML', 'MLI', NULL),
	(138, 'Malta', 'Maltese', 'MT', 'MLT', NULL),
	(139, 'Marshall Islands', 'Marshallese', 'MH', 'MHL', NULL),
	(140, 'Martinique', 'Martiniquais', 'MQ', 'MTQ', NULL),
	(141, 'Mauritania', 'Mauritanian', 'MR', 'MRT', NULL),
	(142, 'Mauritius', 'Mauritian', 'MU', 'MUS', NULL),
	(143, 'Mayotte', 'Mahoran', 'YT', 'MYT', NULL),
	(144, 'Mexico', 'Mexican', 'MX', 'MEX', NULL),
	(145, 'Micronesia, Federated States of', 'Micronesian', 'FM', 'FSM', NULL),
	(146, 'Moldova, Republic of', '', 'MD', 'MDA', NULL),
	(147, 'Monaco', 'MonÃ©gasque, Monacan', 'MC', 'MCO', NULL),
	(148, 'Mongolia', 'Mongolian', 'MN', 'MNG', NULL),
	(149, 'Montenegro', 'Montenegrin', 'ME', 'MNE', NULL),
	(150, 'Montserrat', 'Montserratian', 'MS', 'MSR', NULL),
	(151, 'Morocco', 'Moroccan', 'MA', 'MAR', NULL),
	(152, 'Mozambique', 'Mozambican', 'MZ', 'MOZ', NULL),
	(153, 'Myanmar', '', 'MM', 'MMR', NULL),
	(154, 'Namibia', 'Namibian', 'NA', 'NAM', NULL),
	(155, 'Nauru', 'Nauruan', 'NR', 'NRU', NULL),
	(156, 'Nepal', 'Nepali', 'NP', 'NPL', '977'),
	(157, 'Netherlands', 'Dutch', 'NL', 'NLD', NULL),
	(158, 'New Caledonia', 'New Caledonian', 'NC', 'NCL', NULL),
	(159, 'New Zealand', 'New Zealand', 'NZ', 'NZL', NULL),
	(160, 'Nicaragua', 'Nicaraguan', 'NI', 'NIC', NULL),
	(161, 'Niger', 'Nigerien', 'NE', 'NER', NULL),
	(162, 'Nigeria', 'Nigerian', 'NG', 'NGA', NULL),
	(163, 'Niue', 'Niuean', 'NU', 'NIU', NULL),
	(164, 'Norfolk Island', '', 'NF', 'NFK', NULL),
	(165, 'Northern Mariana Islands', '', 'MP', 'MNP', NULL),
	(166, 'Norway', 'Norwegian', 'NO', 'NOR', NULL),
	(167, 'Oman', 'Omani', 'OM', 'OMN', NULL),
	(168, 'Pakistan', 'Pakistani', 'PK', 'PAK', NULL),
	(169, 'Palau', 'Palauan', 'PW', 'PLW', NULL),
	(170, 'Palestinian Territory, Occupied', '', 'PS', 'PSE', NULL),
	(171, 'Panama', 'Panamanian', 'PA', 'PAN', NULL),
	(172, 'Papua New Guinea', 'Papua New Guinean', 'PG', 'PNG', NULL),
	(173, 'Paraguay', 'Paraguayan', 'PY', 'PRY', NULL),
	(174, 'Peru', 'Peruvian', 'PE', 'PER', NULL),
	(175, 'Philippines', 'Filipino', 'PH', 'PHL', NULL),
	(176, 'Pitcairn', '', 'PN', 'PCN', NULL),
	(177, 'Poland', 'Polish', 'PL', 'POL', NULL),
	(178, 'Portugal', 'Portuguese', 'PT', 'PRT', NULL),
	(179, 'Puerto Rico', 'Puerto Rican', 'PR', 'PRI', NULL),
	(180, 'Qatar', 'Qatari', 'QA', 'QAT', NULL),
	(181, 'Reunion', '', 'RE', 'REU', NULL),
	(182, 'Romania', 'Romanian', 'RO', 'ROU', NULL),
	(183, 'Russian Federation', '', 'RU', 'RUS', NULL),
	(184, 'Rwanda', 'Rwandan', 'RW', 'RWA', NULL),
	(185, 'Saint Barthelemy', '', 'BL', 'BLM', NULL),
	(186, 'Saint Helena, Ascension and Tristan Da Cunha', '', 'SH', 'SHN', NULL),
	(187, 'Saint Kitts and Nevis', '', 'KN', 'KNA', NULL),
	(188, 'Saint Lucia', '', 'LC', 'LCA', NULL),
	(189, 'Saint Martin (French Part)', '', 'MF', 'MAF', NULL),
	(190, 'Saint Pierre and Miquelon', '', 'PM', 'SPM', NULL),
	(191, 'Saint Vincent and The Grenadines', '', 'VC', 'VCT', NULL),
	(192, 'Samoa', 'Samoan', 'WS', 'WSM', NULL),
	(193, 'San Marino', 'Sammarinese', 'SM', 'SMR', NULL),
	(194, 'Sao Tome and Principe', '', 'ST', 'STP', NULL),
	(195, 'Saudi Arabia', 'Saudi Arabian', 'SA', 'SAU', NULL),
	(196, 'Senegal', 'Senegalese', 'SN', 'SEN', NULL),
	(197, 'Serbia', 'Serbian', 'RS', 'SRB', NULL),
	(198, 'Seychelles', 'Seychellois', 'SC', 'SYC', NULL),
	(199, 'Sierra Leone', 'Sierra Leonean', 'SL', 'SLE', NULL),
	(200, 'Singapore', 'Singapore', 'SG', 'SGP', NULL),
	(201, 'Sint Maarten (Dutch Part)', '', 'SX', 'SXM', NULL),
	(202, 'Slovakia', 'Slovak', 'SK', 'SVK', NULL),
	(203, 'Slovenia', 'Slovenian', 'SI', 'SVN', NULL),
	(204, 'Solomon Islands', 'Solomon Island', 'SB', 'SLB', NULL),
	(205, 'Somalia', 'Somalian', 'SO', 'SOM', NULL),
	(206, 'South Africa', 'South African', 'ZA', 'ZAF', NULL),
	(207, 'South Georgia and The South Sandwich Islands', '', 'GS', 'SGS', NULL),
	(208, 'South Sudan', 'South Sudanese', 'SS', 'SSD', NULL),
	(209, 'Spain', 'Spanish', 'ES', 'ESP', NULL),
	(210, 'Sri Lanka', 'Sri Lankan', 'LK', 'LKA', NULL),
	(211, 'Sudan', 'Sudanese', 'SD', 'SDN', NULL),
	(212, 'Suriname', '', 'SR', 'SUR', NULL),
	(213, 'Svalbard and Jan Mayen', '', 'SJ', 'SJM', NULL),
	(214, 'Swaziland', 'Swazi', 'SZ', 'SWZ', NULL),
	(215, 'Sweden', 'Swedish', 'SE', 'SWE', NULL),
	(216, 'Switzerland', 'Swiss', 'CH', 'CHE', NULL),
	(217, 'Syrian Arab Republic', '', 'SY', 'SYR', NULL),
	(218, 'Taiwan, Province of China', '', 'TW', 'TWN', NULL),
	(219, 'Tajikistan', 'Tajikistani', 'TJ', 'TJK', NULL),
	(220, 'Tanzania, United Republic of', '', 'TZ', 'TZA', NULL),
	(221, 'Thailand', 'Thai', 'TH', 'THA', NULL),
	(222, 'Timor-Leste', '', 'TL', 'TLS', NULL),
	(223, 'Togo', 'Togolese', 'TG', 'TGO', NULL),
	(224, 'Tokelau', '', 'TK', 'TKL', NULL),
	(225, 'Tonga', 'Tongan', 'TO', 'TON', NULL),
	(226, 'Trinidad and Tobago', 'Trinidadian', 'TT', 'TTO', NULL),
	(227, 'Tunisia', 'Tunisian', 'TN', 'TUN', NULL),
	(228, 'Turkey', 'Turkish', 'TR', 'TUR', NULL),
	(229, 'Turkmenistan', 'Turkmen', 'TM', 'TKM', NULL),
	(230, 'Turks and Caicos Islands', 'none', 'TC', 'TCA', NULL),
	(231, 'Tuvalu', 'Tuvaluan', 'TV', 'TUV', NULL),
	(232, 'Uganda', 'Ugandan', 'UG', 'UGA', NULL),
	(233, 'Ukraine', 'Ukrainian', 'UA', 'UKR', NULL),
	(234, 'United Arab Emirates', 'Emirati', 'AE', 'ARE', NULL),
	(235, 'United Kingdom', 'British', 'GB', 'GBR', NULL),
	(236, 'United States', 'American', 'US', 'USA', NULL),
	(237, 'United States Minor Outlying Islands', '', 'UM', 'UMI', NULL),
	(238, 'Uruguay', 'Uruguayan', 'UY', 'URY', NULL),
	(239, 'Uzbekistan', 'Uzbekistani', 'UZ', 'UZB', NULL),
	(240, 'Vanuatu', 'Ni-Vanuatu', 'VU', 'VUT', NULL),
	(241, 'Venezuela, Bolivarian Republic of', '', 'VE', 'VEN', NULL),
	(242, 'Viet Nam', '', 'VN', 'VNM', NULL),
	(243, 'Virgin Islands, British', '', 'VG', 'VGB', NULL),
	(244, 'Virgin Islands, U.S.', '', 'VI', 'VIR', NULL),
	(245, 'Wallis and Futuna', 'Wallisian/Futunan', 'WF', 'WLF', NULL),
	(246, 'Western Sahara', 'Sahrawian', 'EH', 'ESH', NULL),
	(247, 'Yemen', 'Yemeni', 'YE', 'YEM', NULL),
	(248, 'Zambia', 'Zambian', 'ZM', 'ZMB', NULL),
	(249, 'Zimbabwe', 'Zimbabwean', 'ZW', 'ZWE', NULL),
	(276, 'Nepall', 'NEPALI', 'NP', 'NPL', '977');
/*!40000 ALTER TABLE `ys_countries` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_customers
DROP TABLE IF EXISTS `ys_customers`;
CREATE TABLE IF NOT EXISTS `ys_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `social_id` varchar(50) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `username` tinytext,
  `salt` varchar(255) DEFAULT NULL,
  `secrete` varchar(255) DEFAULT NULL,
  `media_type` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `accessToken` varchar(255) NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69B3CEB2350A9822` (`accessToken`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_customers: ~6 rows (approximately)
DELETE FROM `ys_customers`;
/*!40000 ALTER TABLE `ys_customers` DISABLE KEYS */;
INSERT INTO `ys_customers` (`id`, `email`, `social_id`, `firstname`, `lastname`, `middlename`, `image`, `gender`, `username`, `salt`, `secrete`, `media_type`, `status`, `createdDate`, `updatedDate`, `last_login`, `fullname`, `phone`, `accessToken`, `date_of_birth`) VALUES
	(1, 'ratnakatuwal89@yahoo.com', '1127268917300403', 'Ratna Katuwal', NULL, NULL, NULL, 'male', NULL, NULL, NULL, 'FACEBOOK', 1, '2015-09-15 06:45:57', '2015-09-20 11:58:28', '2015-09-20 11:58:28', '', 0, '', '0000-00-00 00:00:00'),
	(19, 'mahesh@gmail.com', 'jlR5NBsCobS5lHdO', 'mahesh', 'mahesh', 'babu', NULL, 'male', 'maheshmahesh', 'H86bLvTQ5HAVtJ3NHmgkdiosE5ypCT5s', 't6WLHazbtVJVfg5rwRBem0nimD6SNnP5', 'APP', 1, '2015-09-18 04:55:42', '2015-09-18 10:44:20', '2015-09-18 04:55:42', 'mahesh babu mahesh', 985304323, 'Uy3XxRncSDLGSFRxJe9qGqR4', '0000-00-00 00:00:00'),
	(20, 'mahes@gmail.com', 'WiV9G2ZSNwx2Nbyj', 'mahesh', 'mahesh', 'babu', NULL, 'male', 'maheshmahes', 'OJ0Br+EEPGkzxYDW5faeh7TYu2fNSztH', '4hVSL8ye0N3iS73TAcK4dRWjIeYUQZPA', 'APP', 1, '2015-09-18 04:56:00', '2015-09-18 10:53:00', '2015-09-18 04:56:00', 'mahesh babu mahesh', 985304323, 'rp64dcWLGFh39nkYL98OyUsN', '0000-00-00 00:00:00'),
	(21, 'pradeep@gmail.com', 'uOK8F32LSDUn8JwE', 'pradeep', 'karki', 'kumar', NULL, 'male', 'danepliz', 'I5MDZYCu8Hp7Sr7Yzqfw5M771rVUz4XY', 'rpy5zqOyQGS3tv67X/HcnfQjmQQPfcv1', 'APP', 1, '2015-09-18 05:34:42', '2015-09-18 05:34:42', '2015-09-18 05:34:42', 'pradeep kumar karki', 2147483647, '4luKkJGhqIW6kieq7M11vlUu', '0000-00-00 00:00:00'),
	(22, 'mah@gmail.com', 'vmltIpiZu6VIo9Ot', 'mahesh', 'mahesh', 'babu', NULL, 'male', 'mahesh', 'VaIZ8BZ/s8RS3fwErYTw+c+M2o5L2J4/', 'Wnr9MWhy8c6T5Up+Umtc+wlqSQygCCPy', 'APP', 1, '2015-09-18 09:22:35', '2015-09-18 09:22:35', '2015-09-18 09:22:35', 'mahesh babu mahesh', 985304323, 'ddhdMAqCYY2LNAm5XaGQ4U5J', NULL),
	(23, 'ma@gmail.com', 't5JsE77DklJx7DjZ', 'mahesh', 'mahesh', 'babu', NULL, 'male', 'df', 'Y+z+n4BrDEQKI39HH6uOZPmq5Ro3d9o8', 'ZdjI6iVdzqRSZZnfZxfq3DZZiaZquiYE', 'APP', 1, '2015-09-18 09:38:49', '2015-09-18 09:38:49', '2015-09-18 09:38:49', 'mahesh babu mahesh', 985304323, 'w4DxBm02trT8WG0tj5A7mDUI', NULL);
/*!40000 ALTER TABLE `ys_customers` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_districts
DROP TABLE IF EXISTS `ys_districts`;
CREATE TABLE IF NOT EXISTS `ys_districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6303984FF92F3E70` (`country_id`),
  CONSTRAINT `FK_6303984FF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `ys_countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_districts: ~2 rows (approximately)
DELETE FROM `ys_districts`;
/*!40000 ALTER TABLE `ys_districts` DISABLE KEYS */;
INSERT INTO `ys_districts` (`id`, `country_id`, `name`) VALUES
	(1, 156, 'Sindhupalchowk'),
	(2, 156, 'Kathmandu');
/*!40000 ALTER TABLE `ys_districts` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_groups
DROP TABLE IF EXISTS `ys_groups`;
CREATE TABLE IF NOT EXISTS `ys_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `mtoOnly` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CE4F6DE5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_groups: ~8 rows (approximately)
DELETE FROM `ys_groups`;
/*!40000 ALTER TABLE `ys_groups` DISABLE KEYS */;
INSERT INTO `ys_groups` (`id`, `name`, `description`, `active`, `created`, `mtoOnly`) VALUES
	(1, 'SUPERADMIN', 'Superadmin, Overall permitted user', 1, '2015-08-05 14:55:01', 0),
	(2, 'Vendor', 'vendor group', 1, '2015-08-26 16:43:56', 0),
	(3, 'Dfdf', 'dfdf', 1, '2015-08-28 04:50:44', 0),
	(4, 'Vendor-clone-1441730753', 'dfdfddfdf', 1, '2015-08-26 16:43:56', 0),
	(5, 'Vendor-clone-1442743863', 'vendor group', 1, '2015-08-26 16:43:56', 0),
	(6, 'Vendor-clone-1442743869', 'vendor group', 1, '2015-08-26 16:43:56', 0),
	(7, 'Vendor Group', 'vendor group', 1, '2015-08-26 16:43:56', 0),
	(8, 'Vendor-clone-1442743908', 'vendor group', 1, '2015-08-26 16:43:56', 0);
/*!40000 ALTER TABLE `ys_groups` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_group_permission
DROP TABLE IF EXISTS `ys_group_permission`;
CREATE TABLE IF NOT EXISTS `ys_group_permission` (
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`permission_id`),
  KEY `IDX_8C1CBBFFE54D947` (`group_id`),
  KEY `IDX_8C1CBBFFED90CCA` (`permission_id`),
  CONSTRAINT `FK_8C1CBBFFE54D947` FOREIGN KEY (`group_id`) REFERENCES `ys_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8C1CBBFFED90CCA` FOREIGN KEY (`permission_id`) REFERENCES `ys_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_group_permission: ~54 rows (approximately)
DELETE FROM `ys_group_permission`;
/*!40000 ALTER TABLE `ys_group_permission` DISABLE KEYS */;
INSERT INTO `ys_group_permission` (`group_id`, `permission_id`) VALUES
	(2, 1),
	(2, 21),
	(2, 22),
	(2, 23),
	(2, 24),
	(2, 25),
	(2, 26),
	(2, 27),
	(2, 28),
	(2, 29),
	(3, 18),
	(3, 21),
	(3, 24),
	(4, 1),
	(5, 1),
	(5, 21),
	(5, 22),
	(5, 23),
	(5, 24),
	(5, 25),
	(5, 26),
	(5, 27),
	(5, 28),
	(5, 29),
	(6, 1),
	(6, 21),
	(6, 22),
	(6, 23),
	(6, 24),
	(6, 25),
	(6, 26),
	(6, 27),
	(6, 28),
	(6, 29),
	(7, 1),
	(7, 21),
	(7, 22),
	(7, 23),
	(7, 24),
	(7, 25),
	(7, 26),
	(7, 27),
	(7, 28),
	(7, 29),
	(8, 1),
	(8, 21),
	(8, 22),
	(8, 23),
	(8, 24),
	(8, 25),
	(8, 26),
	(8, 27),
	(8, 28),
	(8, 29);
/*!40000 ALTER TABLE `ys_group_permission` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_options
DROP TABLE IF EXISTS `ys_options`;
CREATE TABLE IF NOT EXISTS `ys_options` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(100) NOT NULL,
  `option_value` text NOT NULL,
  `autoload` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`option_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table boozeMe.ys_options: ~0 rows (approximately)
DELETE FROM `ys_options`;
/*!40000 ALTER TABLE `ys_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `ys_options` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_permissions
DROP TABLE IF EXISTS `ys_permissions`;
CREATE TABLE IF NOT EXISTS `ys_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9A399D765E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_permissions: ~29 rows (approximately)
DELETE FROM `ys_permissions`;
/*!40000 ALTER TABLE `ys_permissions` DISABLE KEYS */;
INSERT INTO `ys_permissions` (`id`, `name`, `description`) VALUES
	(1, 'view user', 'list and view user'),
	(2, 'administer user', 'Create, edit, block, unblock and delete users.'),
	(3, 'reset password', 'Reset other user\'s password.'),
	(4, 'allow user switching', 'User can run application as other user of same (or sub) agent.'),
	(5, 'general setting', 'Update general settings.'),
	(6, 'user setting', 'Update user settings.'),
	(7, 'administer category', 'Manage Category'),
	(8, 'administer questions', 'Manage Question Answers'),
	(9, 'list entry', 'List all the data entries'),
	(10, 'add entry', 'Fill up queries'),
	(11, 'list survey', 'List all the data entries'),
	(12, 'add survey', 'Fill up queries'),
	(13, 'view survey', 'View survey detail'),
	(14, 'delete question', 'Delete Question'),
	(15, 'delete survey', 'Delete Survey'),
	(16, 'administer enumerator', 'Add delete update enumerator'),
	(17, 'administer poll', 'Checks Polls'),
	(18, 'list category', 'List all the categories'),
	(19, 'add category', 'add categories'),
	(20, 'delete category', 'delete categories'),
	(21, 'list vendor', 'List all the categories'),
	(22, 'add vendor', 'add categories'),
	(23, 'delete vendor', 'delete categories'),
	(24, 'list vendor contact person', 'List all vendor contact person'),
	(25, 'add vendor contact person', 'add vendor contact person'),
	(26, 'delete vendor contact person', 'delete vendor contact person'),
	(27, 'list product', 'List all the products'),
	(28, 'add product', 'add products'),
	(29, 'delete product', 'delete products');
/*!40000 ALTER TABLE `ys_permissions` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_productImages
DROP TABLE IF EXISTS `ys_productImages`;
CREATE TABLE IF NOT EXISTS `ys_productImages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `imageName` varchar(50) NOT NULL,
  `caption` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4BBB53324584665A` (`product_id`),
  CONSTRAINT `FK_4BBB53324584665A` FOREIGN KEY (`product_id`) REFERENCES `ys_products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_productImages: ~7 rows (approximately)
DELETE FROM `ys_productImages`;
/*!40000 ALTER TABLE `ys_productImages` DISABLE KEYS */;
INSERT INTO `ys_productImages` (`id`, `product_id`, `imageName`, `caption`) VALUES
	(3, 8, '55af672c6fa0b11.PNG', NULL),
	(4, 2, 'jd12.jpg', NULL),
	(12, 8, 'jd1.jpg', 'jd'),
	(13, 10, '04142011figure_a.jpg', ''),
	(14, 15, 'abced1.png', 'fd'),
	(18, 18, 'jd3.jpg', ''),
	(24, 18, '55af672c6fa0b.PNG', '');
/*!40000 ALTER TABLE `ys_productImages` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_products
DROP TABLE IF EXISTS `ys_products`;
CREATE TABLE IF NOT EXISTS `ys_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `manufacture` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` varchar(50) NOT NULL,
  `metric` varchar(50) DEFAULT NULL,
  `description` longtext NOT NULL,
  `quantity` int(11) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_88E5E895989D9B62` (`slug`),
  KEY `IDX_88E5E89512469DE2` (`category_id`),
  KEY `IDX_88E5E895F603EE73` (`vendor_id`),
  CONSTRAINT `FK_88E5E89512469DE2` FOREIGN KEY (`category_id`) REFERENCES `ys_categories` (`id`),
  CONSTRAINT `FK_88E5E895F603EE73` FOREIGN KEY (`vendor_id`) REFERENCES `ys_vendors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_products: ~18 rows (approximately)
DELETE FROM `ys_products`;
/*!40000 ALTER TABLE `ys_products` DISABLE KEYS */;
INSERT INTO `ys_products` (`id`, `category_id`, `vendor_id`, `manufacture`, `name`, `price`, `metric`, `description`, `quantity`, `slug`, `status`) VALUES
	(1, 2, 1, 'ccccdfddf', 'u', 'xxxx', 'xxx', 'xxx', 23, 'sdsdsssssssssss', 0),
	(2, 2, 2, '', 'a', '', '', '', 45, 'sdsd', 1),
	(3, 2, 1, '', 'b', '34f', '', '', 0, 'rbk', 1),
	(4, 1, 1, '', 'c', 'df', NULL, 'fddfdf', 0, 'dffdf', 1),
	(5, 3, 1, 'dddd', 'd', '', NULL, '', 8, 'bhakta', 1),
	(6, 2, 1, '', 'e', 'dfdff', NULL, '', 0, 'dfdfdfdf', 1),
	(7, 3, 1, 'sdsd', 'f', 'ssd', 'sdsdsd', 'sdsd', 78, 'sdksjdsdsd', 1),
	(8, 2, 3, 'dfddf', 'g', 'dfdf', 'matric', 'sdsd', 126, 'water', 1),
	(9, 3, 1, 'dfd', 'h', 'dfdf', '', '', 34, 'dfdf-1', 1),
	(10, 3, 2, 'fdf', 'i', '', '', '', 0, 'dfd', 0),
	(11, 3, 2, '', 'j', '', '', '', 0, 'dsd', 1),
	(12, 6, 1, 'dfdf', 'k', '45', '', '', 0, 'dfdf-2', 1),
	(13, 2, 2, 'ddf', 'a', '', '', '', 0, 'a', 1),
	(14, 3, 2, 'sd', 'y', '', '', '', 0, 'y', 1),
	(15, 2, 4, 'CG', 'rusrus', '445', '4', 'latest product', 6, 'rusrus', 1),
	(16, 3, 3, '', 'asas', '', '', '', 0, 'asas', 1),
	(17, 3, 3, '', 'asas', '', '', '', 0, 'asas-1', 1),
	(18, 3, 3, 'abcedfghddfdfdfdf', 'abcd', '', '', '', 0, 'abcd', 1);
/*!40000 ALTER TABLE `ys_products` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_sessions
DROP TABLE IF EXISTS `ys_sessions`;
CREATE TABLE IF NOT EXISTS `ys_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table boozeMe.ys_sessions: ~0 rows (approximately)
DELETE FROM `ys_sessions`;
/*!40000 ALTER TABLE `ys_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ys_sessions` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_users
DROP TABLE IF EXISTS `ys_users`;
CREATE TABLE IF NOT EXISTS `ys_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `salt` varchar(255) NOT NULL,
  `secrete` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `api_key` varchar(6) NOT NULL,
  `first_login` tinyint(1) NOT NULL,
  `pwd_change_on` datetime DEFAULT NULL,
  `last_logged` datetime DEFAULT NULL,
  `resetToken` varchar(50) DEFAULT NULL,
  `tokenRequested` datetime DEFAULT NULL,
  `tokenUsed` datetime DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_24C477B2E7927C74` (`email`),
  UNIQUE KEY `UNIQ_24C477B2C912ED9D` (`api_key`),
  KEY `IDX_24C477B2F92F3E70` (`country_id`),
  KEY `IDX_24C477B2FE54D947` (`group_id`),
  KEY `IDX_24C477B2F603EE73` (`vendor_id`),
  CONSTRAINT `FK_24C477B2F603EE73` FOREIGN KEY (`vendor_id`) REFERENCES `ys_vendors` (`id`),
  CONSTRAINT `FK_24C477B2F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `ys_countries` (`id`),
  CONSTRAINT `FK_24C477B2FE54D947` FOREIGN KEY (`group_id`) REFERENCES `ys_groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_users: ~24 rows (approximately)
DELETE FROM `ys_users`;
/*!40000 ALTER TABLE `ys_users` DISABLE KEYS */;
INSERT INTO `ys_users` (`id`, `country_id`, `group_id`, `salt`, `secrete`, `token`, `fullname`, `city`, `address`, `phone`, `mobile`, `email`, `status`, `created`, `api_key`, `first_login`, `pwd_change_on`, `last_logged`, `resetToken`, `tokenRequested`, `tokenUsed`, `vendor_id`) VALUES
	(1, 156, 1, '72yzaLYC0OmKQFdCw1xObJB26siBdPMS', 'KN9u5BGLbsWHS8ox0BP/9YTHeXRpQoq9', '83dd3cfba1afc87a5bff2536299d55ed', 'Petition Superadmin', 'Lalitpur', 'Kopundole', '5520464', '9841000000', 'a', 1, '2015-01-22 08:18:14', '899375', 0, '2015-09-20 10:32:25', '2015-09-21 03:52:59', NULL, NULL, NULL, NULL),
	(2, 156, 2, 'VLLcTqQj/c/b/pCtnuKl8o+V0cIdek0E', 'Z3RmXz/PL6RPl8pYW5ruXRLLGmoIiFXN', '19ec9ef60ffa0453950accda55c3dc20', 'Pravesh Mishra', 'Kathmandu', 'maharajgunj', '', '9841979051', 'pmetal@gmail.com', 1, '2015-08-26 16:47:12', 'e5f257', 1, '2015-08-26 16:47:12', '2015-08-26 16:56:06', NULL, NULL, NULL, NULL),
	(7, NULL, 2, 'BjpYO3D4YEkVbNkdwfTDO3hr+Mkct4d2', 'qH1KM5WeI7C9hYulsd6gXDcdJu6GhApv', '5c299124ef4a3bc548606756057a4a9a', 'Dddd', NULL, '', NULL, '', 'dddd', 1, '2015-09-07 04:12:19', 'c84675', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, NULL, 2, 'fcrLGVufotz8o+yG8symFa0Ef8FOBNpN', 'xXv4MHBJp+hkYTwTojohOv68HBtQZ/DU', '958fda7fb2958daf81e18e5e54951008', 'Cc', NULL, '', NULL, '', 'cc', 1, '2015-09-07 04:17:08', '1585ac', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, NULL, 2, '4XCtkPqvIPV09HpFs08GvSrnHOSiCfBz', 'bKnQDp1qw7DlFj6qmlUxQkkqficPlqbR', '6b795a515b36904ae3bc3c38c1b61d61', 'Nn', NULL, '', NULL, '', 'nn', 1, '2015-09-07 04:48:15', '391b4e', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, NULL, 2, '+7+qnsyW1Z8Fb1+rzcAaAvYn6tQlhYRu', 'Vnzb1csufcp2po8Fnjrzu3KHPS+j1yrw', '56f50938ed96513152e018fa9c4d2a08', 'Sdsd', NULL, '', NULL, '', 'sd', 1, '2015-09-07 05:05:23', '107e87', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, NULL, 2, 'xiy4or2X+FkR2Wqiqj3C170l/OAXnkXl', 'l6gjKpuObjXMt6qGOFpRWA+8XDGccwRU', '91fe5d1003b73ac0f2b54bbc1280e201', 'C``', NULL, '', NULL, '', 'bb', 1, '2015-09-07 06:32:47', '0eb5d5', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, NULL, 2, 'Biij6LEAyZ33HBs0ey1GDuFa6734EEZD', 'qosADrpIjP3aU7YKAxqR5s9F30mFnG6s', '618284bd801c2422414f4e44e54c945f', 'San', NULL, '', NULL, '', 'sann', 1, '2015-09-07 09:27:06', '7ed615', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, NULL, 2, 'opHF/gGZeFl02mNKGluNt5xnCf8CA1Yt', '7Hq5NOoIgA2GErWKIHIMqnyvGMsrDMyR', '0f3ac2a20a7a7fc56c3f58cc428759fa', 'New', NULL, '', NULL, '', 'dfdf', 1, '2015-09-07 10:03:48', 'f09e7c', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, NULL, 2, 'uOxNf4TOIKYlBxMiPFuji8uCzCkeiPKf', 'szHMDSQ82YPYD0Ls1CGBiFMITHriuLrU', 'c441646f971849a1fcca8f46dc8cf82b', 'Ttbb', NULL, '', NULL, '', 'ttbb', 1, '2015-09-08 05:15:40', 'a8f56b', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, NULL, 2, 'EtBQyoFIRiNfFHOu/TyAgSX1wMy+6d5b', 'qORNRyjROpMv4GKFuQzzAUr8E+yids2c', 'c71ab30c072d792adb12e542d8de1d8e', 'Ja', NULL, '', NULL, '', 'jam', 1, '2015-09-08 05:41:45', 'ded6ec', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, NULL, 2, 'QtpLHHns/Utwo+bGmN3yh2W1gh6DxZdt', '4vUzL4YaEZ5nzoNMALQA4ghmhyeH5XBi', 'fd55831ea715ce9eb4e3dff76da98988', 'Ddf', NULL, '', NULL, 'dfd', 'ktm@gmail.com', 1, '2015-09-08 09:01:55', '100a3c', 1, NULL, '2015-09-08 10:14:52', NULL, NULL, NULL, NULL),
	(22, NULL, 2, 'SfLlqIgsF/5yoH/E9euFkaDOACzAXzAX', 'mnte6LROC9p2GCEBa0FfqdEd+n805sQO', 'b9df9fe6c273f7c1ce01b60c227ecec7', 'Dd', NULL, '', NULL, 'sdsd', 'dfdf@dfdf', 1, '2015-09-08 09:32:04', '2cbdfc', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, NULL, 2, '3iihuIhAAqSlwN/IP5NWfyX2gkc9UaQB', '5poXcrEhoC+v4zyo4tgVxIDR52ZPRTNr', 'a20dcd76c12629384a2be167209fea92', 'Dfd', NULL, '', NULL, '23', 'df@gmail.com', 1, '2015-09-08 09:54:37', 'b2bac7', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, NULL, 2, '6MXIALshKUFTKTsP605CbQVAV5fiTLwK', 'M2yzFgK8aBdp8QIStMNaWcD0IoneMWVI', '476e278b7e19c0ed204f29cfb8b80ae4', 'Ram', NULL, '', NULL, '3434', 'ram@gmail.com', 1, '2015-09-08 10:17:06', '4329db', 1, NULL, '2015-09-08 10:25:20', NULL, NULL, NULL, NULL),
	(25, NULL, 2, 'X6B51riRSPEl0OlK7KqkuXLFYrZYGzZd', '08jbX9O8a6uRQEZWnYIUw1wpHmTK0fdB', 'e4f440698ab57809488dd768535b9c48', 'Eeee', NULL, '', NULL, '334', 'eeee@gmail.com', 1, '2015-09-08 10:32:50', '1462e5', 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, NULL, 2, '44feJofAEc4p4hsVVCdg3egOTdh4vpLB', 'AUOV/BfdgC3pK10r1OkKRzia78X+ZwKJ', '4841e0fc1032aa78f8a70a1740b4c057', 'Aaaa', NULL, '', NULL, '323', 'a@gmail.com', 1, '2015-09-08 11:21:13', '980d7e', 1, NULL, '2015-09-12 12:58:07', NULL, NULL, NULL, 1),
	(28, NULL, 2, 'jQkSzFf1rAoCEBsDeYTyIqqdowQOa/3A', 'BpeKyYpXg2Bf7hNQ+asXwmBFBWoPtnCD', '33a3da2b6130dd42d1ec618eac758128', 'D', NULL, '', NULL, '344', 'd@gmail.com', 1, '2015-09-11 04:19:20', '3599b1', 0, '2015-09-20 10:17:13', '2015-09-20 10:15:22', NULL, NULL, NULL, 2),
	(29, NULL, 2, 'GkdkEF+E5hzQ5QllwTeEsiXoGBPFuVzs', 'ZjrX/NHI46m0r0B/CbR9GGbPXyMjzmLu', '47729fc8a9119b8a616128765f6b83f8', 'E', NULL, '', NULL, '34343', 'e@gmail.com', 1, '2015-09-11 04:21:13', '505db1', 1, NULL, '2015-09-11 11:23:51', NULL, NULL, NULL, 3),
	(30, 2, 3, 'FscNnlHTbLoN/QnFjNEL6QHe2WDxmL3U', 'OudRKcSAsPW3pdfP5FtoizqDqXW2l4g6', '79ddd57551f530c628ff6ab54a7bc859', 'Dfd', 'fdf', 'dfdf', '3434', '434', 'fdf@dfdf.com', 1, '2015-09-11 04:48:01', 'f3d0c1', 1, '2015-09-11 04:48:01', '2015-09-11 04:48:01', NULL, NULL, NULL, NULL),
	(31, NULL, 2, 'B5WJ5aJY0NWZXCKV1tC1SZbH5SjHoP7K', '2cIrx0rtV00dKPB1Gy2fEpokhWeiphEp', '4018903a80f4248821854e0174106d0d', 'Dfdf', NULL, '', NULL, '3434', 'ab@gmail.com', 1, '2015-09-15 05:30:48', '474cbb', 1, NULL, NULL, NULL, NULL, NULL, 4),
	(32, NULL, 2, 'jvEBI3kRdNu6s5M12QQZWwUtgzi/Tv0w', 'DkoeWq2oT4n/T3IetNXbSAM/ErBkgTZC', '6adf6dda9f61551bf6920437f8b212cb', 'Naya', NULL, '', NULL, '34343434', 'naya@gmail.com', 1, '2015-09-18 10:00:48', 'f38231', 1, NULL, '2015-09-18 10:01:09', NULL, NULL, NULL, 5),
	(35, NULL, 2, '2ICIuWeyPhgZJLPY24grbbzDKwQxM90L', 'VL/Dqy9jVu226T0wDdtI/FO+TkiwsDCq', 'c12dc428261188cb589eae9cabaed8e0', 'Latest Vendor', NULL, 'jamal', NULL, '8444844', 'lv@kdfjdf.com', 1, '2015-09-20 06:47:24', 'c0b579', 1, NULL, NULL, NULL, NULL, NULL, 6),
	(40, NULL, 2, 'Xm2AkRW8fGGZatE2Q4Xizi90NTrQZcG4', 'XjnnnxyODIcbf6C/0w0sxD6JBLOUGXUF', '8154898a696a055cb5890e5418170675', 'Dabaga', NULL, 'dkfj', NULL, '334', 'dabaga@gmail.com', 1, '2015-09-20 06:56:11', '310465', 1, NULL, NULL, NULL, NULL, NULL, 7);
/*!40000 ALTER TABLE `ys_users` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_vendors
DROP TABLE IF EXISTS `ys_vendors`;
CREATE TABLE IF NOT EXISTS `ys_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessName` varchar(255) NOT NULL,
  `vatBill` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone1` varchar(50) DEFAULT NULL,
  `phone2` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `status` varchar(50) NOT NULL,
  `requestedDate` datetime NOT NULL,
  `approvedDate` datetime NOT NULL,
  `approvedBy` varchar(100) NOT NULL,
  `updatedDate` datetime NOT NULL,
  `slug` varchar(50) NOT NULL,
  `reason` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7EB7BD31989D9B62` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_vendors: ~8 rows (approximately)
DELETE FROM `ys_vendors`;
/*!40000 ALTER TABLE `ys_vendors` DISABLE KEYS */;
INSERT INTO `ys_vendors` (`id`, `businessName`, `vatBill`, `email`, `phone1`, `phone2`, `address`, `status`, `requestedDate`, `approvedDate`, `approvedBy`, `updatedDate`, `slug`, `reason`) VALUES
	(1, 'aaaa', 'qw', 'a@gmail.com', '323', '', '', 'DELETED', '2015-09-08 11:20:22', '2015-09-08 11:20:22', '', '2015-09-20 04:52:33', 'aaaa', 'dds'),
	(2, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'ACTIVE', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 06:19:50', 'dd', 'sdsd'),
	(3, 'e', 'kdfj', 'e@gmail.com', '34343', '', '', 'ACTIVE', '2015-09-11 04:21:05', '2015-09-11 04:21:05', '', '2015-09-20 06:36:21', 'e', 'sd'),
	(4, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'ACTIVE', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-20 07:05:00', 'dfdf', 'df'),
	(5, 'naya', 'dkfjdf', 'naya@gmail.com', '34343434', '', '', 'ACTIVE', '2015-09-18 09:07:28', '2015-09-18 09:07:28', '', '2015-09-20 06:15:54', 'naya', 'kjk'),
	(6, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'ACTIVE', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 07:11:01', 'latest-vendor', 'sdsd'),
	(7, 'Dabaga', 'rerer', 'dabaga@gmail.com', '334', '434', 'dkfj', 'ACTIVE', '2015-09-20 06:55:03', '2015-09-20 06:55:03', '', '2015-09-20 07:10:30', 'dabaga', 'sdsd'),
	(8, 'sdsd', 'sdsd', 'adf@gmail.com', '323', '', 'dfd', 'NEW', '2015-09-20 08:32:08', '2015-09-20 08:32:08', '', '2015-09-20 08:32:08', 'sdsd', NULL);
/*!40000 ALTER TABLE `ys_vendors` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_vendors_audit
DROP TABLE IF EXISTS `ys_vendors_audit`;
CREATE TABLE IF NOT EXISTS `ys_vendors_audit` (
  `id` int(11) NOT NULL,
  `rev` int(11) NOT NULL,
  `businessName` varchar(255) DEFAULT NULL,
  `vatBill` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone1` varchar(50) DEFAULT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `address` longtext,
  `status` varchar(50) DEFAULT NULL,
  `requestedDate` datetime DEFAULT NULL,
  `approvedDate` datetime DEFAULT NULL,
  `approvedBy` varchar(100) DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `revtype` varchar(4) NOT NULL,
  `reason` longtext,
  PRIMARY KEY (`id`,`rev`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_vendors_audit: ~115 rows (approximately)
DELETE FROM `ys_vendors_audit`;
/*!40000 ALTER TABLE `ys_vendors_audit` DISABLE KEYS */;
INSERT INTO `ys_vendors_audit` (`id`, `rev`, `businessName`, `vatBill`, `email`, `phone1`, `phone2`, `address`, `status`, `requestedDate`, `approvedDate`, `approvedBy`, `updatedDate`, `slug`, `revtype`, `reason`) VALUES
	(1, 28, 'Ktm', '3456864', 'ktm@gmail.com', '234567', '', '', 'NEW', '2015-09-08 08:56:37', '2015-09-08 08:56:36', '', '2015-09-08 08:56:37', 'ktm', 'INS', NULL),
	(1, 35, 'Ktm', '3456864', 'ktm@gmail.com', '234567', '', '', 'ACTIVE', '2015-09-08 08:56:37', '2015-09-08 08:56:36', '', '2015-09-08 09:10:00', 'ktm', 'UPD', 's'),
	(1, 40, 'dsd', 'sdsd', 'ktm@gmail.com', '455', '', '', 'NEW', '2015-09-08 09:47:23', '2015-09-08 09:47:22', '', '2015-09-08 09:47:23', 'dsd', 'INS', NULL),
	(1, 51, 'dsd', 'sdsd', 'ktm@gmail.com', '455', '', '', 'BLOCKED', '2015-09-08 09:47:23', '2015-09-08 09:47:22', '', '2015-09-08 10:30:44', 'dsd', 'UPD', 'sdsd'),
	(1, 57, 'aaaa', 'qw', 'a@gmail.com', '323', '', '', 'NEW', '2015-09-08 11:20:22', '2015-09-08 11:20:22', '', '2015-09-08 11:20:22', 'aaaa', 'INS', NULL),
	(1, 58, 'aaaa', 'qw', 'a@gmail.com', '323', '', '', 'ACTIVE', '2015-09-08 11:20:22', '2015-09-08 11:20:22', '', '2015-09-08 11:20:48', 'aaaa', 'UPD', 'sdsd'),
	(1, 59, 'aaaa', 'qw', 'a@gmail.com', '323', '', '', 'BLOCKED', '2015-09-08 11:20:22', '2015-09-08 11:20:22', '', '2015-09-08 11:33:30', 'aaaa', 'UPD', 'fdf'),
	(1, 60, 'aaaa', 'qw', 'a@gmail.com', '323', '', '', 'ACTIVE', '2015-09-08 11:20:22', '2015-09-08 11:20:22', '', '2015-09-08 11:44:32', 'aaaa', 'UPD', 'sd'),
	(1, 73, 'aaaa', 'qw', 'a@gmail.com', '323', '', '', 'DELETED', '2015-09-08 11:20:22', '2015-09-08 11:20:22', '', '2015-09-20 04:52:33', 'aaaa', 'UPD', 'dds'),
	(2, 29, 'bbt', 'dfdf', 'dfdf', '3434', '', '', 'NEW', '2015-09-08 08:57:02', '2015-09-08 08:57:02', '', '2015-09-08 08:57:02', 'bbt', 'INS', NULL),
	(2, 34, 'bbt', 'dfdf', 'dfdf', '3434', '', '', 'ACTIVE', '2015-09-08 08:57:02', '2015-09-08 08:57:02', '', '2015-09-08 09:09:32', 'bbt', 'UPD', 's'),
	(2, 41, 'dfd', 'dfdf', 'dmk@gmail.com', '334', '', '', 'NEW', '2015-09-08 09:47:36', '2015-09-08 09:47:36', '', '2015-09-08 09:47:36', 'dfd', 'INS', NULL),
	(2, 42, 'dfd', 'dfdf', 'ktm@gmail.com', '334', '', '', 'NEW', '2015-09-08 09:47:36', '2015-09-08 09:48:05', '', '2015-09-08 09:48:05', 'dfd', 'UPD', NULL),
	(2, 56, 'dfd', 'dfdf', 'ktm@gmail.com', '334', '', '', 'ACTIVE', '2015-09-08 09:47:36', '2015-09-08 09:48:05', '', '2015-09-08 10:42:52', 'dfd', 'UPD', 'ddsd'),
	(2, 61, 'D', 'dkfj', 'dkfjd@dkfjdf.com', '344', '', '', 'NEW', '2015-09-11 04:18:40', '2015-09-11 04:18:40', '', '2015-09-11 04:18:40', 'd', 'INS', NULL),
	(2, 62, 'D', 'dkfj', 'd@gmail.com', '344', '', '', 'NEW', '2015-09-11 04:18:40', '2015-09-11 04:19:14', '', '2015-09-11 04:19:14', 'd', 'UPD', NULL),
	(2, 63, 'D', 'dkfj', 'd@gmail.com', '344', '', '', 'ACTIVE', '2015-09-11 04:18:40', '2015-09-11 04:19:14', '', '2015-09-11 04:19:19', 'd', 'UPD', 'fddf'),
	(2, 66, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'ACTIVE', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-11 08:58:18', 'dd', 'UPD', 'fddf'),
	(2, 74, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'BLOCKED', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 04:52:56', 'dd', 'UPD', 'sdsd'),
	(2, 75, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'ACTIVE', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 04:53:06', 'dd', 'UPD', 'ccc'),
	(2, 76, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'BLOCKED', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 05:09:39', 'dd', 'UPD', 'dsd'),
	(2, 77, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'ACTIVE', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 05:09:45', 'dd', 'UPD', 'sdsd'),
	(2, 80, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'DELETED', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 05:32:58', 'dd', 'UPD', 'sdsd'),
	(2, 87, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'DELETED', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 06:11:18', 'dd', 'UPD', 'sdsd'),
	(2, 92, 'Dd', 'dkfj', 'd@gmail.com', '344', '', '', 'DELETED', '2015-09-11 04:18:40', '2015-09-11 08:58:18', '', '2015-09-20 06:19:50', 'dd', 'UPD', 'sdsd'),
	(3, 30, 'sdsd', 'sdsd', 'ktm@gmail.com', 'dff', '', '', 'NEW', '2015-09-08 08:58:08', '2015-09-08 08:58:08', '', '2015-09-08 08:58:08', 'sdsd', 'INS', NULL),
	(3, 33, 'sdsd', 'sdsd', 'ktm@gmail.com', 'dff', '', '', 'ACTIVE', '2015-09-08 08:58:08', '2015-09-08 08:58:08', '', '2015-09-08 09:09:06', 'sdsd', 'UPD', 'd'),
	(3, 43, 'sds', 'sdsd', 'dmk@gmail.com', '112', '', '', 'NEW', '2015-09-08 09:49:43', '2015-09-08 09:49:43', '', '2015-09-08 09:49:43', 'sds', 'INS', NULL),
	(3, 64, 'e', 'kdfj', 'e@gmail.com', '34343', '', '', 'NEW', '2015-09-11 04:21:05', '2015-09-11 04:21:05', '', '2015-09-11 04:21:05', 'e', 'INS', NULL),
	(3, 65, 'e', 'kdfj', 'e@gmail.com', '34343', '', '', 'ACTIVE', '2015-09-11 04:21:05', '2015-09-11 04:21:05', '', '2015-09-11 04:21:12', 'e', 'UPD', 'sd'),
	(3, 81, 'e', 'kdfj', 'e@gmail.com', '34343', '', '', 'DELETED', '2015-09-11 04:21:05', '2015-09-11 04:21:05', '', '2015-09-20 05:42:35', 'e', 'UPD', 'sd'),
	(3, 84, 'e', 'kdfj', 'e@gmail.com', '34343', '', '', 'DELETED', '2015-09-11 04:21:05', '2015-09-11 04:21:05', '', '2015-09-20 06:06:16', 'e', 'UPD', 'sd'),
	(3, 88, 'e', 'kdfj', 'e@gmail.com', '34343', '', '', 'DELETED', '2015-09-11 04:21:05', '2015-09-11 04:21:05', '', '2015-09-20 06:11:26', 'e', 'UPD', 'sd'),
	(3, 94, 'e', 'kdfj', 'e@gmail.com', '34343', '', '', 'DELETED', '2015-09-11 04:21:05', '2015-09-11 04:21:05', '', '2015-09-20 06:36:21', 'e', 'UPD', 'sd'),
	(4, 11, 'cg', '', 'cg', '', '', '', 'BLOCKED', '2015-09-06 17:36:07', '2015-09-06 17:36:07', '', '2015-09-08 04:33:59', 'cg', 'UPD', 'ddd'),
	(4, 12, 'cg', '', 'cg', '', '', '', 'ACTIVE', '2015-09-06 17:36:07', '2015-09-06 17:36:07', '', '2015-09-08 04:35:21', 'cg', 'UPD', 'ffdf'),
	(4, 13, 'cg', '', 'cg', '', '', '', 'NEW', '2015-09-06 17:36:07', '2015-09-06 17:36:07', '', '2015-09-08 04:38:59', 'cg', 'UPD', 'sd'),
	(4, 14, 'cg', '', 'cg', '', '', '', 'ACTIVE', '2015-09-06 17:36:07', '2015-09-06 17:36:07', '', '2015-09-08 04:39:19', 'cg', 'UPD', 'cc'),
	(4, 31, 'ddf', 'dfdf', 'ktm@gmail.com', 'dfd', '', '', 'NEW', '2015-09-08 08:59:49', '2015-09-08 08:59:49', '', '2015-09-08 08:59:49', 'ddf', 'INS', NULL),
	(4, 32, 'ddf', 'dfdf', 'ktm@gmail.com', 'dfd', '', '', 'ACTIVE', '2015-09-08 08:59:49', '2015-09-08 08:59:49', '', '2015-09-08 09:01:55', 'ddf', 'UPD', 'd'),
	(4, 44, 'dfd', 'df', 'df@gmail.com', '23', '', '', 'NEW', '2015-09-08 09:53:02', '2015-09-08 09:53:02', '', '2015-09-08 09:53:02', 'dfd-1', 'INS', NULL),
	(4, 45, 'dfd', 'df', 'df@gmail.com', '23', '', '', 'NEW', '2015-09-08 09:53:02', '2015-09-08 09:53:42', '', '2015-09-08 09:53:42', 'dfd-1', 'UPD', NULL),
	(4, 46, 'dfd', 'df', 'df@gmail.com', '23', '', '', 'ACTIVE', '2015-09-08 09:53:02', '2015-09-08 09:53:42', '', '2015-09-08 09:54:37', 'dfd-1', 'UPD', 'c'),
	(4, 67, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'NEW', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-15 05:30:39', 'dfdf', 'INS', NULL),
	(4, 68, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'ACTIVE', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-15 05:30:48', 'dfdf', 'UPD', 'df'),
	(4, 82, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'DELETED', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-20 05:44:55', 'dfdf', 'UPD', 'df'),
	(4, 86, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'DELETED', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-20 06:08:26', 'dfdf', 'UPD', 'df'),
	(4, 89, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'DELETED', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-20 06:11:42', 'dfdf', 'UPD', 'df'),
	(4, 95, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'DELETED', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-20 06:43:56', 'dfdf', 'UPD', 'df'),
	(4, 110, 'dfdf', 'dfdf', 'ab@gmail.com', '3434', '', '', 'DELETED', '2015-09-15 05:30:39', '2015-09-15 05:30:39', '', '2015-09-20 07:05:00', 'dfdf', 'UPD', 'df'),
	(5, 3, 'xxxx', '', 'xxxv', '', '', '', 'ACTIVE', '2015-09-06 17:41:35', '2015-09-07 09:19:12', '', '2015-09-07 09:19:12', 'xxxx', 'UPD', ''),
	(5, 4, 'xxxxf', '', 'xxxv', '', '', '', 'ACTIVE', '2015-09-06 17:41:35', '2015-09-07 09:21:12', '', '2015-09-07 09:21:12', 'xxxxf', 'UPD', ''),
	(5, 36, 'sdsd', 'df', 'ktm@gmail.com', 'df', '', '', 'NEW', '2015-09-08 09:12:56', '2015-09-08 09:12:56', '', '2015-09-08 09:12:56', 'sdsd-1', 'INS', NULL),
	(5, 47, 'ram', 'ram', 'ram@gmail.com', '3434', '3434', '', 'NEW', '2015-09-08 10:16:06', '2015-09-08 10:16:06', '', '2015-09-08 10:16:06', 'ram', 'INS', NULL),
	(5, 48, 'ram', 'ram', 'ram@gmail.com', '3434', '3434', '', 'BLOCKED', '2015-09-08 10:16:06', '2015-09-08 10:16:06', '', '2015-09-08 10:16:48', 'ram', 'UPD', 'ssd'),
	(5, 49, 'ram', 'ram', 'ram@gmail.com', '3434', '3434', '', 'ACTIVE', '2015-09-08 10:16:06', '2015-09-08 10:16:06', '', '2015-09-08 10:17:05', 'ram', 'UPD', 'ddf'),
	(5, 50, 'ram', 'ram', 'ram@gmail.com', '3434', '3434', '', 'BLOCKED', '2015-09-08 10:16:06', '2015-09-08 10:16:06', '', '2015-09-08 10:18:41', 'ram', 'UPD', 'df'),
	(5, 69, 'naya', 'dkfjdf', 'naya@gmail.com', '34343434', '', '', 'NEW', '2015-09-18 09:07:28', '2015-09-18 09:07:28', '', '2015-09-18 09:07:28', 'naya', 'INS', NULL),
	(5, 70, 'naya', 'dkfjdf', 'naya@gmail.com', '34343434', '', '', 'ACTIVE', '2015-09-18 09:07:28', '2015-09-18 09:07:28', '', '2015-09-18 10:00:47', 'naya', 'UPD', 'kjk'),
	(5, 83, 'naya', 'dkfjdf', 'naya@gmail.com', '34343434', '', '', 'DELETED', '2015-09-18 09:07:28', '2015-09-18 09:07:28', '', '2015-09-20 06:01:39', 'naya', 'UPD', 'kjk'),
	(5, 85, 'naya', 'dkfjdf', 'naya@gmail.com', '34343434', '', '', 'DELETED', '2015-09-18 09:07:28', '2015-09-18 09:07:28', '', '2015-09-20 06:08:05', 'naya', 'UPD', 'kjk'),
	(5, 90, 'naya', 'dkfjdf', 'naya@gmail.com', '34343434', '', '', 'DELETED', '2015-09-18 09:07:28', '2015-09-18 09:07:28', '', '2015-09-20 06:15:54', 'naya', 'UPD', 'kjk'),
	(6, 15, 'dddd', '', 'dddd', '', '', '', 'BLOCKED', '2015-09-07 04:12:19', '2015-09-07 04:12:19', '', '2015-09-08 04:52:48', 'dddd', 'UPD', 'dfdf'),
	(6, 16, 'dddd', '', 'dddd', '', '', '', 'ACTIVE', '2015-09-07 04:12:19', '2015-09-07 04:12:19', '', '2015-09-08 05:07:40', 'dddd', 'UPD', 'dsdsddfdf'),
	(6, 17, 'dddd', '', 'dddd', '', '', '', 'BLOCKED', '2015-09-07 04:12:19', '2015-09-07 04:12:19', '', '2015-09-08 05:07:51', 'dddd', 'UPD', 's'),
	(6, 18, 'dddd', '', 'dddd', '', '', '', 'ACTIVE', '2015-09-07 04:12:19', '2015-09-07 04:12:19', '', '2015-09-08 05:08:04', 'dddd', 'UPD', 'sdsd'),
	(6, 37, 'dd', 'dsd', 'dfdf@dfdf', 'sdsd', '', '', 'NEW', '2015-09-08 09:15:36', '2015-09-08 09:15:36', '', '2015-09-08 09:15:36', 'dd', 'INS', NULL),
	(6, 38, 'dd', 'dsd', 'dfdf@dfdf', 'sdsd', '', '', 'ACTIVE', '2015-09-08 09:15:36', '2015-09-08 09:15:36', '', '2015-09-08 09:32:04', 'dd', 'UPD', 'f'),
	(6, 52, 'eeee', 'dsdsd', 'eeee@gmail.com', '334', '42', '', 'NEW', '2015-09-08 10:32:38', '2015-09-08 10:32:38', '', '2015-09-08 10:32:38', 'eeee', 'INS', NULL),
	(6, 53, 'eeee', 'dsdsd', 'eeee@gmail.com', '334', '42', '', 'ACTIVE', '2015-09-08 10:32:38', '2015-09-08 10:32:38', '', '2015-09-08 10:32:50', 'eeee', 'UPD', 'c'),
	(6, 54, 'eeee', 'dsdsd', 'eeee@gmail.com', '334', '42', '', 'BLOCKED', '2015-09-08 10:32:38', '2015-09-08 10:32:38', '', '2015-09-08 10:33:01', 'eeee', 'UPD', 'x'),
	(6, 55, 'eeee', 'dsdsd', 'eeee@gmail.com', '334', '42', '', 'DELETED', '2015-09-08 10:32:38', '2015-09-08 10:32:38', '', '2015-09-08 10:42:05', 'eeee', 'UPD', 'ddf'),
	(6, 71, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', '', 'NEW', '2015-09-20 04:29:24', '2015-09-20 04:29:24', '', '2015-09-20 04:29:24', 'latest-vendor', 'INS', NULL),
	(6, 72, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'NEW', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 04:37:15', 'latest-vendor', 'UPD', NULL),
	(6, 78, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'BLOCKED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 05:16:05', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 79, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'DELETED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 05:17:28', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 91, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'DELETED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:16:14', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 93, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'DELETED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:34:28', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 96, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'BLOCKED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:47:02', 'latest-vendor', 'UPD', 'dsdsd'),
	(6, 97, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'ACTIVE', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:47:24', 'latest-vendor', 'UPD', 'dsdsd'),
	(6, 98, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'BLOCKED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:47:34', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 99, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'ACTIVE', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:47:43', 'latest-vendor', 'UPD', 'sdsdsd'),
	(6, 100, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'BLOCKED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:49:47', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 101, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'ACTIVE', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:49:54', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 102, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'BLOCKED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:50:16', 'latest-vendor', 'UPD', 'sd'),
	(6, 103, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'ACTIVE', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:50:21', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 104, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'BLOCKED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:52:12', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 105, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'ACTIVE', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 06:52:18', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 112, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'DELETED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 07:07:26', 'latest-vendor', 'UPD', 'sdsd'),
	(6, 114, 'Latest Vendor', '994vf9', 'lv@kdfjdf.com', '8444844', '', 'jamal', 'DELETED', '2015-09-20 04:29:24', '2015-09-20 04:37:15', '', '2015-09-20 07:11:01', 'latest-vendor', 'UPD', 'sdsd'),
	(7, 8, 'cc', '', 'cc', '', '', '', 'BLOCKED', '2015-09-07 04:17:08', '2015-09-07 04:17:08', '', '2015-09-08 04:07:08', 'cc', 'UPD', 'dfdddf'),
	(7, 9, 'cc', '', 'cc', '', '', '', 'NEW', '2015-09-07 04:17:08', '2015-09-07 04:17:08', '', '2015-09-08 04:08:27', 'cc', 'UPD', 'cyfyfjghfyfy'),
	(7, 10, 'cc', '', 'cc', '', '', '', 'ACTIVE', '2015-09-07 04:17:08', '2015-09-07 04:17:08', '', '2015-09-08 04:10:41', 'cc', 'UPD', 'bnbnbnbn'),
	(7, 27, 'cc', '', 'cc', '', '', '', 'BLOCKED', '2015-09-07 04:17:08', '2015-09-07 04:17:08', '', '2015-09-08 08:26:15', 'cc', 'UPD', 'sdsd'),
	(7, 39, 'sdsd', 'sdsd', 'sdsd', '323', 'sdsdsd', '', 'NEW', '2015-09-08 09:43:57', '2015-09-08 09:43:57', '', '2015-09-08 09:43:57', 'sdsd-2', 'INS', NULL),
	(7, 106, 'Dabaga', 'rerer', 'dabaga@gmail.com', '334', '434', 'dkfj', 'NEW', '2015-09-20 06:55:03', '2015-09-20 06:55:03', '', '2015-09-20 06:55:03', 'dabaga', 'INS', NULL),
	(7, 107, 'Dabaga', 'rerer', 'dabaga@gmail.com', '334', '434', 'dkfj', 'ACTIVE', '2015-09-20 06:55:03', '2015-09-20 06:55:03', '', '2015-09-20 06:56:11', 'dabaga', 'UPD', 'sdsd'),
	(7, 108, 'Dabaga', 'rerer', 'dabaga@gmail.com', '334', '434', 'dkfj', 'BLOCKED', '2015-09-20 06:55:03', '2015-09-20 06:55:03', '', '2015-09-20 06:56:17', 'dabaga', 'UPD', 'sdsd'),
	(7, 109, 'Dabaga', 'rerer', 'dabaga@gmail.com', '334', '434', 'dkfj', 'ACTIVE', '2015-09-20 06:55:03', '2015-09-20 06:55:03', '', '2015-09-20 06:56:23', 'dabaga', 'UPD', 'sdsd'),
	(7, 111, 'Dabaga', 'rerer', 'dabaga@gmail.com', '334', '434', 'dkfj', 'DELETED', '2015-09-20 06:55:03', '2015-09-20 06:55:03', '', '2015-09-20 07:05:37', 'dabaga', 'UPD', 'sdsd'),
	(7, 113, 'Dabaga', 'rerer', 'dabaga@gmail.com', '334', '434', 'dkfj', 'DELETED', '2015-09-20 06:55:03', '2015-09-20 06:55:03', '', '2015-09-20 07:10:30', 'dabaga', 'UPD', 'sdsd'),
	(8, 115, 'sdsd', 'sdsd', 'adf@gmail.com', '323', '', 'dfd', 'NEW', '2015-09-20 08:32:08', '2015-09-20 08:32:08', '', '2015-09-20 08:32:08', 'sdsd', 'INS', NULL),
	(10, 1, 'c``', '', 'bb', '', '', '', 'NEW', '2015-09-07 06:32:47', '2015-09-07 06:32:47', '', '2015-09-07 06:32:47', 'c-1', 'INS', NULL),
	(10, 2, 'c``', '', 'bb', '', '', '', 'ACTIVE', '2015-09-07 06:32:47', '2015-09-07 06:32:47', '', '2015-09-07 06:48:22', 'c-1', 'UPD', NULL),
	(11, 5, 'san', '', 'sann', '', '', '', 'NEW', '2015-09-07 09:27:06', '2015-09-07 09:27:06', '', '2015-09-07 09:27:06', 'san', 'INS', NULL),
	(11, 7, 'san', '', 'sann', '', '', '', 'ACTIVE', '2015-09-07 09:27:06', '2015-09-07 09:27:06', '', '2015-09-07 12:07:18', 'san', 'UPD', 'dfdfdf'),
	(12, 6, 'new', '', 'dfdf', '', '', '', 'NEW', '2015-09-07 10:03:48', '2015-09-07 10:03:48', '', '2015-09-07 10:03:48', 'new', 'INS', NULL),
	(12, 20, 'new', '', 'dfdf', '', '', '', 'ACTIVE', '2015-09-07 10:03:48', '2015-09-07 10:03:48', '', '2015-09-08 05:21:52', 'new', 'UPD', 'ss'),
	(13, 19, 'ttbb', '', 'ttbb', '', '', '', 'NEW', '2015-09-08 05:15:40', '2015-09-08 05:15:40', '', '2015-09-08 05:15:40', 'ttbb', 'INS', NULL),
	(13, 22, 'ttbb', '', 'ttbb', '', '', '', 'ACTIVE', '2015-09-08 05:15:40', '2015-09-08 05:15:40', '', '2015-09-08 05:37:53', 'ttbb', 'UPD', 'dfd'),
	(14, 21, 'Ratna', '', 'aka', '', '', '', 'NEW', '2015-09-08 05:29:35', '2015-09-08 05:29:35', '', '2015-09-08 05:29:35', 'ratna', 'INS', NULL),
	(14, 23, 'Ratna', '', 'aka', '', '', '', 'ACTIVE', '2015-09-08 05:29:35', '2015-09-08 05:29:35', '', '2015-09-08 05:39:39', 'ratna', 'UPD', 'sdsd'),
	(15, 24, 'ja', '', 'jam', '', '', '', 'NEW', '2015-09-08 05:41:28', '2015-09-08 05:41:28', '', '2015-09-08 05:41:28', 'ja', 'INS', NULL),
	(15, 25, 'ja', '', 'jam', '', '', '', 'ACTIVE', '2015-09-08 05:41:28', '2015-09-08 05:41:28', '', '2015-09-08 05:41:45', 'ja', 'UPD', 'sdsd'),
	(15, 26, 'ja', '', 'jam', '', '', '', 'DELETED', '2015-09-08 05:41:28', '2015-09-08 05:41:28', '', '2015-09-08 05:45:53', 'ja', 'UPD', 'ddfdf');
/*!40000 ALTER TABLE `ys_vendors_audit` ENABLE KEYS */;


-- Dumping structure for table boozeMe.ys_vendor_contact_persons
DROP TABLE IF EXISTS `ys_vendor_contact_persons`;
CREATE TABLE IF NOT EXISTS `ys_vendor_contact_persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `designation` varchar(50) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `contact1` varchar(50) NOT NULL,
  `contact2` varchar(50) NOT NULL,
  `email1` varchar(50) NOT NULL,
  `email2` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_75767359F603EE73` (`vendor_id`),
  CONSTRAINT `FK_75767359F603EE73` FOREIGN KEY (`vendor_id`) REFERENCES `ys_vendors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table boozeMe.ys_vendor_contact_persons: ~1 rows (approximately)
DELETE FROM `ys_vendor_contact_persons`;
/*!40000 ALTER TABLE `ys_vendor_contact_persons` DISABLE KEYS */;
INSERT INTO `ys_vendor_contact_persons` (`id`, `vendor_id`, `designation`, `fullName`, `contact1`, `contact2`, `email1`, `email2`, `address`, `status`, `deleted`) VALUES
	(1, 2, 'PM', 'Ratna bdr katuwal', '343434', '', 'a@gmail.cm', '', '3r3', 1, 0);
/*!40000 ALTER TABLE `ys_vendor_contact_persons` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
