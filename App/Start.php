<?php
/*
|=====================================================================================================
| Framework initial configuration
|=====================================================================================================
| you can change according to your use
|
|
*/

require_once 'Config/App.php';
require_once 'Config/Twig.View.php';
require_once 'Config/Database.php';
require_once 'Config/TwigCustom/TwigFilter.php';
require_once 'Config/TwigCustom/TwigFunction.php';
require_once 'Config/ErrorHandler.php';
require_once 'Config/Modules.php';
require_once 'Modules/Routes.php';

//Location:: Application/Start.php