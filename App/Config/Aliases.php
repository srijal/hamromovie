<?php
$config['aliases'] = array(

    'App'      => 'SlimFacades\App',
    'Config'   => 'SlimFacades\Config',
    'Input'    => 'SlimFacades\Input',
    'Log'      => 'SlimFacades\Log',
    'Request'  => 'SlimFacades\Request',
    'Response' => 'SlimFacades\Response',
    'Route'    => 'SlimFacades\Route',
    'View'     => 'SlimFacades\View',
    'DB'       => 'SlimFacades\DB'
);