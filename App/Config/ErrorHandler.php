<?php
// DEFAULT NotFound and Error redirect
App::notFound(function () {
    View::display('@error/404.html');
});
App::error(function (\Exception $e) {
    $data['error']= $e->getMessage();
    View::display('@error/error.html',$data);
});