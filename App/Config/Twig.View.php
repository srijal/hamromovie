<?php
/*
|=====================================================================================================
| Framework initial configuration
|=====================================================================================================
| you can change according to your use
|
|
*/

// CONFIGURE TWIG VIEW
App::view()->parserOptions = array(

    'debug'             => (APP_MODE=='development')?true:false,
    'charset'           => 'utf-8',
    'cache'             => APP_FOLDER . '/Storage/cache/tpl/',
    'auto_reload'       => (APP_MODE=='development')?true:false,
    'strict_variables'  => false,
    'autoescape'        => false
);


// APPEND DATA TO VIEW
$rootUri= App::request()->getRootUri();
App::view()->appendData(array(

    'App'               => $app,
    'RootUri'           => $rootUri,
    'AssetUri'          => $rootUri,
    'BaseUrl'           => $rootUri,
    'ResourceUri'       => $_SERVER['REQUEST_URI']

));
// Location:: Application/Config/Twig.View.php