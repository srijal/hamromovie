<?php

namespace Site\Controller {

    use App;
    use DB;
    use Input;
    use Request;
    use Response;
    use Site\Model\Home as HomeModel;
    use SlimFacades\Session;
    use View;

    class Home
    {
        public function Index()
        {

            $data = [];
            $data['page']="home";

            $data['bannerData'] 		= HomeModel::GetBanner();
            list($latest1, $latest2, $latest3) 	= HomeModel::LatestMovie();
            $data['latestMovies'] 	= HomeModel::LatestMovies();

            $session = new Session();

            $showPopUp = TRUE;

            if( ! isset($_SESSION['showPopup']) or $_SESSION['showPopup'] === TRUE ){
                $showPopUp = TRUE;
                $_SESSION['showPopup'] = FALSE;
            }else{
                $showPopUp = FALSE;
                $_SESSION['showPopup'] = FALSE;
            }

            $data['showPopup'] = $showPopUp;
            $data['latest1'] = $latest1;
            $data['latest2'] = $latest2;
            $data['latest3'] = $latest3;
            
            $data['playlists'] = HomeModel::GetPlayList();

            View::display('@Site/home/index.twig', $data);

        }
    }
}