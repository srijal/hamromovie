<?php
/**
 * Created by PhpStorm.
 * User: Bidhee
 * Date: 4/8/2015
 * Time: 11:27 AM
 */

namespace Site\Controller;

use App;
use DB;
use Input;
use Request;
use Response;
use Site\Model\Video as VideoModel;
use View;
use Pagination;
use Session;
use Site\Model\Home as HomeModel;

class Video
{

    public function Detail($url=0, $video_id=0)
    {
        $data = [];
        $result = VideoModel::GetVideoById($url,$video_id);
        VideoModel::UpdateView($video_id);
	     
        if ($result) {
             $session = New Session();
	     $data['logged'] = $session->getSession('SiteloggedIn');
    	
	    	if($data['logged'] == true){
		    	
			$user_id = $session->getSession('id');
			
		    	$data['subscriptionCheck'] = VideoModel::CheckSubscription($video_id,$user_id);
	    	
	    	}
            $data['row'] = $result;
            
        } else {
            Response::redirect(App::urlFor('home')); 
        }
        
        $session->setSession([
	        'returnUrl'=>Request::getUrl().Request::getResourceUri()
	        ]);
	if($session->getSession('SiteloggedIn') ==true){
		$data['returnUrlSet']= 1;
	}else{
		$data['returnUrlSet']= '';
	}

	$data['related']= VideoModel::RelatedVideo($result['movieType'], $video_id );
        View::display('@Site/video/video-detail.twig', $data);
    }

    public function LatestAll(){

        try {

            $limit = 15;
            $start = 0;

            $p = Input::get('p');
            isset($p) ? $page = preg_replace("/[^0-9]/", " ", trim($p)) : $page = 1;

            $start = ($page - 1) * $limit;

            $sql = "SELECT

                        m.id as mid,
                        m.title as title,
                        m.image as image,
                        m.details as details,
                        m.trailer_link  as trailerLink,
                        m.full_link  as fullLink,
                        m.price  as price,
                        m.movie_type  as movieType,
                        m.movie_tag  as movieTag,
                        m.cast  as cast,
                        m.director  as director,
                        m.music  as music,
                        m.language  as language,
                        m.subtitle  as subtitle,
                        m.viewed  as view,
                        m.url  as url

                    FROM
                        ramro_movie as m

                    RIGHT JOIN

                        ramro_movie_category as c

                    ON
                        m.cat_id = c.id

                    WHERE
                        m.status=1

                       ";

            // COUNT RECORDS
            DB::query($sql, '', true);
            $data['totalRecords'] = $count = count(DB::fetch_assoc_all());

            // DEFAULT ASSIGN ORDER
            $sql .= " ORDER BY m.id DESC LIMIT $start, $limit";

            // FINAL QUERY FOR THE RESULT
            DB::query($sql, '', true);
            $data['results'] = DB::fetch_assoc_all();

            if (!$data['results']) {
                Response::redirect(App::urlFor('home'));
            }
            // PAGINATION RESULT AND LINK
            $pg = new Pagination();
            $data['pagination'] = $pg->create_links($limit, $page, App::urlfor('latest-all') . '?p=', $count);

        } catch (ResourceNotFoundException $e) {

            $data['errMsg'] = $e->getMessage();
        }

        // ASSIGN PAGE TO FORM TO RESUBMIT
        $data['page'] = $page;

        View::display('@Site/video/view-all.twig', $data);
    }

    public function MostViewedMovie(){

        try {

            $limit = 15;
            $start = 0;

            $p = Input::get('p');
            isset($p) ? $page = preg_replace("/[^0-9]/", " ", trim($p)) : $page = 1;

            $start = ($page - 1) * $limit;

            $sql = "SELECT

                        m.id as mid,
                        m.title as title,
                        m.image as image,
                        m.details as details,
                        m.trailer_link  as trailerLink,
                        m.full_link  as fullLink,
                        m.price  as price,
                        m.movie_type  as movieType,
                        m.movie_tag  as movieTag,
                        m.cast  as cast,
                        m.director  as director,
                        m.music  as music,
                        m.language  as language,
                        m.subtitle  as subtitle,
                        m.viewed  as view,
                        m.url  as url

                    FROM
                        ramro_movie as m

                    RIGHT JOIN

                        ramro_movie_category as c

                    ON
                        m.cat_id = c.id

                    WHERE
                        m.status=1

                       ";

            // COUNT RECORDS
            DB::query($sql, '', true);
            $data['totalRecords'] = $count = count(DB::fetch_assoc_all());

            // DEFAULT ASSIGN ORDER
            $sql .= " ORDER BY m.viewed DESC LIMIT $start, $limit";

            // FINAL QUERY FOR THE RESULT
            DB::query($sql, '', true);
            $data['results'] = DB::fetch_assoc_all();

            if (!$data['results']) {
                Response::redirect(App::urlFor('home'));
            }
            // PAGINATION RESULT AND LINK
            $pg = new Pagination();
            $data['pagination'] = $pg->create_links($limit, $page, App::urlfor('most-viewed') . '?p=', $count);

        } catch (ResourceNotFoundException $e) {

            $data['errMsg'] = $e->getMessage();
        }

        // ASSIGN PAGE TO FORM TO RESUBMIT
        $data['page'] = $page;

        View::display('@Site/video/most-viewed.twig', $data);
    }
    
    // HOME
    public function Search()
        {

            $hasResults = TRUE;

        try {

            $limit = 15;
            $start = 0;

            $p = Input::get('p');
            isset($p) ? $page = preg_replace("/[^0-9]/", " ", trim($p)) : $page = 1;
            
            // DEFINE SEARCH QUERY
            $q = preg_replace('/[^A-Za-z0-9 ]/', ' ', Input::params('q'));

            $start = ($page - 1) * $limit;

            $sql = "SELECT

                        m.id as mid,
                        m.title as title,
                        m.image as image,
                        m.details as details,
                        m.trailer_link  as trailerLink,
                        m.full_link  as fullLink,
                        m.price  as price,
                        m.movie_type  as movieType,
                        m.movie_tag  as movieTag,
                        m.cast  as cast,
                        m.director  as director,
                        m.music  as music,
                        m.language  as language,
                        m.subtitle  as subtitle,
                        m.viewed  as view,
                        m.url  as url

                    FROM
                        ramro_movie as m

                    RIGHT JOIN

                        ramro_movie_category as c

                    ON
                        m.cat_id = c.id

                    WHERE
                        m.status=1
                    AND
                        m.title LIKE ?
                    OR
                        c.title LIKE ?

                       ";

            // COUNT RECORDS
            DB::query($sql,array('%'.$q.'%','%'.$q.'%'), true);
            $data['totalRecords'] = $count = count(DB::fetch_assoc_all());

            // DEFAULT ASSIGN ORDER
            $sql .= " ORDER BY m.id DESC LIMIT $start, $limit";

            // FINAL QUERY FOR THE RESULT
            DB::query($sql,array('%'.$q.'%','%'.$q.'%'), true);
            $data['results'] = DB::fetch_assoc_all();

            if (!$data['results']) {
                $hasResults = FALSE;
//                Response::redirect(App::urlFor('home'));
            }else{
                // PAGINATION RESULT AND LINK
                $pg = new Pagination();
                $data['pagination'] = $pg->create_links($limit, $page, App::urlfor('search') .'?q='.$q. '&p=', $count);
            }


        } catch (ResourceNotFoundException $e) {

            $hasResults = FALSE;
            $data['errMsg'] = $e->getMessage();
        }

            if($hasResults){
                // ASSIGN PAGE TO FORM TO RESUBMIT
                $data['page'] = $page;

                View::display('@Site/video/search.twig', $data);
            }else{
                // ASSIGN PAGE TO FORM TO RESUBMIT
                $data['query_param'] = $q;
                $data['recommendations'] = HomeModel::GetSubPlaylist(3);
                View::display('@Site/video/no-video.twig', $data);
            }


        }

}