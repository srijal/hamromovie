<?php

namespace Site\Controller;

use App;
use DB;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Google_Client;
use Google_Oauth2Service;
use Google_Service_Drive;
use Input;
use Request;
use Response;
use Session;
use Validate;
use View;
use Site\Model\User as UserModel;

//use Facebook\FacebookResponse;
//use Facebook\FacebookSDKException;
//use Facebook\FacebookAuthorizationException;
//use Facebook\GraphObject;
//use Facebook\Entities\AccessToken;
//use Facebook\HttpClients\FacebookCurlHttpClient;
//use Facebook\HttpClients\FacebookHttpable;


class User
{

    public function GoogleLogin()
    {


        $data = [];
        $session = new Session();
        $returnUrl = $session->getSession('returnUrl');// return to page if exist

        try {

            $client = new Google_Client();
            $client->setApplicationName('Login to Hamro Movie.');
            $client->setClientId(GOOGLE_CLIENT_ID);
            $client->setClientSecret(GOOGLE_CLIENT_SECRET);
            $client->setScopes([
                "https://www.googleapis.com/auth/plus.login",
                "https://www.googleapis.com/auth/userinfo.email",
                "https://www.googleapis.com/auth/userinfo.profile",
                "https://www.googleapis.com/auth/plus.me"
            ]);

            $redirectUrl = BASE_URL . App::urlFor('gplus-login');

            $client->setDeveloperKey(GOOGLE_DEVELOPERS_KEY);
            $client->setRedirectUri($redirectUrl);

            $oauth = new Google_Oauth2Service($client);

            //If user wish to log out, we just unset Session variable
            if (isset($_REQUEST['reset'])) {
                unset($_SESSION['token']);
                $client->revokeToken();
                $this->site_redirect($redirectUrl);
            }

            //If code is empty, redirect user to google authentication page for code.
            //Code is required to aquire Access Token from google
            //Once we have access token, assign token to session variable
            //and we can redirect user back to page and login.
            if (isset($_GET['code'])) {
                $client->authenticate($_GET['code']);
                $_SESSION['token'] = $client->getAccessToken();
                $this->site_redirect($redirectUrl);
                return;
            }

            if (isset($_SESSION['token'])) {
                $client->setAccessToken($_SESSION['token']);
            }

            if ($client->getAccessToken()) {

                $user = $oauth->userinfo->get();
                $social_id = $user['id'];
                $full_name = filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
                $first_name = filter_var($user['given_name'], FILTER_SANITIZE_SPECIAL_CHARS);
                $last_name = filter_var($user['family_name'], FILTER_SANITIZE_SPECIAL_CHARS);
                $gender = filter_var($user['gender'], FILTER_SANITIZE_SPECIAL_CHARS);
                $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
                $profile_image_url = filter_var($user['picture'], FILTER_VALIDATE_URL);
                $_SESSION['token'] = $client->getAccessToken();

                $res = UserModel::GetUserBySocialMedia($social_id, 'google');

                if (!$res) {
                    $rawData = [
                        'full_name' => $full_name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'social_id' => $social_id,
                        'email' => $email,
                        'social_media' => 'google',
                        'role' => 3,
                        'gender' => $gender,
                        'created_at' => CUR_TIME,
                        'updated_at' => CUR_TIME,
                        'image' => $profile_image_url
                    ];

                    UserModel::SaveUser($rawData);
                }

                $row = UserModel::GetUserBySocialMedia($social_id, 'google');

                if ($row) {
                    $session->setSession([
                        'id' => $row['id'],
                        'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                        'fname' => $row['first_name'],
                        'lname' => $row['last_name'],
                        'gender' => ($row['gender'] != '') ? $row['gender'] : 'male',
                        'socialId' => $row['social_id'],
                        'socialMedia' => $row['social_media'],
                        'role' => $row['role'],
                        'SiteloggedIn' => true,
                        'profilePic' => $row['image'],
                        'email'=> $row['email']
                    ]);

                    if ($returnUrl != '') {
                        $this->site_redirect($returnUrl);
                    } else {
                        Response::redirect(App::urlFor('home'));
                    }

                } else {
                    $data['errMsg'] = 'Something went wrong!!!. Please try Again Later';
                    View::display('@Site/user/login.twig', $data);
                }

            } else {
                $auth_url = $client->createAuthUrl();
                $this->site_redirect($auth_url);
            }

        } catch (\Google_Exception $ge) {

            $data['errMsg'] = $ge->getMessage();
            View::display('@Site/user/login.twig', $data);
        }


    }

    public function site_redirect($url)
    {
        $filteredUrl = filter_var($url, FILTER_SANITIZE_URL);
        echo "<script> window.location.href='" . $filteredUrl . "'; </script>";
    }


    public function Login()
    {
        $data = [];
        $session = New Session();
        $logged = $session->getSession('SiteloggedIn');
        if ($logged == true) {
            Response::redirect(App::urlFor('home'));
        }
//        $redirect_url = 'http://ramro-movie.bidhee.net/login';
        $redirect_url = BASE_URL . '/login';
        $returnUrl = $session->getSession('returnUrl');// return to page if exist

        /*
         *====================================================================
         * Facebook Signup
         *===================================================================
         */
//        FacebookSession::setDefaultApplication('1469909969950661', '55298232f49504649ed19f7665620545');
        FacebookSession::setDefaultApplication('1664959030393029', '1ab53e8a6943d70b5cf1ef7726b5f19c');
//        FacebookSession::setDefaultApplication(FB_APP_ID, FB_APP_SECRET);
        $helper = new FacebookRedirectLoginHelper($redirect_url);

        try {

            $facebookSession = $helper->getSessionFromRedirect();
            if (isset($facebookSession)) {

                $request = new FacebookRequest($facebookSession, 'GET', '/me');
                $response = $request->execute();
                $graphObject = $response->getGraphObject();

                $social_id = $graphObject->getProperty('id');
                $full_name = $graphObject->getProperty('name');
                $first_name = $graphObject->getProperty('first_name');
                $last_name = $graphObject->getProperty('last_name');
                $gender = $graphObject->getProperty('gender');

                $res = UserModel::GetUserBySocialMedia($social_id, 'facebook');
                if (!$res) {

                    $rawData = [

                        'full_name' => $full_name,
                        'first_name' => ($first_name != '') ? $first_name : $full_name,
                        'last_name' => ($last_name != '') ? $last_name : $full_name,
                        'social_id' => $social_id,
                        'social_media' => 'facebook',
                        'role' => 3,
                        'gender' => ($gender != '') ? $gender : 'male',
                        'created_at' => CUR_TIME,
                        'updated_at' => CUR_TIME,

                    ];

                    UserModel::SaveUser($rawData);
                    $row = UserModel::GetUserBySocialMedia($social_id, 'facebook');

                    $session->setSession([

                        'id' => $row['id'],
                        'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                        'fname' => $row['first_name'],
                        'lname' => $row['last_name'],
                        'gender' => $row['gender'],
                        'socialId' => $row['social_id'],
                        'socialMedia' => $row['social_media'],
                        'role' => $row['role'],
                        'SiteloggedIn' => true

                    ]);

                    if ($returnUrl != '') {
                        echo "<script>
				    window.opener.location.href='" . $returnUrl . "';
		            	    self.close();
		            
		           	</script>";

                    } else {
                        echo "<script>
			            window.close();
			    	    window.opener.location.reload();
			         </script>";
                    }


                } else {

                    $row = UserModel::GetUserBySocialMedia($social_id, 'facebook');

                    $session->setSession([
                        'id' => $row['id'],
                        'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                        'fname' => $row['first_name'],
                        'lname' => $row['last_name'],
                        'socialId' => $row['social_id'],
                        'socialMedia' => $row['social_media'],
                        'role' => $row['role'],
                        'SiteloggedIn' => true
                    ]);

                    if ($returnUrl != '') {
                        echo "<script>
				    window.opener.location.href='" . $returnUrl . "';
		            	    self.close();
		            
		           </script>";
                    } else {
                        echo "<script>
			            window.close();
			    	    window.opener.location.reload();
			         </script>";
                    }

                }

            } else {
                $data['fbLoginUrl'] = $helper->getLoginUrl();
            }
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
            $data['errMsg'] = "Facebook error try again later!";
        } catch (Exception $ex) {
            // When validation fails or other local issues
            $data['errMsg'] = "Facebook validation error try again!";
        }


        /*
         *====================================================================
         * Normal Signup
         *===================================================================
         */

        if (sizeof(Input::post()) > 0) {

            $input = (object)Input::post();


            $data['v'] = Validate::Instance(); //OBJECT VALIDATE
            Validate::Email($input->email, $displayName = 'Email', $req = TRUE);
            Validate::Str($input->passwd, $displayName = 'Password', $req = TRUE);

            if (Validate::IsFine()) {

                $userId = UserModel::CheckUser($input->email, md5($input->passwd));

                if ($userId) {
                    $row = UserModel::GetUserById($userId);

                    $session = New Session();
                    $session->setSession([
                        'id' => $row['id'],
                        'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                        'fname' => $row['first_name'],
                        'lname' => $row['last_name'],
                        'socialMedia' => $row['social_media'],
                        'role' => $row['role'],
                        'SiteloggedIn' => true,
                        'email' => $row['email']
                    ]);
                    if ($returnUrl != '') {
                        Response::redirect($returnUrl);
                    } else {
                        Response::redirect(App::urlFor('home'));
                    }

                } else {

                    $data['errMsg'] = "Invalid Access";
                }

            }
        }

        View::display('@Site/user/login.twig', $data);

    }

    // LOGOUT
    public function Logout()
    {
        unset($_SESSION);
        $_SESSION = array();
        session_destroy();
        Response::redirect(App::urlFor('home'));
    }

    public function Signup()
    {

        $data = [];
        $session = New Session();
        $logged = $session->getSession('SiteloggedIn');
        if ($logged == true) {
            Response::redirect(App::urlFor('home'));
        }

        $redirect_url = BASE_URL . '/signup';
        $returnUrl = $session->getSession('returnUrl');// return to page if exist

        /*
         *====================================================================
         * Facebook Signup
         *===================================================================
         */
        FacebookSession::setDefaultApplication('1469909969950661', '55298232f49504649ed19f7665620545');
        $helper = new FacebookRedirectLoginHelper($redirect_url);

        try {
            $facebookSession = $helper->getSessionFromRedirect();
            if (isset($facebookSession)) {

                $request = new FacebookRequest($facebookSession, 'GET', '/me');
                $response = $request->execute();
                $graphObject = $response->getGraphObject();

                $social_id = $graphObject->getProperty('id');
                $full_name = $graphObject->getProperty('name');
                $first_name = $graphObject->getProperty('first_name');
                $last_name = $graphObject->getProperty('last_name');
                $gender = $graphObject->getProperty('gender');

                $res = UserModel::GetUserBySocialMedia($social_id, 'facebook');
                if (!$res) {

                    $rawData = [

                        'full_name' => $full_name,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'social_id' => $social_id,
                        'social_media' => 'facebook',
                        'role' => 3,
                        'gender' => $gender,
                        'created_at' => CUR_TIME,
                        'updated_at' => CUR_TIME,

                    ];

                    UserModel::SaveUser($rawData);
                    $row = UserModel::GetUserBySocialMedia($social_id, 'facebook');

                    $session->setSession([

                        'id' => $row['id'],
                        'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                        'fname' => $row['first_name'],
                        'lname' => $row['last_name'],
                        'gender' => $row['gender'],
                        'socialId' => $row['social_id'],
                        'socialMedia' => $row['social_media'],
                        'role' => $row['role'],
                        'SiteloggedIn' => true

                    ]);

                    if ($returnUrl != '') {
                        echo "<script>
					    window.opener.location.href='" . $returnUrl . "';
			            	    self.close();
			            
			           </script>";
                    } else {
                        echo "<script>
				            window.close();
				    	    window.opener.location.reload();
				         </script>";
                    }

                } else {

                    $row = UserModel::GetUserBySocialMedia($social_id, 'facebook');

                    $session->setSession([
                        'id' => $row['id'],
                        'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                        'fname' => $row['first_name'],
                        'lname' => $row['last_name'],
                        'socialId' => $row['social_id'],
                        'socialMedia' => $row['social_media'],
                        'role' => $row['role'],
                        'SiteloggedIn' => true
                    ]);

                    if ($returnUrl != '') {
                        echo "<script>
					    window.opener.location.href='" . $returnUrl . "';
			            	    self.close();
			            
			           </script>";
                    } else {
                        echo "<script>
				            window.close();
				    	    window.opener.location.reload();
				         </script>";
                    }

                }

            } else {
                $data['fbLoginUrl'] = $helper->getLoginUrl();
            }
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
            $data['errMsg'] = "Facebook error try again later!";
        } catch (Exception $ex) {
            // When validation fails or other local issues
            $data['errMsg'] = "Facebook validation error try again!";
        }

        /*
         *====================================================================
         * Normal Signup
         *===================================================================
         */
        if (sizeof(Input::post()) > 1) {

            $input = (object)Input::post();
            $data['v'] = Validate::Instance(); //OBJECT VALIDATE

            Validate::Email($input->email, $displayName = 'Email', $req = TRUE);
            Validate::Char($input->firstname, $displayName = 'First Name', $req = TRUE);
            Validate::Char($input->lastname, $displayName = 'Last Name', $req = TRUE);
            Validate::Str($input->passwd, $displayName = 'Password', $req = TRUE);

            if (Validate::IsFine()) {

                if (!UserModel::CheckEmail($input->email)) {

                    $rawData = [

                        'full_name' => $input->firstname . " " . $input->lastname,
                        'first_name' => $input->firstname,
                        'last_name' => $input->lastname,
                        'email' => $input->email,
                        'pass' => md5($input->passwd),
                        'social_media' => 'normal',
                        'role' => 3,
                        'created_at' => CUR_TIME,
                        'updated_at' => CUR_TIME,

                    ];

                    $user_id = UserModel::SaveUser($rawData);
                    $row = UserModel::GetUserById($user_id);

                    $session = New Session();
                    $session->setSession([
                        'id' => $row['id'],
                        'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                        'fname' => $row['first_name'],
                        'lname' => $row['last_name'],
                        'socialMedia' => $row['social_media'],
                        'role' => $row['role'],
                        'SiteloggedIn' => true,
                        'email' => $row['email']
                    ]);

                    if ($returnUrl != '') {
                        Response::redirect($returnUrl);
                    } else {
                        Response::redirect(App::urlFor('home'));
                    }

                } else {

                    $data['errMsg'] = "Email already Exists";
                }

            }
        }

        View::display('@Site/user/signup.twig', $data);
    }

    // PROFILE
    public function Profile()
    {
        $data = [];

        if( !isset($_SESSION['id']) or $_SESSION['id'] == '' ){
            Response::redirect(App::urlFor('home'));
        }

        if (sizeof(Input::post()) > 1) {

            $input = (object)Input::post();
            $data['v'] = Validate::Instance(); //OBJECT VALIDATE

            Validate::Email($input->email, $displayName = 'Email', $req = TRUE);
            Validate::Char($input->firstname, $displayName = 'First Name', $req = TRUE);
            Validate::Char($input->lastname, $displayName = 'Last Name', $req = TRUE);
//            Validate::Str($input->passwd, $displayName = 'Password', $req = TRUE);

            if ($input->user_id != "") {
                if (Validate::IsFine()) {

                    if ($input->old_email == $input->email or !UserModel::CheckEmail($input->email)) {

                        $id = $input->user_id;

                        $rawData = [

                            'full_name' => $input->firstname . " " . $input->lastname,
                            'first_name' => $input->firstname,
                            'last_name' => $input->lastname,
                            'email' => $input->email,
                            'updated_at' => CUR_TIME,

                        ];

                        if (UserModel::updateUser($rawData, $id)) {

                            $row = UserModel::GetUserById($id);

                            $session = New Session();
                            $session->setSession([
                                'id' => $row['id'],
                                'fullname' => $row['first_name'] . ' ' . $row['last_name'],
                                'fname' => $row['first_name'],
                                'lname' => $row['last_name'],
                                'socialMedia' => $row['social_media'],
                                'role' => $row['role'],
                                'SiteloggedIn' => true,
                                'email' => $row['email']
                            ]);
                        } else {
                            $data['errMsg'] = "Unable to update your profile. Please try again later.";
                        }

                        Response::redirect(App::urlFor('user-profile'));

                    } else {

                        $data['errMsg'] = "Email already Exists";
                    }

                }else{
//                    $data['errMsg'] = "Unable to process your request. Please try again later.";
                    $data['errMsg'] = Validate::ErrorList();
                }
            }


        }


        View::display('@Site/user/profile.twig', $data);
    }

    // PASSWORD
    public function Password()
    {
        $enc = new \Encryption();
        $media = $enc->decode($_SESSION['socialMedia']);
//        echo $media;
//
//        print_r($_SESSION['socialMedia']);
//        die;
        if( !isset($_SESSION['id']) or $_SESSION['id'] == '' or $media != 'normal'){
            Response::redirect(App::urlFor('home'));
        }

        $data = [];


        View::display('@Site/user/password.twig', $data);
    }

    public function FacebookLoginTest()
    {

        $data = [];
        FacebookSession::setDefaultApplication('1469909969950661', '55298232f49504649ed19f7665620545');
        $helper = new FacebookRedirectLoginHelper('http://ramro-movie.bidhee.net/facebook');

        try {
            $session = $helper->getSessionFromRedirect();
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch (Exception $ex) {
            // When validation fails or other local issues
        }

        if (isset($session)) {

            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            $graphObject = $response->getGraphObject();

            $data['fb'] = [

                'id' => $graphObject->getProperty('id'),
                'username' => $graphObject->getProperty('username'),
                'name' => $graphObject->getProperty('name'),
                'first_name' => $graphObject->getProperty('first_name'),
                'last_name' => $graphObject->getProperty('last_name'),
                'birthday' => $graphObject->getProperty('birthday'),
                'email' => $graphObject->getProperty('email'),
                'gender' => $graphObject->getProperty('gender')

            ];

        }

        // show login url
        $data['loginUrl'] = $helper->getLoginUrl();
        View::display('@Site/user/login.twig', $data);

    }

    public function GoogleLoginTest()
    {

        $google_client_id = '12167756767-aulcp5enrcancljj72m2opepv9argfpm.apps.googleusercontent.com';
        $google_client_secret = 'YGG-xLfiWjmSZ63U7mBtJyjw';
        $google_redirect_url = 'http://ramro-movie.bidhee.net/login'; //path to your script
        $google_developer_key = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx';

        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to ramro-movie.com');
        $gClient->setClientId($google_client_id);
        $gClient->setClientSecret($google_client_secret);
        $gClient->setRedirectUri($google_redirect_url);
        //$gClient->setDeveloperKey($google_developer_key);

        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_REQUEST['reset'])) {
            unset($_SESSION['token']);
            $gClient->revokeToken();
            header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
        }

        if (isset($_GET['code'])) {
            $gClient->authenticate($_GET['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
            return;
        }


        if (isset($_SESSION['token'])) {
            $gClient->setAccessToken($_SESSION['token']);
        }


        if ($gClient->getAccessToken()) {
            //For logged in user, get details from google using access token
            $user = $google_oauthV2->userinfo->get();
            $user_id = $user['id'];
            $user_name = filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
            $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
            $profile_url = filter_var($user['link'], FILTER_VALIDATE_URL);
            $profile_image_url = filter_var($user['picture'], FILTER_VALIDATE_URL);
            $personMarkup = "$email<div><img src='$profile_image_url?sz=50'></div>";
            $_SESSION['token'] = $gClient->getAccessToken();
        } else {
            //For Guest user, get google login url
            $authUrl = $gClient->createAuthUrl();
        }

        //HTML page start
        echo '<!DOCTYPE HTML><html>';
        echo '<head>';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<title>Login with Google</title>';
        echo '</head>';
        echo '<body>';
        echo '<h1>Login with Google</h1>';

        if (isset($authUrl)) //user is not logged in, show login button
        {
            echo '<a class="login" href="' . $authUrl . '">Google Login</a>';
        } else // user logged in
        {
            //list all user details
            echo '<br /><a class="logout" href="?reset=1">Logout</a>';
            echo "<img src='" . $user['picture'] . "'><br/>";
            echo '<pre>';
            print_r($user);
            echo '</pre>';
        }
        echo '</body></html>';
    }

}