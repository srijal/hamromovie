<?php
/**
 * Created by PhpStorm.
 * User: Bidhee
 * Date: 4/5/2015
 * Time: 5:18 PM
 */

namespace Site\Model;

use DB;
class Home
{

    // Banner Data
    public static function GetBanner()
    {
        $sql = "
            SELECT
                title,details,movie_type,link,image
            FROM
                ramro_movie_banner
            WHERE
                status=1
            ORDER BY
                sort_order
            ASC
        ";
        DB::query($sql,'',true);
        $row = DB::fetch_assoc_all();
        if ($row) {
            return $row;
        } else {
            return false;
        }
    }
    // Banner Data
    public static function LatestMovie()
    {
        $sql1 = "
            SELECT

                m.id as mid,
                m.title as title,
                m.image as image,
                m.details as details,
                m.trailer_link  as trailerLink,
                m.full_link  as fullLink,
                m.price  as price,
                m.movie_type  as movieType,
                m.movie_tag  as movieTag,
                m.cast  as cast,
                m.director  as director,
                m.music  as music,
                m.language  as language,
                m.subtitle  as subtitle,
                m.viewed  as view,
                m.url  as url

            FROM
                ramro_movie as m

            RIGHT JOIN

                ramro_movie_category as c

            ON
                m.cat_id = c.id

            WHERE
                m.status=1 ";

        $sql2 = " ORDER BY
                        m.id
                    DESC

                    LIMIT 5
                ";
        DB::query($sql1.$sql2,'',true);
        $latest1 = DB::fetch_assoc_all();
        $latest1Ids = self::GetAllIds($latest1);

        $sqlMid1 = " AND m.id NOT IN({$latest1Ids})";
        DB::query($sql1.$sqlMid1.$sql2,'',true);
        $latest2 = DB::fetch_assoc_all();
        $latest2Ids = self::GetAllIds($latest2);

        $allIds =$latest1Ids.','.$latest2Ids;
        $sqlMid2 = " AND m.id NOT IN({$allIds})";
        DB::query($sql1.$sqlMid2.$sql2,'',true);
        $latest3 = DB::fetch_assoc_all();

        return [$latest1,$latest2,$latest3];

    }

    public static function LatestMovies()
    {
        $sql1 = "
            SELECT

                m.id as mid,
                m.title as title,
                m.image as image,
                m.details as details,
                m.trailer_link  as trailerLink,
                m.full_link  as fullLink,
                m.price  as price,
                m.movie_type  as movieType,
                m.movie_tag  as movieTag,
                m.cast  as cast,
                m.director  as director,
                m.music  as music,
                m.language  as language,
                m.subtitle  as subtitle,
                m.viewed  as view,
                m.url  as url

            FROM
                ramro_movie as m

            RIGHT JOIN

                ramro_movie_category as c

            ON
                m.cat_id = c.id

            WHERE
                m.status=1 ";

        $sql2 = " ORDER BY
                        m.id
                    DESC
                    LIMIT 15
                ";
        DB::query($sql1.$sql2,'',true);

        return DB::fetch_assoc_all();


    }

    public static function GetAllIds($ids){
        // GET IDS
        foreach($ids as $id){
            $allId[]=$id['mid'];
        }
        return implode(',',$allId);
    }
    
    public static function GetPlayList(){
    
    $sql = "
            SELECT
            	id as pid,
                title as ptitle
            FROM
                ramro_movie_playlist
            WHERE
                status=1
            ORDER BY
                sort
            ASC
        ";
        DB::query($sql,'',true);
        $data = DB::fetch_assoc_all();
        
        foreach($data as $key => $value)
        {
        
        
        //FILTER AND
        $in = array($data[$key]['subplaylist'] = self::GetSubPlaylist($value['pid']));
        self::ArrayPushAfter($data, $in, 8);
        }
        return $data;
        
    	
    }
    
    public static function GetSubPlaylist($id){
    
    $sql = "
            SELECT
                m.id as mid,
                m.title as title,
                m.image as image,
                m.details as details,
                m.trailer_link  as trailerLink,
                m.full_link  as fullLink,
                m.price  as price,
                m.movie_type  as movieType,
                m.movie_tag  as movieTag,
                m.cast  as cast,
                m.director  as director,
                m.music  as music,
                m.language  as language,
                m.subtitle  as subtitle,
                m.viewed  as view,
                m.url  as url
                
            FROM
                ramro_movie_sub_playlist as s
            RIGHT JOIN
            	ramro_movie as m
            	
            ON
            	s.movie_id = m.id
            WHERE
                s.status=1
            AND
                m.status = 1
            AND
            	s.playlist_id = ?
            ORDER BY
                s.created_at
            DESC
            LIMIT 40
        ";
        DB::query($sql,[$id],true);
        return $data = DB::fetch_assoc_all();
    }
    
    public static function ArrayPushAfter($src, $in, $pos = 1)
	{
	    if(is_int($pos)){
	        $R = array_merge(array_slice($src, 0, $pos + 1), $in, array_slice($src, $pos + 1));
	    }else
	    {
	        foreach($src as $k => $v)
	            {
	            $R[$k] = $v;
	            if($k == $pos)
	                $R = array_merge($R, $in);
	            }
	    }
	    return $R;
	}


    public static function getMoviesByPlaylistType($playlist_id, $limit){
        $sql = "select
                p.title as playlist,
                m.id as mid,
                m.title as title,
                m.details as details,
                m.movie_tag as movieTag,
                m.cast as cast,
                m.director as director,
                m.subtitle as subtitle,
                m.url as url,
                m.image as image,
                m.movie_type as movieType
                from
                ramro_movie_sub_playlist r
                left join ramro_movie_playlist p on p.id = r.playlist_id
                left join ramro_movie m on m.id = r.movie_id
                where r.status = 1 and r.playlist_id = $playlist_id
                order by r.id desc
                limit $limit";
    }

}