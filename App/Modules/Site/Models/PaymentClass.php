<?php

namespace Site\Model;

use DB;

/**
 * Description of PaymentClass
 *
 * @author Bidhee
 */
class PaymentClass {

    private static $_tblname = "paypal_transaction";

    public static function SavePayerInfo($dataUser) {
        if (DB::insert(self::$_tblname, $dataUser)) {
            return true;
        } else {
            return false;
        }
    }

    public static function UpdatePayerInfo($payment_id) {

        if (DB::update(self::$_tblname, array('approved' => 1), 'payment_id = ?', array($payment_id))) {
            return true;
        } else {
            return false;
        }
    }

    public static function GetProductInfo($movie_id) {
        DB::select('*', 'ramro_movie', 'id = ?', array($movie_id));
        return DB::fetch_obj();
    }

    public static function GetInvoiceNo($length = 6) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function GetPaypalTransactionData($payment_id) {
        DB::select(array('payment_for', 'user_id', 'movie_id', 'payment_id', 'approved'), 'paypal_transaction', 'payment_id = ? AND approved=?', array($payment_id,1));
        $res = DB::fetch_obj();
        if ($res) {
            return $res;
        } else {
            return false;
        }
    }

    public static function SavePremiumDataToUser($dataUser) {
        if (DB::insert('ramro_movie_premium', $dataUser)) {
            return true;
        } else {
            return false;
        }
    }

}
