<?php
function AuthenticateUser(\Slim\Route $route)
{

    // Getting request headers
    $headers = apache_request_headers();
    $response = array();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if ($api_key != "abc123") {

            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            Response::json(401, $response);
            //App::stop();

        } else {
            global $user_id;
            // get user primary key id
            $user_id = "abc123";
        }

    } else {

        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        Response::json(400, $response);
        App::stop();
    }
}