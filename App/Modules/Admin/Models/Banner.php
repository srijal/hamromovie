<?php

namespace Admin\Model {

    use App;
    use DB;
    use Encryption;
    use Upload;
    use Session;

    class Banner
    {
        protected static $_table = "ramro_movie_banner";
        protected static $imagePath = "Asset/uploads/banner/";

        public static function GetAll()
        {
            DB::query('SELECT * FROM ' . self::$_table . ' WHERE role=2', '', true);
            $result = DB::fetch_obj_all();

            return isset($result) ? $result : FALSE;
        }

        public static function GetById($id)
        {
            $enc = New Encryption();
            DB::select(array('id','title','details','link','status','movie_type','image','sort_order'), self::$_table, 'id = ?', array($enc->decode($id)));
            $rows = DB::fetch_obj();
            if ($rows) {
                return $rows;
            } else {
                return false;
            }
        }

        public static function Add($data)
        {
            $id = DB::insert(self::$_table, $data);

            return isset($id) ? $id : FALSE;
        }

        public static function Update($rawData, $id)
        {

            $enc = New Encryption();

            if (DB::update(self::$_table, $rawData, 'id = ?', array($enc->decode($id)))) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public static function Remove($ids)
        {
            $enc = New Encryption();
            $res = false;
            foreach ($ids as $did) {

                DB::select('image', self::$_table, 'id = ?', array($enc->decode($did)));
                $row = DB::fetch_obj();

                $file = $row->image;

                if ($row->image != NULL) {
                    unlink($file);
                }

                DB::delete(self::$_table, 'id = ?', array($enc->decode($did)));
                $res = True;
            }

            return $res;

        }

        public static function CheckTitle($title)
        {

            if (DB::dcount('id', self::$_table, 'title = ?', array($title))) {

                return TRUE;
            } else {

                return FALSE;
            }

        }

        public static function Upload($imageName)
        {

            Upload::Instance();

            if (Upload::IsFileSet($imageName)) {

                // returns Image name to save in database
                return Upload::uploadFile($imageName, self::$imagePath);

            } else {

                return false;
            }
        }

        public static function ChangeStatus($ids)
        {

            $enc = New Encryption();
            DB::transaction_start();

            DB::select('status', self::$_table, 'id = ?', array($enc->decode($ids)));
            $res = DB::fetch_assoc();
//            $row->status === 0 ? $stat = 1 :$stat = 0;
            if ($res['status'] == 0) {
                $stat = 1;
            } elseif ($res['status'] == 1) {
                $stat = 0;
            }

            if (DB::update(self::$_table, array('status' => $stat), 'id = ?', array($enc->decode($ids)))) {

                DB::transaction_complete();
                return true;

            } else {

                return false;
            }

        }

        public static function UnlinkUploadFile($imageName,$eid){

            Upload::Instance();

            if(Upload::IsFileSet($imageName)){

                $row = self::GetById($eid);
                $file = $row->image;

                if (file_exists($file) and is_file($file)) {
                    unlink($file);
                    return self::Upload($imageName);

                }else{

                    return self::Upload($imageName);
                }
            }

        }

        public static function SetInSession($sortData)
        {
            $session = New Session();
            $session->setSession($sortData);

        }

        public static function GetFromSession($sortData)
        {
            $data = [];
            $session = New Session();
            foreach ($sortData as $val) {
                $data[] = $session->getSession($val);

            }
            return $data;
        }

        public static function ClearSortVariable($sessData)
        {

            foreach ($sessData as $val) {
                $_SESSION[$val] = '';
            }

        }

        public static function ToggleSort($sort)
        {
            return $sort == "ASC" ? "DESC" : "ASC";
        }

    }
}