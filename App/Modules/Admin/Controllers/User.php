<?php

namespace Admin\Controller {

    use Admin\model\User as UserModel;
    use App;
    use Auth\Model\Auth as AuthModel;
    use DB;
    use Input;
    use Pagination;
    use Request;
    use Response;
    use Upload;
    use Validate;
    use View;

    class User
    {

        // HOME
        public function Home()
        {
            AuthModel::IsLogged(array('loggedIn' =>true));

            try {

                $limit = 2;
                $start = 0;

                $p = Input::get('p');
                isset($p) ? $page = preg_replace("/[^0-9]/", " ", trim($p)) : $page = 1;

                $start = ($page - 1) * $limit;

                $sql = "SELECT
                                *
                        FROM
                               ramro_movie_user
                        WHERE
                               role = 2
                ";

                // DEFINE SEARCH QUERY
                if ($q = preg_replace('/[^A-Za-z0-9 ]/', ' ', Input::params('q'))) {
                    $sql .= " AND first_name LIKE ? ";
                }

                // COUNT RECORDS
                DB::query($sql, array('%' . $q . '%'), QUERY_CACHE);
                $data['totalRecords'] = $count = count(DB::fetch_assoc_all());

                // DEFAULT ASSIGN ORDER
                $sql .= " ORDER BY id DESC LIMIT $start, $limit";

                // FINAL QUERY FOR THE RESULT
                DB::query($sql, array('%' . $q . '%'), QUERY_CACHE);
                $data['results'] = DB::fetch_assoc_all();

                // PAGINATION RESULT AND LINK
                $pg = new Pagination();
                $data['pagination'] = $pg->create_links($limit, $page, App::urlfor('user-grid') . '?q=' . $q . '&p=', $count);

            } catch (ResourceNotFoundException $e) {

                $data['errMsg'] = $e->getMessage();
            }

            // ASSIGN PAGE TO FORM TO RESUBMIT
            $data['page'] = $page;

            View::display('@Admin/user/user-grid.twig', $data);
        }

        // DETAILS
        public function Details($eid, $page)
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' =>true));

            if (UserModel::GetById($eid) != FALSE) {

                $data['row'] = UserModel::GetById($eid);
            } else {

                Response::redirect(App::urlFor('user-grid'));
            }
            $data['page'] = $page;
            View::display('@Admin/user/user-details.twig', $data);
        }

        // ADD
        public function Insert()
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' =>true));

            // GRAB ALL INPUTS
            $input = (object)Input::post();

            if (Request::isPost()) {

                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->fname, $displayName = 'First Name', $req = TRUE, $min = 3, $max = 30);
                Validate::Str($input->lname, $displayName = 'Last Name', $req = TRUE, $min = 3, $max = 30);
                Validate::Str($input->email, $displayName = 'Email', $req = TRUE);
                Validate::Str($input->country, $displayName = 'Country', $req = TRUE);
                Validate::File($imageName = 'image', $req = True, $formats = array('jpeg', 'jpg', 'png'), $sizeInMb = 2, $fileName = 'Profile Image');

                if (Validate::IsFine()) {

                    if (!UserModel::CheckUserEmail($input->email)) {

                        try {

                            DB::transaction_start();

                            $dataUser = array(

                                'first_name' => $input->fname,
                                'last_name' => $input->lname,
                                'email' => $input->email,
                                'country' => $input->country,
                                'image' => UserModel::Upload('image'),
                                'role' =>2,// 2 is Sub User
                                'status' => 2,
                                'created_at' => CUR_TIME,
                                'updated_at' => CUR_TIME

                            );

                            if (UserModel::Add($dataUser)) {

                                $data['succMsg'] = "Successfully Added";
                                $data['reset'] = true;

                            } else {

                                $data['errMsg'] = "Error in Adding Data try again later";
                            }

                            DB::transaction_complete();

                        } catch (ResourceNotFoundException $e) {

                            $data['errMsg'] = $e->getMessage();
                        }
                    } else {
                        $data['errMsg'] = 'User Already Exists';
                    }
                }

            }

            View::display('@Admin/user/user-add.twig', $data);
        }

        // DELETE
        public function Delete()
        {
            AuthModel::IsLogged(array('loggedIn' =>true));

            if (Request::isPost()) {

                $ids = Input::post('toDelete');

                if (count(Input::post()) > 0) {

                    if (UserModel::Remove($ids)) {

                        Response::redirect(App::urlFor('user-grid'));
                    }

                } else {

                    Response::redirect(App::urlFor('user-grid'));
                }

            }
        }

        // EDIT
        public function Edit($eid, $page)
        {

            AuthModel::IsLogged(array('loggedIn' =>true));
            $data['v'] = Validate::Instance(); //OBJECT VALIDATE

            if (Request::isPost() && sizeof(Request::isPost()) > 0) {

                $input = (object)Input::post();

                //VALIDATE DATA
                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->fname, $displayName = 'First Name', $req = TRUE, $min = 3, $max = 30);
                Validate::Str($input->lname, $displayName = 'Last Name', $req = TRUE, $min = 3, $max = 30);
                Validate::Str($input->email, $displayName = 'Email', $req = TRUE);
                Validate::File($imageName = 'image', $req = False, $formats = array('jpeg', 'jpg', 'png'), $sizeInMb = 2, $fileName = 'Profile Image');

                if (Validate::isFine()) {

                    try {

                        DB::transaction_start();
                        $imageName = (Upload::IsFileSet('image') === TRUE) ? UserModel::UnlinkUploadFile('image', $eid) : $input->old_image;

                        $rawData = array(

                            'first_name' => $input->fname,
                            'last_name' => $input->lname,
                            'email' => $input->email,
                            'image' => $imageName
                        );
                        //UPDATE DATA
                        if (UserModel::Update($rawData, $eid)) {

                            $data['succMsg'] = "Data Updated Successfully";
                        }

                        DB::transaction_complete();

                    } catch (ResourceNotFoundException $e) {

                        $data['errMsg'] = $e->getMessage();
                    }
                }
            }

            if ($rows = UserModel::GetById($eid)) {
                $data['row'] = $rows;
            } else {

                Response::redirect(App::urlFor('dashboard'));
            }
            $data['page'] = $page;
            View::display('@Admin/user/user-edit.twig', $data);
        }

        // CHANGE STATUS
        public function Status()
        {
            AuthModel::IsLogged(array('loggedIn' =>true));
            if (Request::isGet()) {

                $sid = Input::get('id');
                $p = (int)Input::get('p');

                if (UserModel::ChangeStatus($sid) === TRUE) {
                    Response::redirect(App::urlFor('user-grid') . '?p=' . $p);
                }
            }

        }

    }

}