<?php

namespace Admin\Controller {

    use Admin\Model\Movie as MovieModel;
    use App;
    use Auth\Model\Auth as AuthModel;
    use DB;
    use Input;
    use Pagination;
    use Request;
    use Response;
    use Upload;
    use Validate;
    use View;

    class Movie
    {

        // HOME
        public function Home()
        {
            AuthModel::IsLogged(array('loggedIn' => true));

            $userId = AuthModel::GetSessionValue('id');
            $pages = (int)Input::get('p');
            $searchParams = preg_replace('/[^A-Za-z0-9 ]/', ' ', Input::params('q'));
            $clear = (int)Input::get('clear', 0);

            //  SORT PARAMETER
            $sort_type = preg_replace('/[^A-Za-z_]/', ' ', Input::get('t'));
            $sort_by = preg_replace('/[^A-Z]/', ' ', Input::get('s'));

            // CLEAR ALL SORT VALUES
            if ($clear == 1) {

                movieModel::ClearSortVariable(['sort_type', 'sort_by', 'search', 'page']);
                Response::redirect(App::urlFor('admin-movie'));

            }

            // SET SORT PARAMETER IN SESSION
            if ($sort_by != '' || $sort_type != '' || $sort_by != '' || $pages != '' || $searchParams != '') {

                // SET VARIABLE IN SESSION
                movieModel::SetInSession(['sort_type' => $sort_type, 'sort_by' => $sort_by, 'search' => $searchParams, 'page' => $pages]);

            }

            // GET SORT PARAMETER FROM SESSION
            list($type, $sort, $search, $page) = MovieModel::GetFromSession(['sort_type', 'sort_by', 'search', 'page']);

            try {

                if ($page == '') {
                    $page = 1;
                };

                $limit = PAGE_LIMIT != "" ? PAGE_LIMIT : 8;
                $offset = ($page - 1) * $limit;

                $sql = "
                        SELECT
                                m.id as id,
                                m.title as title,
                                m.image as image,
                                m.trailer_link as trailer_id,
                                m.full_link as movie_id,
                                m.movie_type as movie_type,
                                m.price as price,
                                m.created_at as created_at,
                                m.status as status,
                                c.title as category
                        FROM
                               ramro_movie as m

                        RIGHT JOIN

                              ramro_movie_category as c
                        ON
                              m.cat_id = c.id
                       ";

                // APPEND SEARCH QUERY
                $sql .= isset($search) ? " WHERE m.title LIKE '%" . $search . "%' OR m.movie_type LIKE '%" . $search . "%' " : "";

                DB::query($sql, array($userId), QUERY_CACHE);
                $data['totalRecords'] = $totalRecord = count(DB::fetch_assoc_all());

                $sql .= ($type != '') ? " ORDER BY m.{$type} {$sort}" : " ORDER BY m.id DESC ";

                $sql .= " LIMIT $offset, $limit";

                DB::query($sql, array($userId), QUERY_CACHE);
                $data['results'] = DB::fetch_assoc_all();

                $data['pagination'] = Pagination::create_links($limit, $page, App::urlfor('admin-movie') . '?q=' . $search . '&t=' . $type . '&s=' . $sort . '&p=', $totalRecord);

            } catch (ResourceNotFoundException $e) {

                $data['errMsg'] = $e->getMessage();
            }

            $data['totalRecord'] = $totalRecord;
            $data['search'] = $search;
            $data['sort'] = MovieModel::ToggleSort($sort);

            DB::close();

            View::display('@Admin/movie/movie-grid.twig', $data);
        }

        // ADD
        public function Insert()
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' => true));

            // GRAB ALL INPUTS
            $input = (object)Input::post();



            if (Request::isPost()) {

//                var_dump(Input::post());
//                die;

                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->title, $displayAs = 'Title', $req = TRUE, $min = 3);
                Validate::Str($input->category, 'Category', $req = TRUE, 1);
                Validate::Num($input->trailer_id, 'Trailer Id', $req = TRUE, $min = 2);
                Validate::Num($input->full_id, 'Full Movie Id', $req = FALSE, $min = 2);
                Validate::Str($input->movie_type, 'Movie Type', $req = TRUE, $min = 2);
                Validate::Num($input->price, 'Price', $req = FALSE, $min = 1, $max = 5);
                Validate::Str($input->cast, 'Cast', $req = TRUE, $min = 2);
                Validate::Str($input->director, 'Director', $req = TRUE, $min = 2);
                Validate::Str($input->subtitle, 'Subtitle', $req = TRUE, $min = 2);
                Validate::Str($input->language, 'Language', $req = TRUE, $min = 2);
                Validate::Str($input->music, 'Music', $req = TRUE, $min = 2);
                Validate::Str($input->movie_tags, 'Movie Tag', $req = TRUE, $min = 2);
                Validate::Str($input->details, 'Details', $req = TRUE, $min = 10);
                Validate::Str($input->meta_description, 'Meta Description', $req = FALSE, $min = 10, $max = 156);
                Validate::File('image', $req = TRUE, $formats = array('jpeg', 'jpg', 'png'), $sizeInMb = 3, $fileName = 'Movie Image');

                if (Validate::IsFine()) {

                    if (MovieModel::CheckPremium($input->movie_type, $input->price)) {
                        if (!MovieModel::CheckTitle($input->title)) {


                            try {

                                DB::transaction_start();

                                $dataUser = array(

                                    'title' => $input->title,
                                    'cat_id' => $input->category,
                                    'trailer_link' => $input->trailer_id,
                                    'full_link' => $input->full_id,
                                    'movie_type' => $input->movie_type,
                                    'price' => ($input->movie_type=='premium')?$input->price:"",
                                    'cast' => $input->cast,
                                    'director' => $input->director,
                                    'subtitle' => $input->subtitle,
                                    'language' => $input->language,
                                    'music' => $input->music,
                                    'movie_tag' => $input->movie_tags,
                                    'details' => $input->details,
                                    'url' => preg_replace('[ ]', '-', $input->title),
                                    'image' => MovieModel::Upload('image'),
                                    'status' => 1,
                                    'user_id' => AuthModel::GetSessionValue('id'),
                                    'created_at' => CUR_TIME,
                                    'updated_at' => CUR_TIME,
                                    'meta_author' => $input->meta_author,
                                    'meta_keywords' => $input->meta_keywords,
                                    'meta_description' => $input->meta_description

                                );

                                if (MovieModel::Add($dataUser)) {

                                    $data['succMsg'] = "Successfully Added";
                                    $data['reset'] = true;

                                } else {

                                    $data['errMsg'] = "Error in Adding Data try again later";
                                }

                                DB::transaction_complete();

                            } catch (ResourceNotFoundException $e) {

                                $data['errMsg'] = $e->getMessage();
                            }
                        } else {
                            $data['errMsg'] = 'Title Already Exists';
                        }
                    } else {
                        $data['errMsg'] = 'Price cannot be empty for premium movie';
                    }
                }

            }

            $data['categories'] = MovieModel::GetCategory();

            View::display('@Admin/movie/movie-add.twig', $data);
        }

        // DETAILS
        public function Details($eid)
        {

            $data = array();

            AuthModel::IsLogged(array('loggedIn' => true));

            if (MovieModel::GetById($eid) != FALSE) {

                $data['row'] = MovieModel::GetById($eid);
            } else {

                Response::redirect(App::urlFor('admin-movie'));
            }

            $data['categories'] = MovieModel::GetCategory();
            View::display('@Admin/movie/movie-details.twig', $data);
        }

        // EDIT
        public function Edit($eid)
        {
            AuthModel::IsLogged(array('loggedIn' => true));

            $data['v'] = Validate::Instance(); //OBJECT VALIDATE

            if (Request::isPost() && sizeof(Request::isPost()) > 0) {

                $input = (object)Input::post();

                //VALIDATE DATA
                $data['v'] = Validate::Instance(); //OBJECT VALIDATE


                Validate::Str($input->title, $displayAs = 'Title', $req = TRUE, $min = 3);
                Validate::Str($input->category, 'Category', $req = TRUE, 1);
                Validate::Num($input->trailer_id, 'Trailer Id', $req = TRUE, $min = 2);
                Validate::Num($input->full_id, 'Full Movie Id', $req = FALSE, $min = 2);
                Validate::Str($input->movie_type, 'Movie Type', $req = TRUE, $min = 2);
                Validate::Num($input->price, 'Price', $req = FALSE, $min = 1, $max = 5);
                Validate::Str($input->cast, 'Cast', $req = TRUE, $min = 2);
                Validate::Str($input->director, 'Director', $req = TRUE, $min = 2);
                Validate::Str($input->subtitle, 'Subtitle', $req = TRUE, $min = 2);
                Validate::Str($input->language, 'Language', $req = TRUE, $min = 2);
                Validate::Str($input->music, 'Music', $req = TRUE, $min = 2);
                Validate::Str($input->movie_tags, 'Movie Tag', $req = TRUE, $min = 2);
                Validate::Str($input->details, 'Details', $req = TRUE, $min = 10);
                Validate::Str($input->meta_description, 'Meta Description', $req = FALSE, $min = 10, $max = 156);
                Validate::File('image', $req = FALSE, $formats = array('jpeg', 'jpg', 'png'), $sizeInMb = 3, $fileName = 'Movie Image');

                if (Validate::isFine()) {

                    if (MovieModel::CheckPremium($input->movie_type, $input->price)) {
                        try {

                            DB::transaction_start();
                            $imageName = (Upload::IsFileSet('image') === TRUE) ? MovieModel::UnlinkUploadFile('image', $eid) : $input->old_image;

                            $rawData = array(

                                'title' => $input->title,
                                'cat_id' => $input->category,
                                'trailer_link' => $input->trailer_id,
                                'full_link' => $input->full_id,
                                'movie_type' => $input->movie_type,
                                'price' => ($input->movie_type=='premium')?$input->price:"",
                                'cast' => $input->cast,
                                'director' => $input->director,
                                'subtitle' => $input->subtitle,
                                'language' => $input->language,
                                'music' => $input->music,
                                'movie_tag' => $input->movie_tags,
                                'details' => $input->details,
                                'url' => preg_replace('[ ]', '-', $input->title),
                                'image' => $imageName,
                                'status' => 1,
                                'user_id' => AuthModel::GetSessionValue('id'),
                                'created_at' => CUR_TIME,
                                'updated_at' => CUR_TIME,
                                'meta_author' => $input->meta_author,
                                'meta_keywords' => $input->meta_keywords,
                                'meta_description' => $input->meta_description

                            );
                            //UPDATE DATA
                            if (MovieModel::Update($rawData, $eid)) {

                                $data['succMsg'] = "Data Updated Successfully";
                            }

                            DB::transaction_complete();

                        } catch (ResourceNotFoundException $e) {

                            $data['errMsg'] = $e->getMessage();
                        }

                    } else {
                        $data['errMsg'] = 'Price cannot be empty for premium movie';
                    }

                }
            }

            if ($rows = MovieModel::GetById($eid)) {
                $data['row'] = $rows;
            } else {

                Response::redirect(App::urlFor('dashboard'));
            }

            $data['categories'] = MovieModel::GetCategory();
            View::display('@Admin/movie/movie-edit.twig', $data);
        }

        // DELETE
        public function Delete()
        {
            AuthModel::IsLogged(array('loggedIn' => true));

            if (Request::isPost()) {

                $ids = Input::post('toDelete');

                if (count(Input::post()) > 0) {

                    if (MovieModel::Remove($ids)) {

                        Response::redirect(App::urlFor('admin-movie'));
                    }

                } else {

                    Response::redirect(App::urlFor('admin-movie'));
                }

            }
        }

        // CHANGE STATUS
        public function Status()
        {
            AuthModel::IsLogged(array('loggedIn' => true));

            if (Request::isGet()) {

                $sid = Input::get('id');
                $p = (int)Input::get('p');

                if (MovieModel::ChangeStatus($sid) === TRUE) {
                    Response::redirect(App::urlFor('admin-movie') . '?p=' . $p);
                }
            }

        }

    }

}