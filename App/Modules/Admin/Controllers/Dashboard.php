<?php

namespace Admin\Controller;

use App;
use DB;
use Input;
use Request;
use Response;
use View;
use Auth\model\Auth as AuthModel;

class Dashboard
{

    public function IndexAction()
    {

        $data = array();
        AuthModel::IsLogged(array('loggedIn' =>true));

        $data['totalMovieCount'] = DB::dcount('id','ramro_movie');
        $data['totalBannerCount'] = DB::dcount('id','ramro_movie_banner');
        $data['totalCategoryCount'] = DB::dcount('id','ramro_movie_category');
        $data['totalPlaylistCount'] = DB::dcount('id','ramro_movie_playlist');

        View::display('@Admin/dashboard/index.twig', $data);
    }

}