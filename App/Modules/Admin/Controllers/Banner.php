<?php

namespace Admin\Controller {

    use App;
    use Auth\Model\Auth as AuthModel;
    use DB;
    use Input;
    use Pagination;
    use Request;
    use Response;
    use Admin\Model\Banner as BannerModel;
    use Validate;
    use View;
    use Upload;

    class Banner
    {

        // HOME
        public function Home()
        {

            AuthModel::IsLogged(array('loggedIn' => true));

            $userId         = AuthModel::GetSessionValue('id');
            $pages          = (int)Input::get('p');
            $searchParams   = preg_replace('/[^A-Za-z0-9 ]/', ' ', Input::params('q'));
            $clear          = (int)Input::get('clear',0);

            //  SORT PARAMETER
            $sort_type      = preg_replace('/[^A-Za-z_]/', ' ', Input::get('t'));
            $sort_by        = preg_replace('/[^A-Z]/', ' ', Input::get('s'));

            // CLEAR ALL SORT VALUES
            if ($clear == 1) {

                BannerModel::ClearSortVariable(['sort_type', 'sort_by', 'search', 'page']);
                Response::redirect(App::urlFor('banner'));

            }

            // SET SORT PARAMETER IN SESSION
            if ($sort_by != '' || $sort_type != '' ||  $sort_by != '' || $pages != '' || $searchParams !='') {

                // SET VARIABLE IN SESSION
                BannerModel::SetInSession(['sort_type' => $sort_type,'sort_by' => $sort_by,'search' => $searchParams,'page' => $pages]);

            }

            // GET SORT PARAMETER FROM SESSION
            list($type, $sort, $search, $page) = BannerModel::GetFromSession(['sort_type','sort_by','search','page']);

            try {

                if ($page == ''){ $page = 1; };

                $limit = PAGE_LIMIT != "" ? PAGE_LIMIT : 8;
                $offset = ($page - 1) * $limit;

                $sql = "
                        SELECT
                                *
                        FROM
                               ramro_movie_banner
                       ";

                // APPEND SEARCH QUERY
                $sql .= isset($search) ? " WHERE title LIKE '%" . $search . "%' " : "";

                DB::query($sql, array($userId), QUERY_CACHE);
                $data['totalRecords'] = $totalRecord = count(DB::fetch_assoc_all());

                $sql .= ($type != '') ? " ORDER BY {$type} {$sort}" : " ORDER BY id DESC ";

                $sql .= " LIMIT $offset, $limit";

                DB::query($sql, array($userId), QUERY_CACHE);
                $data['results'] = DB::fetch_assoc_all();
                DB::close();

                $data['pagination'] = Pagination::create_links($limit, $page, App::urlfor('banner') . '?q=' . $search . '&t=' . $type . '&s=' . $sort . '&p=', $totalRecord);

            } catch (ResourceNotFoundException $e) {

                $data['errMsg'] = $e->getMessage();
            }

            $data['totalRecord'] = $totalRecord;
            $data['search']      = $search;
            $data['sort']        = BannerModel::ToggleSort($sort);

            View::display('@Admin/banner/banner-grid.twig', $data);
        }

        // ADD
        public function Insert()
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' =>true));

            // GRAB ALL INPUTS
            $input = (object)Input::post();

            if (Request::isPost()) {


                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->title, $displayName = 'Title', $req = TRUE, $min = 3);
                Validate::Str($input->details, $displayName = 'Details', $req = TRUE, $min = 3);
                Validate::Str($input->movie_type, $displayName = 'Movie Type', $req = TRUE, $min = 1);
                Validate::Num($input->sort_order, $displayName = 'Sort Order', $req = TRUE, $min = 1,$max=3);
                Validate::Url($input->link, $displayName = 'Link', $req = TRUE, $min = 1);
                Validate::File($imageName = 'image', $req = True, $formats = array('jpeg', 'jpg', 'png'), $sizeInMb = 2, $fileName = 'Banner Image');

                if (Validate::IsFine()) {

                    if (!BannerModel::CheckTitle($input->title)) {

                        try {

                            DB::transaction_start();

                            $dataUser = array(

                                'title' => $input->title,
                                'details' => $input->details,
                                'movie_type' => $input->movie_type,
                                'link' => $input->link,
                                'sort_order' => $input->sort_order,
                                'image' => BannerModel::Upload('image'),
                                'user_id' =>AuthModel::GetSessionValue('id'),
                                'created_at' => CUR_TIME,
                                'updated_at' => CUR_TIME

                            );

                            if (BannerModel::Add($dataUser)) {

                                $data['succMsg'] = "Successfully Added";
                                $data['reset'] = true;

                            } else {

                                $data['errMsg'] = "Error in Adding Data try again later";
                            }

                            DB::transaction_complete();

                        } catch (ResourceNotFoundException $e) {

                            $data['errMsg'] = $e->getMessage();
                        }
                    } else {
                        $data['errMsg'] = 'Title Already Exists';
                    }
                }

            }

            View::display('@Admin/banner/banner-add.twig', $data);
        }

        // EDIT
        public function Edit($eid)
        {
            AuthModel::IsLogged(array('loggedIn' =>true));

            $data['v'] = Validate::Instance(); //OBJECT VALIDATE

            if (Request::isPost() && sizeof(Request::isPost()) > 0) {

                $input = (object)Input::post();

                //VALIDATE DATA
                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->title, $displayName = 'Title', $req = TRUE, $min = 3);
                Validate::Str($input->details, $displayName = 'Details', $req = TRUE, $min = 3);
                Validate::Str($input->movie_type, $displayName = 'Movie Type', $req = TRUE, $min = 1);
                Validate::Num($input->sort_order, $displayName = 'Sort Order', $req = TRUE, $min = 1,$max=3);
                Validate::Url($input->link, $displayName = 'Link', $req = TRUE, $min = 1);
                Validate::File($imageName = 'image', $req = False, $formats = array('jpeg', 'jpg', 'png'), $sizeInMb = 2, $fileName = 'Banner Image');

                if (Validate::isFine()) {

                    try {

                        DB::transaction_start();
                        $imageName = (Upload::IsFileSet('image') === TRUE) ? BannerModel::UnlinkUploadFile('image', $eid) : $input->old_image;

                        $rawData = array(

                                'title' => $input->title,
                                'details' => $input->details,
                                'movie_type' => $input->movie_type,
                                'link' => $input->link,
                                'sort_order' => $input->sort_order,
                                'image' => $imageName,
                                'updated_at' => CUR_TIME
                        );
                        //UPDATE DATA
                        if (BannerModel::Update($rawData, $eid)) {

                            $data['succMsg'] = "Data Updated Successfully";
                        }

                        DB::transaction_complete();

                    } catch (ResourceNotFoundException $e) {

                        $data['errMsg'] = $e->getMessage();
                    }
                }
            }

            if ($rows = BannerModel::GetById($eid)) {
                $data['row'] = $rows;
            } else {

                Response::redirect(App::urlFor('dashboard'));
            }

            View::display('@Admin/banner/banner-edit.twig', $data);
        }

        // DETAILS
        public function Details($eid)
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' =>true));

            if (BannerModel::GetById($eid) != FALSE) {

                $data['row'] = BannerModel::GetById($eid);
            } else {

                Response::redirect(App::urlFor('banner'));
            }

            View::display('@Admin/banner/banner-details.twig', $data);
        }

        // DELETE
        public function Delete()
        {
            AuthModel::IsLogged(array('loggedIn' =>true));

            if (Request::isPost()) {

                $ids = Input::post('toDelete');

                if (count(Input::post()) > 0) {

                    if (BannerModel::Remove($ids)) {

                        Response::redirect(App::urlFor('banner'));
                    }

                } else {

                    Response::redirect(App::urlFor('banner'));
                }

            }
        }

        // CHANGE STATUS
        public function Status()
        {
            AuthModel::IsLogged(array('loggedIn' =>true));
            if (Request::isGet()) {

                $sid = Input::get('id');

                if (BannerModel::ChangeStatus($sid) === TRUE) {
                    Response::redirect(App::urlFor('banner'));
                }
            }

        }

    }
}