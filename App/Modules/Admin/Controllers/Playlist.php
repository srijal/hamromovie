<?php

namespace Admin\Controller {

    use Admin\Model\Playlist as PlaylistModel;
    use App;
    use Auth\Model\Auth as AuthModel;
    use DB;
    use Input;
    use Pagination;
    use Request;
    use Response;
    use Validate;
    use View;

    class Playlist
    {

        // HOME
        public function Home()
        {

            AuthModel::IsLogged(array('loggedIn' => true));

            $userId = AuthModel::GetSessionValue('id');
            $pages = (int)Input::get('p');
            $searchParams = preg_replace('/[^A-Za-z0-9 ]/', ' ', Input::params('q'));
            $clear = (int)Input::get('clear', 0);

            //  SORT PARAMETER
            $sort_type = preg_replace('/[^A-Za-z_]/', ' ', Input::get('t'));
            $sort_by = preg_replace('/[^A-Z]/', ' ', Input::get('s'));

            // CLEAR ALL SORT VALUES
            if ($clear == 1) {

                PlaylistModel::ClearSortVariable(['sort_type', 'sort_by', 'search', 'page']);
                Response::redirect(App::urlFor('admin-playlist'));

            }

            // SET SORT PARAMETER IN SESSION
            if ($sort_by != '' || $sort_type != '' || $sort_by != '' || $pages != '' || $searchParams != '') {

                // SET VARIABLE IN SESSION
                PlaylistModel::SetInSession(['sort_type' => $sort_type, 'sort_by' => $sort_by, 'search' => $searchParams, 'page' => $pages]);

            }

            // GET SORT PARAMETER FROM SESSION
            list($type, $sort, $search, $page) = PlaylistModel::GetFromSession(['sort_type', 'sort_by', 'search', 'page']);

            try {

                if ($page == '') {
                    $page = 1;
                };

                $limit = PAGE_LIMIT != "" ? PAGE_LIMIT : 8;
                $offset = ($page - 1) * $limit;

                $sql = "
                        SELECT
                                *
                        FROM
                               ramro_movie_playlist
                       ";

                // APPEND SEARCH QUERY
                $sql .= isset($search) ? " WHERE title LIKE '%" . $search . "%' " : "";

                DB::query($sql, array($userId), QUERY_CACHE);
                $data['totalRecords'] = $totalRecord = count(DB::fetch_assoc_all());

                $sql .= ($type != '') ? " ORDER BY {$type} {$sort}" : " ORDER BY id DESC ";

                $sql .= " LIMIT $offset, $limit";

                DB::query($sql, array($userId), QUERY_CACHE);
                $data['results'] = DB::fetch_assoc_all();
                DB::close();

                $data['pagination'] = Pagination::create_links($limit, $page, App::urlfor('admin-playlist') . '?q=' . $search . '&t=' . $type . '&s=' . $sort . '&p=', $totalRecord);

            } catch (ResourceNotFoundException $e) {

                $data['errMsg'] = $e->getMessage();
            }

            $data['totalRecord'] = $totalRecord;
            $data['search'] = $search;
            $data['sort'] = PlaylistModel::ToggleSort($sort);

            View::display('@Admin/playlist/playlist-grid.twig', $data);
        }

        // ADD
        public function Insert()
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' => true));

            // GRAB ALL INPUTS
            $input = (object)Input::post();

            if (Request::isPost()) {


                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->title, $displayName = 'Title', $req = TRUE, $min = 3);
                Validate::Num($input->sort_order, $displayName = 'Sort Order', $req = TRUE, $min = 1);
                
                if (Validate::IsFine()) {

                    if (!PlaylistModel::CheckTitle($input->title)) {

                        try {

                            DB::transaction_start();

                            $dataUser = array(

                                'title' => $input->title,
                                'sort' => $input->sort_order,
                                'status' => 1,
                                'user_id' => AuthModel::GetSessionValue('id'),
                                'created_at' => CUR_TIME,
                                'updated_at' => CUR_TIME

                            );

                            if (PlaylistModel::Add($dataUser)) {

                                $data['succMsg'] = "Successfully Added";
                                $data['reset'] = true;

                            } else {

                                $data['errMsg'] = "Error in Adding Data try again later";
                            }

                            DB::transaction_complete();

                        } catch (ResourceNotFoundException $e) {

                            $data['errMsg'] = $e->getMessage();
                        }
                    } else {
                        $data['errMsg'] = 'Title Already Exists';
                    }
                }

            }

            View::display('@Admin/playlist/playlist-add.twig', $data);
        }

        // EDIT
        public function Edit($eid)
        {
            AuthModel::IsLogged(array('loggedIn' => true));

            $data['v'] = Validate::Instance(); //OBJECT VALIDATE

            if (Request::isPost() && sizeof(Request::isPost()) > 0) {

                $input = (object)Input::post();

                //VALIDATE DATA
                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->title, $displayName = 'Title', $req = TRUE, $min = 3);
                Validate::Num($input->sort_order, $displayName = 'Sort Order', $req = TRUE, $min = 1);

                if (Validate::isFine()) {

                    try {

                        DB::transaction_start();

                        $rawData = array(

                            'title' => $input->title,
                            'sort' => $input->sort_order,
                            'updated_at' => CUR_TIME
                        );
                        //UPDATE DATA
                        if (PlaylistModel::Update($rawData, $eid)) {

                            $data['succMsg'] = "Data Updated Successfully";
                        }

                        DB::transaction_complete();

                    } catch (ResourceNotFoundException $e) {

                        $data['errMsg'] = $e->getMessage();
                    }
                }
            }

            if ($rows = PlaylistModel::GetById($eid)) {
                $data['row'] = $rows;
            } else {

                Response::redirect(App::urlFor('dashboard'));
            }

            View::display('@Admin/playlist/playlist-edit.twig', $data);
        }

        // DETAILS
        public function Details($eid)
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' => true));

            if (PlaylistModel::GetById($eid) != FALSE) {

                $data['row'] = PlaylistModel::GetById($eid);
            } else {

                Response::redirect(App::urlFor('admin-playlist'));
            }

            View::display('@Admin/playlist/playlist-details.twig', $data);
        }

        // DELETE
        public function Delete()
        {
            AuthModel::IsLogged(array('loggedIn' => true));

            if (Request::isPost()) {

                $ids = Input::post('toDelete');

                if (count(Input::post()) > 0) {

                    if (PlaylistModel::Remove($ids)) {

                        Response::redirect(App::urlFor('admin-playlist'));
                    }

                } else {

                    Response::redirect(App::urlFor('admin-playlist'));
                }

            }
        }

        // CHANGE STATUS
        public function Status()
        {
            AuthModel::IsLogged(array('loggedIn' => true));
            if (Request::isGet()) {

                $sid = Input::get('id');

                if (PlaylistModel::ChangeStatus($sid) === TRUE) {
                    Response::redirect(App::urlFor('admin-playlist'));
                }
            }

        }

        public function SubPlaylist($playlist_id)
        {

            AuthModel::IsLogged(array('loggedIn' => true));

            $userId = AuthModel::GetSessionValue('id');

            try {


                $limit = PAGE_LIMIT != "" ? PAGE_LIMIT : 8;

                $sql = "
                        SELECT
                        DISTINCT
                                s.id as id,
                                m.title as title,
                                m.image as image
                        FROM
                               ramro_movie_sub_playlist as s

                        LEFT JOIN

                               ramro_movie_playlist as p
                        ON

                               p.id = s.playlist_id

                        LEFT JOIN

                              ramro_movie as m

                        ON

                              m.id = s.movie_id
                        WHERE
                              s.playlist_id = ?
                       ";

                // APPEND SEARCH QUERY
                //$sql .= isset($search) ? " WHERE title LIKE '%" . $search . "%' " : "";

                DB::query($sql, array($playlist_id), QUERY_CACHE);
                $data['totalRecords'] = $totalRecord = count(DB::fetch_assoc_all());


                $sql .= " ORDER BY s.id DESC LIMIT  $limit";

                DB::query($sql, array($playlist_id), QUERY_CACHE);
                $data['results'] = DB::fetch_assoc_all();
                DB::close();

                //$data['pagination'] = Pagination::create_links($limit, $page, App::urlfor('admin-sub-playlist').'/' .$playlist_id.'&p=', $totalRecord);

            } catch (ResourceNotFoundException $e) {

                $data['errMsg'] = $e->getMessage();
            }

            $data['totalRecord'] = $totalRecord;
            $data['pid'] = $playlist_id;

            View::display('@Admin/playlist/sub-playlist.twig', $data);

        }

        // ADD
        public function SubPlayListAdd($movie_id)
        {
            $data = array();
            AuthModel::IsLogged(array('loggedIn' => true));

            // GRAB ALL INPUTS
            $input = (object)Input::post();

            if (Request::isPost()) {


                $data['v'] = Validate::Instance(); //OBJECT VALIDATE

                Validate::Str($input->playlist_id, $displayName = 'Playlist', $req = TRUE, $min =1);

                if (Validate::IsFine()) {

                    if (!PlaylistModel::CheckSubPlayListMovie((int)$movie_id,$input->playlist_id)) {

                        try {

                            DB::transaction_start();

                            $data = array(

                                'playlist_id' => $input->playlist_id,
                                'movie_id' => (int)$movie_id,
                                'status' => 1,
                                'user_id' => AuthModel::GetSessionValue('id'),
                                'created_at' => CUR_TIME,
                                'updated_at' => CUR_TIME

                            );

                            if (PlaylistModel::AddSubList($data)) {

                                $data['succMsg'] = "Successfully Added";
                                $data['reset'] = true;

                            } else {

                                $data['errMsg'] = "Error in Adding Data try again later";
                            }

                            DB::transaction_complete();

                        } catch (ResourceNotFoundException $e) {

                            $data['errMsg'] = $e->getMessage();
                        }
                    } else {
                        $data['errMsg'] = 'Movie Already Exists';
                    }
                }

            }
            $data['mid']=$movie_id;
            $data['playlist'] = PlaylistModel::GetAll();

            View::display('@Admin/playlist/sub-playlist-add.twig', $data);
        }

        // DELETE
        public function SubPlayListDelete($id)
        {
            AuthModel::IsLogged(array('loggedIn' => true));

            if (Request::isPost()) {

                $ids = Input::post('toDelete');

                if (count(Input::post()) > 0) {

                    if (PlaylistModel::RemoveSubPlayList($ids)) {

                        Response::redirect(App::urlFor('admin-sub-playlist').'/'.$id);
                    }

                } else {

                    Response::redirect(App::urlFor('admin-sub-playlist').'/'.$id);
                }

            }
        }

    }
}