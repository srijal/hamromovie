<?php

/*
  ||============================================================
  || ###########################################################
  ||           HOME ROUTE DETAILS
  || ###########################################################
  ||
  ||============================================================
  || INDEX
  ||============================================================
  ||  1. || '/'             For Site
  ||
  ||  2. || '/admin'        For Admin
  ||
  ||  3. || '/api'          For Api
  ||
  ||                         a. '/v1' For Ver 1
  ||                         b. '/v2' For Ver 2
  ||
  ||  4. || '/notification' For Notification
  ||=============================================================
  ||
 */



Route::get('/', '\\Site\\Controller\\Home:Index')->name('home');
//Route::get('/facebook', 'Site\Controller\User:FacebookLogin')->name('facebook');
Route::map('/login', '\\Site\\Controller\\User:Login')->via('GET', 'POST')->name('site-login');
Route::map('/logout', '\\Site\\Controller\\User:Logout')->via('GET', 'POST')->name('site-logout');
Route::map('/signup', '\\Site\\Controller\\User:Signup')->via('GET', 'POST')->name('site-signup');
Route::map('/gplus-login', '\\Site\\Controller\\User:GoogleLogin')->via('GET', 'POST')->name('gplus-login');
Route::map('/gplus-login-redirect', '\\Site\\Controller\\User:GoogleLoginRedirect')->via('GET', 'POST')->name('gplus-redirect');

Route::map('/privacy-policy', '\\Site\\Controller\\StaticTemplate:PrivacyPolicy')->via('GET')->name('privacy-policy');
Route::map('/terms-and-condition', '\\Site\\Controller\\StaticTemplate:TermsAndCondition')->via('GET')->name('terms-and-condition');
Route::map('/contact-us', '\\Site\\Controller\\StaticTemplate:ContactUs')->via('GET','POST')->name('contact-us');
Route::map('/about-us', '\\Site\\Controller\\StaticTemplate:AboutUs')->via('GET')->name('about-us');

//USER
Route::map('/profile', '\\Site\\Controller\\User:Profile')->via('GET', 'POST')->name('user-profile');
Route::map('/password', '\\Site\\Controller\\User:Password')->via('GET', 'POST')->name('user-password');


// VIDEO
Route::get('/latest-all', '\\Site\\Controller\\Video:LatestAll')->name('latest-all');
Route::get('/video-detail(/:url(/:video_id))', '\\Site\\Controller\\Video:Detail')->name('video-detail')->conditions(array('video_id' => '[0-9]{1,}'));
Route::get('/playlist-all(/:playlist_id)', '\\Site\\Controller\\PlayList:ViewAll')->name('playlist-all');
Route::get('/category(/:category_name)', '\\Site\\Controller\\Category:Show')->name('category-site')->conditions(array('category_name' => '[A-Za-z]{1,}'));
Route::get('/most-viewed', '\\Site\\Controller\\Video:MostViewedMovie')->name('most-viewed');
Route::map('/search', '\\Site\\Controller\\Video:Search')->name('search')->via('GET', 'POST');

// PREMIUM
Route::map('/payment(/:id)', '\\Site\\Controller\\PaymentClass:Premium')->name('payment')->via('GET', 'POST');
Route::map('/approval-premium', '\\Site\\Controller\\PaymentClass:ApprovalPremium')->via('GET', 'POST');
Route::map('/approval-subscription', '\\Site\\Controller\\PaymentClass:ApprovalSubscription')->via('GET', 'POST');

//SUBSCRIPTION
Route::map('/subscription', '\\Site\\Controller\\Subscription:Show')->name('subscription')->via('GET', 'POST');
Route::map('/subscription-plan', '\\Site\\Controller\\PaymentClass:Subscription')->name('subscription-plan')->via('GET', 'POST');
Route::map('/subscription-message', '\\Site\\Controller\\PaymentClass:Subscription')->name('subscription-message')->via('GET', 'POST');
//Route::map('/subscription-message', '\\Site\\Controller\\Subscription:SubscriptionMessage')->name('subscription-message')->via('GET', 'POST');


/*
  ||============================================================
  || ###########################################################
  ||           ADMIN ROUTE DETAILS
 */


Route::group('/admin', function () {

    // AUTHENTICATION SECTION
    Route::map('/', '\\Auth\\Controller\\Auth:Login')->via('GET', 'POST')->name('login');
    Route::get('/logout', '\\Auth\\Controller\\Auth:Logout')->name('logout');
    Route::map('/forgot', '\\Auth\\Controller\\Auth:Forgot')->via('GET', 'POST')->name('forgot');
    Route::map('/forgot-change-password', '\\Auth\\Controller\\Auth:ForgotChangePassword')->via('GET', 'POST')->name('forgot-change-password');
    Route::map('/change-password', '\\Auth\\Controller\\Auth:EditPassword')->via('GET', 'POST')->name('change-password');
    Route::get('/user-detail', '\\Auth\\Controller\\Auth:UserDetail')->via('GET', 'POST')->name('user-detail');

    // DASHBOARD SECTION
    Route::map('/dashboard', '\\Admin\\Controller\\Dashboard:IndexAction')->via('GET')->name('dashboard');

    // USER SECTION
    Route::map('/user-grid', '\\Admin\\Controller\\User:Home')->via('GET', 'POST')->name('user-grid');
    Route::map('/user-add', '\\Admin\\Controller\\User:Insert')->via('GET', 'POST')->name('user-add');
    Route::post('/user-delete', '\\Admin\\Controller\\User:Delete')->name('user-delete');
    Route::map('/user-edit(/:eid(/:page))', '\\Admin\\Controller\\User:Edit')->via('GET', 'POST')->name('user-edit');
    Route::get('/user-status', '\\Admin\\Controller\\User:Status')->name('user-status');
    Route::get('/user-details(/:eid(/:page))', '\\Admin\\Controller\\User:Details')->name('user-details');

    // BANNER
    Route::map('/banner', '\\Admin\\Controller\\Banner:Home')->via('GET', 'POST')->name('banner');
    Route::map('/banner-add', '\\Admin\\Controller\\Banner:Insert')->via('GET', 'POST')->name('banner-add');
    Route::post('/banner-delete', '\\Admin\\Controller\\Banner:Delete')->name('banner-delete');
    Route::map('/banner-edit(/:eid)', '\\Admin\\Controller\\Banner:Edit')->via('GET', 'POST')->name('banner-edit');
    Route::get('/banner-status', '\\Admin\\Controller\\Banner:Status')->name('banner-status');
    Route::get('/banner-details(/:eid)', '\\Admin\\Controller\\Banner:Details')->name('banner-details');

    // CATEGORY
    Route::map('/category', '\\Admin\\Controller\\Category:Home')->via('GET', 'POST')->name('category');
    Route::map('/category-add', '\\Admin\\Controller\\Category:Insert')->via('GET', 'POST')->name('category-add');
    Route::post('/category-delete', '\\Admin\\Controller\\Category:Delete')->name('category-delete');
    Route::map('/category-edit(/:eid)', '\\Admin\\Controller\\Category:Edit')->via('GET', 'POST')->name('category-edit');
    Route::get('/category-status', '\\Admin\\Controller\\Category:Status')->name('category-status');
    Route::get('/category-details(/:eid)', '\\Admin\\Controller\\Category:Details')->name('category-details');

    // PLAYLIST
    Route::map('/playlist', '\\Admin\\Controller\\Playlist:Home')->via('GET', 'POST')->name('admin-playlist');
    Route::map('/playlist-add', '\\Admin\\Controller\\Playlist:Insert')->via('GET', 'POST')->name('admin-playlist-add');
    Route::post('/playlist-delete', '\\Admin\\Controller\\Playlist:Delete')->name('admin-playlist-delete');
    Route::map('/playlist-edit(/:eid)', '\\Admin\\Controller\\Playlist:Edit')->via('GET', 'POST')->name('admin-playlist-edit');
    Route::get('/playlist-status', '\\Admin\\Controller\\Playlist:Status')->name('admin-playlist-status');
    Route::get('/playlist-details(/:eid)', '\\Admin\\Controller\\Playlist:Details')->name('admin-playlist-details');

    // SUB PLAYLIST
    Route::map('/sub-playlist(/:playlist_id)', '\\Admin\\Controller\\Playlist:SubPlaylist')->via('GET', 'POST')->name('admin-sub-playlist');
    Route::map('/sub-playlist-add(/:movie_id)', '\\Admin\\Controller\\Playlist:SubPlayListAdd')->via('GET', 'POST')->name('admin-sub-playlist-add');
    Route::post('/sub-playlist-delete(/:playlist_id)', '\\Admin\\Controller\\Playlist:SubPlayListDelete')->name('admin-sub-playlist-delete');


    // NEWS
    Route::map('/movie', '\\Admin\\Controller\\Movie:Home')->via('GET', 'POST')->name('admin-movie');
    Route::map('/movie-add', '\\Admin\\Controller\\Movie:Insert')->via('GET', 'POST')->name('admin-movie-add');
    Route::post('/movie-delete', '\\Admin\\Controller\\Movie:Delete')->name('admin-movie-delete');
    Route::map('/movie-edit(/:eid)', '\\Admin\\Controller\\Movie:Edit')->via('GET', 'POST')->name('admin-movie-edit');
    Route::get('/movie-status', '\\Admin\\Controller\\Movie:Status')->name('admin-movie-status');
    Route::get('/movie-details(/:eid)', '\\Admin\\Controller\\Movie:Details')->name('admin-movie-details');
});


/*
  ||============================================================
  || ###########################################################
  ||           API ROUTE DETAILS
 */

Route::group('/api', function () {

    // VERSION 1
    Route::group('/v1', function () {
        // ALBUM
        Route::get('/album/', '\\Api\\v1\\Controller\\Album:Index');
    });

    // VERSION 2
    Route::group('v2/', function () {

        //Route::post('/contact/', '\\Api\\v1\\Controller\\Contact:Feedback');
    });
});


/*
  ||============================================================
  || ###########################################################
  ||           NOTIFICATION ROUTE
 */

Route::group('/notification', function () {

    // REGISTER GCM AND APNS IDS
    Route::map('/', '\\Notification\\Controller\\Notification:Home')->via('GET', 'POST')->via('GET', 'POST')->name('notification');
    Route::map('/notification-add', '\\Notification\\Controller\\Notification:Insert')->via('GET', 'POST')->name('notification-add');
    Route::post('/notification-delete', '\\Notification\\Controller\\Notification:Delete')->name('notification-delete');

    Route::post('/register-id/', '\\Notification\\Controller\\Notification:RegisterId');
    Route::post('/test-notification/', '\\Notification\\Controller\\Notification:TestNotification');
});


// SOURCE FILE :: Application/Modules/Routes.php