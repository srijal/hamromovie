<?php
/*
 ||============================================================
 || ###########################################################
 ||           ROUTE DETAILS FOR VERSION 2
 */

Route::group('/api/', function () {

    Route::group('v2/', function () {

        Route::get('show', 'api\v1\HotJokeController:Home');
        Route::get('my', 'api\v1\HotJokeController:MyMethod');
        Route::get('add', 'api\v1\HotJokeController:Add');
        Route::get('remove/:id', 'api\v1\HotJokeController:Delete');

    });
});
// SOURCE FILE :: Api\v2\Routes.php