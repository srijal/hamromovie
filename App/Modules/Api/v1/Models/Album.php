<?php

namespace Api\v1\Model;

use App;
use DB;


class Album {

    protected static $_table = "kripa_album";
    protected static $limit = 5;

    public static function GetAll($since_id = '', $max_id = '')
    {
        $sql = " SELECT

                        id as id,
                        image as image,
                        title as albumTitle,
                        7 as totalImage,
                        created_at as createdAt

                    FROM

                    " . self::$_table;

        if ($since_id != '') {

            $sql .= "  WHERE id >  {$since_id} ORDER BY id DESC LIMIT " . self::$limit;
        } elseif ($max_id != '') {

            $sql .= "  WHERE id < {$max_id}  ORDER BY id DESC LIMIT " . self::$limit;
        } else {

            $sql .= " ORDER BY id DESC LIMIT " . self::$limit;
        }

        DB::query($sql, '', true);

        $result = DB::fetch_assoc_all();

        return isset($result) ? $result : FALSE;
    }


}