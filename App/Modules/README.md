#####################################################################
Procedure to create new module
#####################################################################

DOCUMENTATION PREFERRED

1. After Creating New module don't forget to add newly created module folder in (Config/Modules.php)

2. Also  add namespace and folder in twig view template directory(Config/View)

    => App::view()->-setTemplatesDirectory(
        'your_module_name'=>'path_to_module_views_folders'
    );

    => So that you can use as View::display('@your_module_name','html_file_inside_view_folder');
    => For reference use twig documentation.

3. Congratulation you have done great job ... Enjoy