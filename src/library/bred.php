<?php
    class Bred
    {
        public $sym;
        public $home;
        public $base_url;
     
        function bred_cra()
        {
            $server_path = $_SERVER['PHP_SELF'];
            if($this->base_url != NULL)
            {
                $base = parse_url($this->base_url);
                
                if(strpos($server_path, $base['path']) >=0)
                {
                    $server_path = substr($server_path, strlen($base['path']));
                }
            }

            $url = ($this->base_url != NULL ? $this->base_url . $server_path : 'http://' .((!empty($_SERVER['HTTPS'])) ? "s" : "") . $_SERVER['HTTP_HOST'] . $server_path);

            $domain = ($this->base_url != NULL ? $this->base_url : 'http://' .((!empty($_SERVER['HTTPS'])) ? "s" : "") . $_SERVER['HTTP_HOST']);
            
            $url = substr($url, strlen($domain));

            $path = explode("/", $url);

            if(end($path) == '')
            {
                array_pop($path);
            }
            if(end($path) == 'index.php' || end($path) == 'index.html')
            {
                array_pop($path);
            }


            for($i=0; $i <= count($path)-1; $i++)
            {
                    if($i == 0)
                    {
                        $bred = '<a href="' . $domain . '">' . ($this->home==NULL ? "Home" : $this->home). '</a>';
                    }
                    elseif($i > 0 && $i < count($path)-1)
                    {
                        $pre_url .= $path[$i] . '/';
                        
                        if($path[$i+1] == 'index.php' || $path[$i+1] == 'index.html')
                        {
                            $pre_url .= $path[$i+1] . '/';
                            $bred .= ($this->sym == "" ? " " : $this->sym). '<a href="' . $domain . '/' . $pre_url . '">' . ucfirst($path[$i]) . '</a>';
                            $i++;
                        }

                        else
                        {
                            $bred .= ($this->sym == "" ? " " : $this->sym). '<a href="' . $domain . '/' . $pre_url . '">' . ucfirst($path[$i]) . '</a>';
                        }
                    }
                    
                    else
                    {
                        $bred .= ($this->sym == "" ? " " : $this->sym)  . ucfirst($path[$i]);
                    }
            }
            print $bred;
        }
    }
    
    /* Use
    $obj = new Bred;
    $obj->sym = ' >> ';//optional simbol to separate pages (default space if not used)
    $obj->base_url = 'http://localhost/bred/';//works without only if domain and root directory are same
    //$obj->home = 'root_dir';//optional
    $obj->bred_cra();
    
    */
?>