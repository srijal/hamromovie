<?php

class Upload
{
    protected static $instance = null;


    // Upload Multiple And Single File
    public static function UploadFile($image_input_name, $image_path)
    {
        //CREATE DIRECTORY IF NOT EXISTS
        if (is_dir($image_path) == FALSE) {
            $status = mkdir($image_path, 0744, TRUE);
            if ($status < 1) {
                echo "<script> alert('Unable to Create Folder Path.')</script>";
            }
        }

        //MULTIPLE FILE UPLLOAD
        if (is_array($_FILES[$image_input_name]['tmp_name'])) {
            foreach ($_FILES[$image_input_name]['tmp_name'] as $key => $tmp_name) {
                $file_name = $_FILES[$image_input_name]['name'][$key];
                $file_tmp_name = $_FILES[$image_input_name]['tmp_name'][$key];
                $file_size = $_FILES[$image_input_name]['size'][$key];
                if ($file_size > 0) {
                    //RENAME WITH NEW DATE AND TIME
                    $image = date('ymdh') . str_replace(' ','_',$file_name);
                    $path = $image_path . $image;
                    move_uploaded_file($file_tmp_name, $path);
                    $images[] = $path;
                }
            }//FOREACH ENDS
        } else {
            //SINGLE FILE UPLOAD
            $file_name = $_FILES[$image_input_name]['name'];
            $file_tmp_name = $_FILES[$image_input_name]['tmp_name'];
            $file_size = $_FILES[$image_input_name]['size'];
            if ($file_size > 0) {
                //RENAME WITH NEW DATE AND TIME
                $images = date('ymdh') . str_replace(' ','_',$file_name);
                $path = $image_path . $images;
                move_uploaded_file($file_tmp_name, $path);
            }
        }
        return $path;
    }

    // Check If File set or not
    public static function IsFileSet($imageName)
    {
        if ($_FILES[$imageName]['size'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Unlink File
    public static function Unlink($filePath)
    {
        if (is_file($filePath)) {
            unlink($filePath);
            return true;
        } else {
            return false;
        }

    }

    public static function Instance()
    {
        if(!isset(self::$instance)){
            self::$instance = new Upload();
        }
        return self::$instance;
    }

}
