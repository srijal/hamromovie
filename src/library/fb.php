<?php
/** 
 * share.php :: social share buttons class 
 * 
 * Class version 1.0.0.0 
 * copyright (c) 2012 by Sandeep Kumar 
 * Social Share Buttons is an open source PHP class library for easily display various social share buttons on webpages.  
  
 * Social Share Buttons is released under the terms of the LGPL license 
 * http://www.gnu.org/copyleft/lesser.html#SEC3 
 * 
 * This library is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU Lesser General Public 
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version. 
 * 
 * This library is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details. 
 *  
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library; if not, write to the Free Software 
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 
 *  
 * @package Social Networking 
 * @copyright Copyright (c) 2012-2015  by Sandeep Kumar 
 * @license http://www.gnu.org/copyleft/lesser.html#SEC3 LGPL License 
 */ 
class Fb
{
	var $title='';
	var $url='';
	var $body='';
	var $theme='';
	
	var $bookmarks=array('delicious'=>'del.icio.us','stumbleupon'=>'StumbleUpon','digg'=>'Digg','technorati'=>'Technorati','mixx'=>'Mixx','twitter'=>'Twit This','facebook'=>'Facebook','newsvine'=>'NewsVine','reddit'=>'Reddit','google'=>'Google','yahoo'=>'YahooMyWeb');
	
	function social($url,$title)
	{
	
		$this->title=$title;
		$this->url=$url;
		
	}
	function createCode($theme='default')
	{
		$this->theme=$theme;
		$li="<ul class='social_buttons'>";
		foreach($this->bookmarks as $name=>$value)
		{
			$li.="<li><a href='".$this->getUrl($name)."' style='background:none'><img src='theme/".$this->theme."/".$name.".png' alt='".$value."' title='".$value."'  ></a></li>";		
		}
		$li.="</ul>";
		
		return $li;
	}
	
	function getUrl($url)
	{
		switch($url)
		{
		
			case 'delicious' : return 'http://del.icio.us/post?url='.urlencode($this->url).'&title='.urlencode($this->title); break;
			case 'stumbleupon' : return 'http://www.stumbleupon.com/submit?url='.urlencode($this->url).'&title='.urlencode($this->title); break;
			case 'digg'		   : return 'http://digg.com/submit?url='.urlencode($this->url);break;
			case 'technorati': return 'http://technorati.com/favorites/?add='.urlencode($this->url);break;
			case 'mixx' : return 'http://www.mixx.com/submit?page_url='.urlencode($this->url).'&title='.urlencode($this->title);break;
			case 'twitter': return 'http://twitthis.com/twit?url='.urlencode($this->url);break;
			case 'facebook': return 'http://www.facebook.com/sharer.php?u='.urlencode($this->url).'&t='.urlencode($this->title);break;
			case 'newsvine': return 'http://www.newsvine.com/_tools/seed&save?u='.urlencode($this->url).'&h='.urlencode($this->title);break;	
			case 'reddit': return 'http://reddit.com/submit?url='.urlencode($this->url).'&title='.urlencode($this->title);break;	
			case 'google': return 'http://www.google.com/bookmarks/mark?op=edit&bkmk='.urlencode($this->url).'&title='.urlencode($this->title); break;		
			case 'comments' : return 'http://co.mments.com/track?url='.urlencode($this->url).'&title='.urlencode($this->title); break;
			case 'yahoo' : return 'http://myweb2.search.yahoo.com/myresults/bookmarklet?u='.urlencode($this->url).'&='.urlencode($this->url);break;
		
	}
	
}
}
?>