<?php
/*
* Author: Mick Sear
* See http://www.ecreate.co.uk
* Please support my work by clicking the Google links on my site :)
* 
* Overview:  On every page, stick this code in.  This page will go to 
*/
session_start();
include("./Breadcrumb.php");
$trail = new Breadcrumb();
$trail->add('Home', $_SERVER['PHP_SELF'], 0);

//Sample CSS
echo "
<style>
 #breadcrumb ul li{
   list-style-image: none;
   display:inline;
   padding: 0 3px 0 0; 
   margin: 3px 0 0 0;
 }
 #breadcrumb ul{
   margin:0;padding:0;
   list-style-type: none;
   padding-left: 1em;
 }
</style>
";

//Now output the navigation.
$trail->output();
?>