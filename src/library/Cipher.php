<?php

class Cipher {

    private $securekey, $iv;

    function __construct($textkey) {
        $this->securekey = hash('sha256', $textkey, TRUE);
        $this->iv = mcrypt_create_iv(32);
    }

    function encrypt($input) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
    }

    function decrypt($input) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
    }

}


/*$cipher = new Cipher('wX1Q{!^v1V*A"$tC0*=.x?b;g09-97');
$encryptedtext = $cipher->encrypt("RaghunATH cHAAUDAHARY");
echo "$encryptedtext<br />";
$decryptedtext = $cipher->decrypt($encryptedtext);
echo " $decryptedtext<br />";
echo '<pre>';
print_r($cipher);*/