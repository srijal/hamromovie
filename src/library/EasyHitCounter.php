<?php
/**
    * @name EasyHitCounter
    *
    * Simples classe que grava as visitas em um arquivo de texto
    * Use a classe da seguinte forma
    * 
    *     <?php
    *        $contador = new  EasyHitCounter();
    *         echo $contador->getCount();
    *    ?>
    * 
    * Voc� tambem podera especificar um diret�rio
    * 
    *     <?php
    *        $contador = new  EasyHitCounter('diretorio/');
    *         echo $contador->getCount();
    *    ?>
    * 
    * 
    * Ou pedir que o resultado saia formatado
    * 
    *  
    *     <?php
    *        $contador = new  EasyHitCounter('diretorio/');
    *         echo $contador->getCountFormated();
    *    ?>
    * 
    * 
    * Obs.: Nao esque�a de conceder as permiss�es para o diret�rio
    * 
    * 
    * 
    *
    * @package PontoPHP
    *
    * @author Enio Borges Ribeiro <damutuca@gmail.com>
    * @version 1.0
    * @since version 1.0
*/




final class EasyHitCounter{

    private $ipList;
    private $countFile;
    private $code;



    public function __construct($path = null){

        if(!preg_match("/\//", $path) || !is_dir($path)){
            $path = "./";
        }

        $this->ipList     = $path . 'ipList.txt';
        $this->countFile= $path . 'count.txt';

        $this->setCode();
        $this->insertIpInList();
    }


    private function setCode(){
        $this->code = $this->getIP() ."|". gethostbyaddr($this->getIP()) ."|". date('Ymd') ."\r\n";
        return $this;
    }

    private function getCode(){
        return $this->code;
    }


    /**
     * Pega o Ip do Usu�rio
     *
     * @return string
     */
    private function getIP()
    {
        if (isset($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        } else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if(isset($_SERVER['HTTP_X_FORWARDED'])) {
            return $_SERVER['HTTP_X_FORWARDED'];
        } else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        } else if(isset($_SERVER['HTTP_FORWARDED'])) {
            return $_SERVER['HTTP_FORWARDED'];
        } else if(isset($_SERVER['HTTP_X_COMING_FROM'])) {
            return $_SERVER['HTTP_X_COMING_FROM'];
        } else if(isset($_SERVER['HTTP_COMING_FROM'])) {
            return $_SERVER['HTTP_COMING_FROM'];
        } else if(isset($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
    }


    /**
     * Cria o arquivo com os IPs
     *
     * @return void
     */
    private function createListIps(){
        $fp = fopen($this->ipList, 'a+') or die("O arquivo n�o pode ser aberto!");
        return $this;
    }

    /**
     * Retorna os IPs do arquivo
     *
     * @return array
     */
    private function getIpInList(){

        if($fp = @fopen($this->ipList, 'r') === FALSE){
            $this->createListIps();
        }else{
            $fp = fopen($this->ipList, 'r');
        }

        $ipsArray = array();

        if(count(file($this->ipList)) > 0){
            while (!feof ($fp) ) {
                $line = fgets($fp,100);
                array_push($ipsArray, $line);
            }
        }

        return $ipsArray;
    }


    private function findIpInList(){

        $ipsArray = $this->getIpInList();

        if(sizeof($ipsArray) > 0){

            if(array_search($this->getCode(), $ipsArray) === FALSE){
                return 0;
            }else{
                return 1;
            }

        }else{
            return 0;
        }

    }


    private function insertIpInList(){

        $ipsArray = $this->getIpInList();

        $fp     = fopen($this->ipList, 'a+') or die("O arquivo n�o pode ser aberto!");


        if(sizeof($ipsArray) <= 0)
        {
            fwrite($fp, $this->getCode());
            $this->writeCount();
        }
        else
        {
            if($this->findIpInList() <= 0){
                fwrite($fp, $this->getCode());
                $this->writeCount();
            }
        }

        fclose($fp);

    }


    private function createCountFile(){
        $fp = fopen($this->countFile, "a+");
        fwrite($fp, 0);
        fclose($fp);
    }


    private function readCount(){

        if(!file_exists($this->countFile)){
            $this->createCountFile();
        }

        $fp  = fopen($this->countFile, "r") or die("O arquivo n�o pode ser aberto!");
        $num = fread($fp, filesize($this->countFile));
        fclose($fp);

        return $num;
    }

    private function writeCount(){
        $num = $this->readCount();
        $fp  = fopen($this->countFile, "w") or die("O arquivo n�o pode ser aberto!");
        fwrite($fp, $num + 1);
        fclose($fp);
    }

    public function getCount(){
        return $this->readCount();
    }
    
    public function getCountFormated(){
        return number_format($this->readCount(),null, null, '.');
    }

}


?>