<?php

/*
  |----------------------------------------------------------------------
  | Programmar Raghu Chaudhary Updated (2014-12-20)
  |----------------------------------------------------------------------
 */

class Validate
{

    public static $errors = array();
    protected static $instance = null;


    public static function Instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Validate();
        }
        return self::$instance;
    }

    /*
      |--------------------------------------------
      | Validate File
      |--------------------------------------------
     */

    public static function File($image_input_name, $req = TRUE, $format = array('jpeg', 'jpg', 'gif', 'png'), $size = 2, $fileName = 'File')
    {

        //MULTIPLE FILE UPLOAD VALIDATION
        if (is_array($_FILES[$image_input_name]['tmp_name'])) {
            foreach ($_FILES[$image_input_name]['tmp_name'] as $key => $tmp_name) {
                $file_name = $_FILES[$image_input_name]['name'][$key];
                $ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
                $file_size = $_FILES[$image_input_name]['size'][$key];

                //CHECK FILE SIZE
                if ($req == TRUE) {
                    if ($file_size <= 0) {
                        self::SetError($fileName, "{$fileName} Is Empty");

                        //CHECK FILE EXTENTION
                    }
                }
                if ($file_size > 0) {
                    if (!in_array($ext, $format)) {
                        self::SetError($fileName, 'Only ' . implode(" , ", $format) . " are allowed");
                        //CHECK FILE SIZE
                    } elseif ($file_size >= 1048576 * $size) {
                        self::SetError($fileName, "{$fileName} Size is greater than " . $size . " Mb");
                    }
                }
            }
            //FOREACH ENDS
        } //IS_ARRAY CHECK ENDS
        else {
            //SINGLE FILE UPLOAD VALIDATION
            $file_name = $_FILES[$image_input_name]['name'];
            $ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
            $file_size = $_FILES[$image_input_name]['size'];

            //CHECK FILE SIZE
            if ($req == TRUE) {
                if ($file_size <= 0) {
                    self::SetError($fileName, "{$fileName} Is Empty");

                    //CHECK FILE EXTENTION
                }
            }
            if ($file_size > 0) {
                if (!in_array($ext, $format)) {
                    self::SetError($fileName, 'Only ' . implode(" , ", $format) . "  are allowed");
                    //CHECK FILE SIZE
                } elseif ($file_size >= 1048576 * $size) {
                    self::SetError($fileName, "{$fileName} Size is greater than " . $size . " Mb");
                }
            }
        }
        //END ELSE
    }

    /*
      |--------------------------------------------
      | Validate String
      |--------------------------------------------
     */

    public static function Str($postVal, $postName, $req = TRUE, $min = 2, $max = 999999)
    {
        $postVal = trim($postVal);
        if ($req == TRUE) {
            if (strlen($postVal) < 1 or empty($postVal)) {
                self::SetError($postName, ucfirst($postName) . " field is Required.");
            }
        }
        if (!empty($postVal)) {
            if (strlen($postVal) < intval($min)) {
                self::SetError($postName, ucfirst($postName) . " must be at least {$min} characters long.");
            } elseif (strlen($postVal) > intval($max)) {
                self::SetError($postName, ucfirst($postName) . " must be less than {$max} characters long.");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate Password
      |--------------------------------------------
     */

    public static function Pass($passVal, $passName = "Password")
    {
        $passVal = trim($passVal);
        if (empty($passVal)) {
            self::SetError($passName, ucFirst($passName) . " field is Required.");
        } elseif (strlen($passVal) < 6) {
            self::SetError($passName, ucFirst($passName) . " must be at least 6 character long.");
        } elseif (!preg_match('#[0-9]+#', $passVal)) {
            self::SetError($passName, ucFirst($passName) . " must have at least one Number.");
        } elseif (!preg_match('#[a-z]+#', $passVal)) {
            self::SetError($passName, ucFirst($passName) . " must have at least one Lowercase Letter.");
        } elseif (!preg_match('#[A-Z]+#', $passVal)) {
            self::SetError($passName, ucFirst($passName) . " must have at least one Uppercase Letter.");
        } elseif (!preg_match('/[!\-_\+=\)\(\*&\^%$#@!\}\{\[\]|\.\:;|\,<>\?]+/', $passVal)) {
            self::SetError($passName, ucFirst($passName) . " must have at least one special character.");
        } elseif (strlen($passVal) > 20) {
            self::SetError($passName, ucFirst($passName) . " must not be greater than 20 character long.");
        }
    }

    /*
      |--------------------------------------------
      | Validate Password Match
      |--------------------------------------------
     */

    public static function Match($passRetype, $passFirst, $passName = "Retyped Password")
    {
        $passRetype = trim($passRetype);
        $passFirst = trim($passFirst);
        if (empty($passRetype) or strlen($passRetype) < 1) {
            self::SetError($passName, ucfirst($passName) . " field is Empty.");
        } elseif ($passRetype != $passFirst) {
            self::SetError($passName, ucfirst($passName) . " didn't Matched!.");
        }
    }

    /*
      |--------------------------------------------
      | Validate Checkbox
      |--------------------------------------------
     */

    public static function CheckBox($cbVal, $cbName, $cbMin = 1, $cbMax = 2)
    {
        //$cbVal = trim($cbVal);
        if (count($cbVal) <= 0) {
            self::SetError($cbName, ucFirst($cbName) . " is not Checked.");
        } else
            if (count($cbVal) < $cbMin) {
                self::SetError($cbName, ucFirst($cbName) . " field must have at least {$cbMin} Checked. ");
            } elseif (count($cbVal) > $cbMax) {
                self::SetError($cbName, ucFirst($cbName) . " cannot be checked more than {$cbMax} Field. ");
            }
    }

    /*
      |--------------------------------------------
      | Validate Radio
      |--------------------------------------------
     */

    public static function Radio($rbVal, $rbName)
    {
        $rbVal = trim($rbVal);
        if (count($rbVal) <= 0) {
            self::SetError($rbName, ucFirst($rbName) . " is not Checked.");
        }
    }

    /*
      |--------------------------------------------
      | Validate Email
      |--------------------------------------------
     */

    public static function Email($postVal, $postName = "Email", $req = TRUE)
    {
        $postVal = trim($postVal);
        if ($req == TRUE) {
            if (strlen($postVal) < 1 or empty($postVal)) {
                self::SetError($postName, ucfirst($postName) . " field is Required.");
            }
        }
        if (!empty($postVal)) {
            $domain = substr($postVal, strpos($postVal, '@') + 1);
            if (!preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $postVal) AND !filter_var($postVal, FILTER_VALIDATE_EMAIL)) {
                self::SetError($postName, ucFirst($postName) . " address is not Valid");
            } elseif (checkdnsrr($domain) == FALSE) {
                self::SetError($postName, ucFirst($postName) . " Domain is not Valid");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate Bool
      |--------------------------------------------
     */

    public static function Bool($boolVal, $boolName, $req = TRUE)
    {
        $boolVal = trim($boolVal);
        if ($req == TRUE) {
            if (strlen($boolVal) <= 0) {
                self::SetError($boolName, ucFirst($boolName) . " field is Required");
            }
        }
        if (!empty($boolVal)) {
            if (!filter_var($boolVal, FILTER_VALIDATE_BOOLEAN)) {
                self::SetError($boolName, ucFirst($boolName) . " is not a Valid Boolean");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate Float
      |--------------------------------------------
     */

    public static function Float($floatVal, $floatName, $req = TRUE)
    {
        $floatVal = trim($floatVal);
        if ($req == TRUE) {
            if (strlen($floatVal) <= 0) {
                self::SetError($floatName, ucFirst($floatName) . " field is required");
            }
        }
        if (!empty($floatVal)) {
            if (!filter_var($floatVal, FILTER_VALIDATE_FLOAT)) {
                self::SetError($floatName, ucFirst($floatName) . " is not  a valid float number");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate IP
      |--------------------------------------------
     */

    public static function Ip($ipVal, $ipName, $req = TRUE)
    {
        $ipVal = trim($ipVal);
        if ($req == TRUE) {
            if (strlen($ipVal) <= 0) {
                self::SetError($ipName, ucFirst($ipName) . " field is Required");
            }
        }
        if (!empty($ipVal)) {
            if (!filter_var($ipVal, FILTER_VALIDATE_IP)) {
                self::SetError($ipName, ucFirst($ipName) . " Not a Valid IP Address");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate Url
      |--------------------------------------------
     */

    public static function Url($urlVal, $urlName, $req = TRUE)
    {
        $urlVal = trim($urlVal);
        if ($req == TRUE) {
            if (strlen($urlVal) <= 0) {
                self::SetError($urlName, ucFirst($urlName) . " field is Required");
            }
        }
        if (!empty($urlVal)) {
            if (!filter_var($urlVal, FILTER_VALIDATE_URL)) {
                self::SetError($urlName, ucFirst($urlName) . " is not a Valid URL Address");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate Alphanumeric
      |--------------------------------------------
     */

    public static function AlphaNum($anVal, $anName, $req = TRUE, $min = 1, $max = 99)
    {
        $anVal = trim($anVal);
        if ($req == TRUE) {
            if (strlen($anVal) <= 0) {
                self::SetError($anName, ucFirst($anName) . " field is Required");
            }
        }
        if (!empty($anVal)) {
            if (!preg_match('/^[a-z0-9][a-z0-9-]*[a-z0-9]$/i', $anVal)) {
                self::SetError($anName, ucfirst($anName) . " Not a Valid Alphanumeric Value");
            } elseif (strlen($anVal) < intval($min)) {
                self::SetError($anName, ucfirst($anName) . " must be at least {$min} characters long.");
            } elseif (strlen($anVal) > intval($max)) {
                self::SetError($anName, ucfirst($anName) . " must be less than {$max} characters long.");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate Number
      |--------------------------------------------
     */

    public static function Num($numVal, $numName, $req = TRUE, $min = 3, $max = 15)
    {
        $numVal = trim($numVal);
        if ($req == TRUE) {
            if (strlen($numVal) <= 0) {
                self::SetError($numName, ucFirst($numName) . " field is Required");
            }
        }

        if (!empty($numVal)) {
            if (!is_numeric($numVal)) {
                self::SetError($numName, ucfirst($numName) . " is  not a Valid Number");
            } elseif (strlen($numVal) < intval($min)) {
                self::SetError($numName, ucfirst($numName) . " must be at least {$min} characters long.");
            } elseif (strlen($numVal) > intval($max)) {
                self::SetError($numName, ucfirst($numName) . " must be less than {$max} characters long.");
            }
        }
    }

    /*
      |--------------------------------------------
      | Validate Character
      |--------------------------------------------
     */

    public static function Char($charVal, $charName, $req = TRUE, $min = 1, $max = 99)
    {
        $charVal = trim($charVal);
        if ($req == TRUE) {
            if (strlen($charVal) <= 0) {
                self::SetError($charName, ucFirst($charName) . " field is Required");
            }
        }

        if (!empty($charVal)) {
            if (preg_match('/[^a-zA-Z ]+/', $charVal)) {
                self::SetError($charName, $charName . "is not a Valid Character");
            } elseif (strlen($charVal) < intval($min)) {
                self::SetError($charName, ucfirst($charName) . " must be at least {$min} characters long.");
            } elseif (strlen($charVal) > intval($max)) {
                self::SetError($charName, ucfirst($charName) . " must be less than {$max} characters long.");
            }
        }
    }

    /*
      |--------------------------------------------
      | Repopulate Data
      |--------------------------------------------
     */

    public static function RePopulate($value, $reset = false)
    {

        return isset($reset) ? '' : self::SetValue($value);
    }

    /*
   |--------------------------------------------
   | Repopulate Data
   |--------------------------------------------
  */

    public static function RePopulateRadio($postName, $value, $reset = false)
    {
        $chk = '';

        if(!$reset){

            if($_REQUEST[$postName] === $value){

                $chk= "checked";

            }else{
                $chk = "";
            }

        }

        return $chk;
    }

    /*
|--------------------------------------------
| Repopulate Data
|--------------------------------------------
*/

    public static function RePopulateSelect($postName, $value, $reset = false)
    {
        $chk = '';

        if(!$reset){

            if($_REQUEST[$postName] === $value){

                $chk= "selected";

            }else{
                $chk = "";
            }

        }

        return $chk;
    }

    /*
      |--------------------------------------------
      | Set Error
      |--------------------------------------------
     */

    private static function SetError($element, $message)
    {
        self::$errors[$element] = $message;
    }

    /*
      |--------------------------------------------
      | Repopulate input Data
      |--------------------------------------------
     */

    public static function SetValue($postName)
    {

        return isset($_REQUEST[$postName]) ? $_REQUEST[$postName] : '';
    }

    /*
      |--------------------------------------------
      | Get Errors
      |--------------------------------------------
     */

    public static function GetError($elementName)
    {
        if (isset(self::$errors[$elementName])) {
            return self::$errors[$elementName];
        } else {
            return '';
        }
    }

    /*
      |--------------------------------------------
      | Get Error List
      |--------------------------------------------
     */

    public static function ErrorList()
    {
        $errorsList = "<ul style=\"color:red;list-style:none;font:15px Arial;padding:3px;\">\n";
        foreach (self::$errors as $value) {
            $errorsList .= "<li>" . $value . "</li>\n";
        }
        $errorsList .= "</ul>\n";
        return $errorsList;
    }

    public static function IsFine()
    {
        if (count(self::$errors) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /*
      |--------------------------------------------
      | Display Error in Number
      |--------------------------------------------
     */

    public static function ErrorNum()
    {
        if (count(self::$errors) > 1) {
            $message = "There were " . count(self::$errors) . " errors sending your message!\n";
        } else {
            $message = "There was an error sending your message!\n";
        }
        return $message;
    }

// END HASERROR
}
